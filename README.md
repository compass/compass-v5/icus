# Quickstart

3 lines for (1) configuring, (2) compiling & testing, (3) building python bindings:
```shell
rm -rf _build && cmake -B _build -DCMAKE_BUILD_TYPE=Debug -DBUILD_DOC=OFF
cmake --build _build -j `nproc` && cmake --build _build --target test
python -m pip install --no-build-isolation -ve . --config-settings=cmake.build-type=Debug
```

For testing python bindings, you need to build the example binding too:
```shell
python -m pip install --no-build-isolation -ve python/test/custom_field_example --config-settings=cmake.define.icus_DIR=`realpath _build/cmake`
pytest python/test
```

# C++ compilation (headers decalration and tests)

To compile using cmake:

```
# configure
cmake -B _build -DICUS_BUILD_EXPERIMENTAL=ON -Dnanobind_DIR=/where/to/find/nanobind -Drange-v3_DIR=/where/to/find/rangev3
# compile
cmake --build _build -j 4
```


# Python bindings

Python bindings are generated using [nanobind](https://github.com/wjakob/nanobind).
The compilation is driven by [CMake](https://cmake.org) relying on [scikit-build-core](https://github.com/scikit-build/scikit-build-core).
Currently the bindings are generated using static linking to nanobind.
Dynamic linking could be use if there are more than a single binding.

To build python bindings you must first have build the C++ icus project (let's suppose you used the `build` directory - cf. supra), then run from the root icus directory:

```shell
python -m pip install --no-build-isolation -ve .`
```

You can pass several options (cf. [scikit-build-core](https://github.com/scikit-build/scikit-build-core) documentation). For example if you want a Debug build:

```shell
python -m pip install --no-build-isolation -ve . --config-settings=cmake.build-type=Debug
```

The `-ve` option is to have verbose output and use editable mode.

To compile the field demo project:

```shell
python -m pip install --no-build-isolation -ve python/test/custom_field_example --config-settings=cmake.define.icus_DIR=`realpath _build/cmake`
```
