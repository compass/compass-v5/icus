
#include <iostream>
#include <range/v3/action/sort.hpp>
#include <range/v3/action/unique.hpp>
#include <range/v3/view/all.hpp>
#include <vector>

template <typename R>
std::ostream& dump(std::ostream& os, const R& r) {
    for (auto&& x : r) {
        os << x << " ";
    }
    return os;
}

int main() {
    using namespace ranges;
    auto all_unique = actions::sort | actions::unique;
    auto v = std::vector<int>({0, 1, 1, 0});
    dump(std::cout, v) << std::endl;
    // v |= actions::sort | actions::unique;
    // dump(std::cout, v) << std::endl;
    // dump(std::cout, move(v) | actions::sort | actions::unique) << std::endl;
    // dump(std::cout, v) << std::endl; // v is empty (cf. move -> actions
    // consume ranges)
    dump(std::cout, move(v) | all_unique) << std::endl;
    dump(std::cout, v) << std::endl;
    v = {0, 1, 1, 0};
    dump(std::cout, views::all(v) | actions::sort) << std::endl;
    dump(std::cout, v) << std::endl;
    v = {0, 1, 1, 0};
    auto w = move(v) | actions::sort;
    dump(std::cout, v) << std::endl;
    dump(std::cout, w) << std::endl;
    v = {0, 1, 1, 0};
    std::vector<int> vi;
    std::cout
        << "insert(vi, end(vi), move(v) | actions::sort | actions::unique)"
        << std::endl;
    insert(vi, end(vi), move(v) | actions::sort | actions::unique);
    dump(std::cout, v) << std::endl;
    dump(std::cout, vi) << std::endl;
}
