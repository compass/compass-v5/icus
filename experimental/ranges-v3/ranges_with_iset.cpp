
#include <icus/ISet.h>

#include <algorithm>
#include <iostream>
#include <range/v3/all.hpp>
#include <vector>

int main() {
    auto I = icus::ISet(5);

    std::for_each(begin(I), end(I), [](auto&& i) { std::cout << i << " "; });
    std::cout << std::endl;

    ranges::for_each(begin(I), end(I), [](auto&& i) { std::cout << i << " "; });
    std::cout << std::endl;

    ranges::for_each(I, [](auto&& i) { std::cout << i << " "; });
    std::cout << std::endl;

    std::vector<icus::ISet::index_type> v{begin(I), end(I)};
    ranges::fill(v, 0);
    assert(size(v) == size(I));
    ranges::copy(I, &v.front());
}
