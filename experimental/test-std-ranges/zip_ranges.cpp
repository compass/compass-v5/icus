#include <array>
#include <iostream>
#include <list>
#include <ranges>
#include <tuple>
#include <vector>

int main() {
    auto x = std::vector{1, 2, 3, 4};
    auto y = std::array{'A', 'B', 'C', 'D', 'E', 'F'};

    // WAIT For C++23 !

    // for (auto&& [i, c] : std::views::zip(x, y)) {
    //     std::cout << i << " " << c << std::endl;
    //     // std::get<char&>(elem) += ('a' - 'A'); // modifies the element of z
    // }

    auto v = std::vector<std::pair<int, int>>{{1, 2}, {3, 4}};
    for (auto &&[i, j] : v) {
        std::cout << i << " " << j << std::endl;
        ++i;
    }
    for (auto &&[i, j] : v) {
        std::cout << i << " " << j << std::endl;
    }
}
