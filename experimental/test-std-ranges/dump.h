#pragma once

#include <iostream>

std::ostream &dump(std::ostream &os, auto &&I) {
    for (auto &&i : I) {
        os << i << " ";
    }
    os << std::endl;
    return os;
}
