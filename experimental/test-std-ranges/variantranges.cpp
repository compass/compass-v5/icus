#include <algorithm>
#include <iostream>
#include <ranges>
#include <variant>
#include <vector>

#include "dump.h"

namespace icus {

using index_t = int;

template <typename... Ts>
struct Mix {
    using alternatives = std::tuple<Ts...>;
    using range = std::variant<Ts...>;
    // must be enriched with some operators (++, *, ...)
    using base_iterator = std::variant<std::ranges::iterator_t<Ts>...>;
    using sentinel = std::variant<std::ranges::sentinel_t<Ts>...>;
    struct iterator : base_iterator {
        index_t operator*() {
            return std::visit([](auto &&p) { return *p; },
                              *reinterpret_cast<base_iterator *>(this));
        }
        iterator &operator++() {
            std::visit([](auto &&p) { ++p; },
                       *reinterpret_cast<base_iterator *>(this));
            return *this;
        }
    };

    range vrange;

    auto begin() {
        return std::visit(
            [](auto &&r) { return iterator{std::ranges::begin(r)}; }, vrange);
    }

    auto end() {
        return std::visit(
            [](auto &&r) { return sentinel{std::ranges::end(r)}; }, vrange);
    }

    auto size() const {
        return std::visit(
            [](auto &&r) {
                return static_cast<std::size_t>(std::ranges::size(r));
            },
            vrange);
    }
};

using indices = std::vector<index_t>;
using stride = std::ranges::iota_view<index_t, index_t>;  // = Python's range
using mix = Mix<indices, stride>;

}  // namespace icus

int main() {
    auto I = icus::stride{3, 8};
    icus::mix m{I};
    dump(std::cout, m);
    m = icus::mix{icus::indices{{1, 2, 9, 10}}};
    dump(std::cout, m);
    return 0;
}
