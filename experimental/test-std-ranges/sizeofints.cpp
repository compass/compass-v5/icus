#include <iostream>
// https://en.cppreference.com/w/cpp/types/integer
#include <cstdint>

int main() {
    std::cout << sizeof(int) << " vs " << sizeof(std::size_t) << std::endl;

    using index_type = std::uint_fast32_t;
    std::cout << sizeof(int) << " vs " << sizeof(index_type) << std::endl;

    // comparaison sans warning
    index_type i = 8;
    std::size_t n = 10;
    if (i < n) std::cout << "c'est tout bon" << std::endl;

    return 0;
}
