#include <algorithm>
#include <iostream>
#include <ranges>
#include <vector>

#include "dump.h"

// Remark: not so easy to use ADL to simplify code

int main() {
    auto I = std::ranges::iota_view{0, 100};

    dump(std::cout, I);
    dump(std::cout, I);

    std::cout << "Memory footprint: " << sizeof(I) << " bytes" << std::endl;
    // ADL does not work here because I is in namespace views
    std::cout << "Size of I: " << std::ranges::size(I) << std::endl;

    std::vector<int> indices;
    // ADL does not work here because I is in namespace views
    std::ranges::copy(I, back_inserter(indices));
    auto J = std::ranges::subrange(begin(indices) + 20, begin(indices) + 40);
    // auto J = subrange(begin(indices) + 20, begin(indices) + 40);
    std::cout << "== J (subrange) =========================" << std::endl;
    dump(std::cout, J);
    std::cout << "Memory footprint: " << sizeof(J) << " bytes" << std::endl;
    std::cout << "Size of J: " << size(J) << std::endl;
    std::cout << "  memory footprint of actual storage: "
              << size(J) * sizeof(std::ranges::range_value_t<decltype(J)>)
              << std::endl;
    auto vJ = std::views::all(J);
    std::cout << "Memory footprint (view of J): " << sizeof(J) << " bytes"
              << std::endl;
    if (!std::ranges::equal(J, vJ)) {
        throw std::runtime_error("weird !");
    }

    auto is_multiple_of_3 = [](auto i) { return i % 3 == 0; };

    std::cout << "Multiples of 3 in J: ";
    dump(std::cout, J | std::views::filter(is_multiple_of_3));

    return 0;
}
