#include <iostream>
#include <ranges>

#include "dump.h"

using namespace std;

struct A {
   private:
    using iota = ranges::iota_view<int, int>;
    using iterator = ranges::iterator_t<iota>;
    using sentinel = ranges::sentinel_t<iota>;

   public:
    int n;
    auto begin() const { return iterator{0}; }
    auto end() const {
        return sentinel{n};
    }  // juste remplacer n par l'appel à ISet::size
};

int main() {
    std::cout << "Memory footpring of A: " << sizeof(A) << " bytes"
              << std::endl;

    auto a = A{12};
    dump(std::cout, a);

    return 0;
}
