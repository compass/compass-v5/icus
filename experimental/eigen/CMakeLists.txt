find_package(Eigen3 REQUIRED NO_MODULE)

set(EXPERIMENTS_SRC simple_sparse small_block lil_buffer)
foreach(s ${EXPERIMENTS_SRC})
  set(target experiment-eigen_with_${s})
  add_executable(${target} ${s}.cpp)
  target_link_libraries(${target} PRIVATE Eigen3::Eigen)
  add_test(NAME ${target} COMMAND ${target})
endforeach()
