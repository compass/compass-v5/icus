#include <Eigen/Dense>
#include <iostream>
#include <vector>

template <typename T, typename Index = int>
struct LilTriplet {
    Index i, j;
    T value;
};

template <typename T, int n, int m = n>
struct LilBuffer {
    using block_type = Eigen::Matrix<double, n, m, Eigen::RowMajor>;
    void clear() { buffer.clear(); }
    block_type& operator()(const int i, const int j) {
        return buffer.emplace_back(i, j, block_type::Zero()).value;
    }
    void dump() {
        for (auto&& [i, j, Bij] : buffer) {
            std::cout << "Block at (" << i << "," << j
                      << ") ==========" << std::endl;
            std::cout << Bij << std::endl;
        }
    }

   private:
    std::vector<LilTriplet<block_type>> buffer;
};

int main() {
    LilBuffer<double, 2, 2> J;
    using Block = decltype(J)::block_type;

    J(2, 4) << 1, 0, 0, 1;  // sets a identity block at location (2, 4)

    // B is a reference to 2x2 matrix located at (1,5)
    // BEWARE that as soon as another location is referenced
    // using J(i,j) the reference may become unvalidated
    // this is because we are using std::vector under the hood
    // an alternative could be to use std::list (a true list)
    auto& B = J(1, 5);
    B(0, 0) += 12;
    B(1, 0) = -1;
    B(0, 0) += 12;

    // something weird using matrix algebra
    auto& C = J(0, 0);
    C << 0, 1, 1, 0;
    C = C.inverse().eval();

    J.dump();

    return 0;
}
