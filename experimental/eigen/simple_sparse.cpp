#include <Eigen/Dense>
#include <Eigen/IterativeLinearSolvers>
#include <Eigen/Sparse>
#include <iostream>
#include <vector>

using SpMat = Eigen::SparseMatrix<double>;
using Triplet = Eigen::Triplet<double>;

int main() {
    const int n = 10;
    std::vector<Triplet> coefficients;

    for (int i = 0; i < n; ++i) {
        if (i > 0) coefficients.emplace_back(i, i - 1, 1.);
        if (i + 1 < n) coefficients.emplace_back(i, i + 1, 1.);
        coefficients.emplace_back(i, i, -2.);
    }

    SpMat A{n, n};
    A.setFromTriplets(begin(coefficients), end(coefficients));

    Eigen::VectorXd b{Eigen::VectorXd::Zero(n)};
    b(0) = 1.;

    Eigen::BiCGSTAB<SpMat> solver;
    solver.compute(A);
    solver.setMaxIterations(100);
    solver.setTolerance(1e-10);
    auto x = solver.solve(b);

    if (solver.info() != Eigen::Success) {
        std::cerr << "Something went wrong!!!" << std::endl;
    }

    std::cout << "  nb iterations: " << solver.iterations() << std::endl;
    std::cout << "estimated error: " << solver.error() << std::endl;

    return 0;
}
