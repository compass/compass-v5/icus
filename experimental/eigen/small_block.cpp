#include <Eigen/Dense>
#include <iostream>

constexpr int n = 2;
constexpr int m = 2;
using Block = Eigen::Matrix<double, n, m, Eigen::RowMajor>;

int main() {
    std::cout << "block " << n << "x" << m << std::endl;
    std::cout << "block memory footprint: " << sizeof(Block) << " bytes"
              << std::endl;
    std::cout << "block equivalent array footprint: "
              << n * m * sizeof(typename Block::Scalar) << " bytes"
              << std::endl;

    Block A;
    auto p = A.data();
    for (auto&& x : {1, 2, 3, 4}) {
        *p = x;
        ++p;
    }

    std::cout << A << std::endl;

    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < m; ++j) {
            std::cout << "A(" << i << "," << j << ") = " << A(i, j)
                      << std::endl;
        }
    }

    return 0;
}
