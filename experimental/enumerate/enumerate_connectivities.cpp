#include <icus/Connectivity.h>
#include <icus/ISet.h>

#include <iostream>
#include <range/v3/all.hpp>  // get everything
#include <string>

using std::cout;

struct A {
    using value_type = icus::Simple_integer_iterator::value_type;
    using reference_type = const value_type &;
    using pointer_type = const value_type *;
    auto begin() { return icus::Simple_integer_iterator{0}; }
    auto end() { return icus::Simple_integer_iterator{10}; }
};

int main() {
    auto vertices = icus::make_iset(4);
    auto con = icus::Connectivity(
        vertices,         // source ISet
        vertices,         // target ISet (same in this case)
        {0, 3, 4, 6, 8},  // offsets
        {1, 2, 0, 3, 0, 3, 1, 2}
        // connections such that connections[offsets[i]:offsets[i+1]]
        // = vertices that are connected to vertex i
    );

    // Iterate over source
    for (auto &&i : con.source) {
        cout << i << " ";
    }
    cout << std::endl;

    std::vector<icus::index_type> v;
    // FIXME: the following fails if the iterator type is not complete
    std::copy(begin(vertices), end(vertices), back_inserter(v));
    // alternative:
    // for (auto &&i : vertices) {
    //   v.emplace_back(i);
    // }

    for (auto &&[i, j] : ranges::views::zip(v, v)) {
        assert(i == j);
        cout << i << " and " << j << std::endl;
    }

    // FIXME: the following fails because it's likely that vertices
    //        does not satisfy the range concept
    // for (auto &&[i, j] : ranges::views::zip(*vertices, *vertices)) {
    //   assert(i == j);
    //   cout << i << " and " << j << std::endl;
    // }

    // FIXME: This is the ultimate target !
    //   for(auto&& [i, neighbors]: ranges::views::zip(con.source, con)) {
    //      cout << i << " has neighbors: ";
    //      for(auto&& j: neighbors) cout << j << " ";
    //      cout << std::endl;
    //   }
    // A current workaround relying on an ad-hoc definition of
    // Connectivity_iterator
    for (auto &&[i, neighbors] : con) {
        // FIXME: the use of "->" here is very annoying
        //        it is NOT what one would expect
        cout << i << " has " << neighbors->size() << " neighbors: ";
        for (auto &&j : neighbors) cout << j << " ";
        cout << std::endl;
    }
  auto con = icus::Connectivity(
      vertices,        // source ISet
      vertices,        // target ISet (same in this case)
      {0, 3, 4, 6, 8}, // offsets
      {1, 2, 0, 3, 0, 3, 1, 2}
  cout << "For each: ";
  std::string s{"abcde"};
  ranges::for_each(s, [](auto &&i) {
        cout << i << " "; });
  cout << std::endl;

  cout << "For each: ";
  // ranges::for_each(vertices, [](auto &&i) { cout << i << " "; });
  A a;
  ranges::for_each(a, [](auto &&i) {
        cout << i << " "; });
  cout << std::endl;

  // // Iterate over source
  // for (auto &&i : con.source) {
  //   cout << i << " ";
  // }
  // cout << std::endl;

  // std::vector<icus::index_type> v;
  // // FIXME: the following fails if the iterator type is not complete
  // std::copy(begin(vertices), end(vertices), back_inserter(v));
  // // alternative:
  // // for (auto &&i : vertices) {
  // //   v.emplace_back(i);
  // // }

  // for (auto &&[i, j] : ranges::views::zip(v, v)) {
  //   assert(i == j);
  //   cout << i << " and " << j << std::endl;
  // }

  // // FIXME: the following fails because it's likely that vertices
  // //        does not satisfy the range concept
  // // for (auto &&[i, j] : ranges::views::zip(*vertices, *vertices)) {
  // //   assert(i == j);
  // //   cout << i << " and " << j << std::endl;
  // // }

  // // FIXME: This is the ultimate target !
  // //   for(auto&& [i, neighbors]: ranges::views::zip(con.source, con)) {
  // //      cout << i << " has neighbors: ";
  // //      for(auto&& j: neighbors) cout << j << " ";
  // //      cout << std::endl;
  // //   }
  // // A current workaround relying on an ad-hoc definition of
  // // Connectivity_iterator
  // for (auto &&[i, neighbors] : con) {
  //   // FIXME: the use of "->" here is very annoying
  //   //        it is NOT what one would expect
  //   cout << i << " has " << neighbors->size() << " neighbors: ";
  //   for (auto &&j : neighbors)
  //     cout << j << " ";
  //   cout << std::endl;
  // }
}
