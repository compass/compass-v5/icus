
#include "ISet.h"

// iterer sur des iset
// iterer sur la base des isets
// rebase: renvoit un nouvel iset
// common_base
// union

// puis emballage python
int main() {
    // ceci est un pointeur...
    auto I = icus::make_iset(10);

    // en d�referencant le pointeur
    for (auto &&i : *I) {
        std::cout << i << std::endl;
    }

    // sans d�referencer le pointeur grace � la definition de begin/end en
    // fonctions globales
    for (auto &&i : I) {
        std::cout << i << std::endl;
    }

    std::cout << "Playing with base ----------------- " << std::endl;

    auto J = icus::make_iset(5, I, {{1, 3, 5, 2, 8}});

    std::cout << "J: ";
    for (auto &&j : J) {
        std::cout << j << " ";
    }
    std::cout << std::endl;
    std::cout << "J (base):";
    for (auto &&j : base(J)) {
        std::cout << j << " ";
    }
    std::cout << std::endl;
}
