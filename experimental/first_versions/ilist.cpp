#include <cassert>
#include <iostream>
#include <variant>
#include <vector>

using Indices = std::vector<int>;

struct FancyIterator {
    using indices_iterator = Indices::const_iterator;
    using base = std::variant<int, indices_iterator>;
    base p;
    auto operator*() {
        return std::visit(
            [](auto &&i) -> int {
                using I = std::decay_t<decltype(i)>;
                if constexpr (std::is_same_v<I, int>) return i;
                if constexpr (std::is_same_v<I, indices_iterator>) return *i;
                assert(false);
                return 0;
            },
            p);
    }
    FancyIterator &operator++() {
        std::visit([](auto &&i) { ++i; }, p);
        return *this;
    }
    FancyIterator operator++(int) {
        auto pre_increment = FancyIterator{*this};
        std::visit([](auto &&i) { ++i; }, p);
        return pre_increment;
    }
    bool operator!=(const FancyIterator &other) const {
        return std::visit(
            [](auto &&i, auto &&j) -> bool {
                using I = std::decay_t<decltype(i)>;
                using J = std::decay_t<decltype(j)>;
                if constexpr (std::is_same_v<I, J>) return i != j;
                assert(false);  // choux et carottes
                return true;
            },
            p, other.p);
    }
};

struct IndexList {
    std::variant<int, Indices> indices;
    auto size() const {  // el cardinal gringo!
        return std::visit(
            [](auto &&v) -> int {
                using T = std::decay_t<decltype(v)>;
                if constexpr (std::is_same_v<T, int>) return v;
                if constexpr (std::is_same_v<T, Indices>) return v.size();
                assert(false);
                return 0;
            },
            indices);
    }
    auto begin() const {
        return std::visit(
            [](auto &&v) -> FancyIterator {
                using T = std::decay_t<decltype(v)>;
                if constexpr (std::is_same_v<T, Indices>)
                    return FancyIterator{v.begin()};
                return FancyIterator{0};
            },
            indices);
    }
    auto end() const {
        return std::visit(
            [](auto &&v) -> FancyIterator {
                using T = std::decay_t<decltype(v)>;
                if constexpr (std::is_same_v<T, int>) return FancyIterator{v};
                if constexpr (std::is_same_v<T, Indices>)
                    return FancyIterator{v.end()};
                return FancyIterator{0};
            },
            indices);
    }
};

std::ostream &operator<<(std::ostream &os, const IndexList &I) {
    os << "set with " << I.size() << " elements:";
    for (auto &&i : I) {
        os << " " << i;
    }
    return os;
}

int main() {
    auto I = IndexList{10};
    std::cout << I << std::endl;
    auto J = IndexList{Indices{{1, 4, 2, 6}}};
    std::cout << J << std::endl;
}
