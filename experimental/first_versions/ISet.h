#pragma once

#include <algorithm>
#include <cassert>
#include <iostream>
#include <memory>
#include <optional>
#include <stdexcept>
#include <string>
#include <vector>

namespace icus {

struct Simple_integer_iterator {
    int value;
    auto operator*() { return value; }
    Simple_integer_iterator &operator++() {
        ++value;
        return *this;
    }
    Simple_integer_iterator operator++(int) {
        auto pre_increment = Simple_integer_iterator{*this};
        ++value;
        return pre_increment;
    }
    bool operator==(const Simple_integer_iterator &other) const {
        return value == other.value;
    }
    bool operator!=(const Simple_integer_iterator &other) const {
        return value != other.value;
    }
    auto operator-(const Simple_integer_iterator &other) const {
        return Simple_integer_iterator{value - other.value};
    }
};

// ISets are managed by std::shared_ptr under the hood
// https://en.cppreference.com/w/cpp/memory/enable_shared_from_this
struct ISet : std::enable_shared_from_this<ISet> {
    using sptr = std::shared_ptr<ISet>;
    using indices = std::vector<int>;
    using iterator = Simple_integer_iterator;
    //
    // -- The data --
    //
    struct Base {
        sptr is;  // j'aime pas is ca fait "to be"
        indices mapping;
    };
    int size;  // � remplacer par _size pour avoir une fonction size() utilis�e
               // par certaines fonctions de la STL ?
    std::optional<Base> _base;
    //
    // -- range API --
    //
    iterator begin() const { return iterator{0}; }
    iterator end() const { return iterator{size}; }
    //
    // -- base related operations --
    //
    void throw_if_no_base(const std::string &message = "") const {
        if (!_base) {
            if (message.empty()) {
                throw std::runtime_error("ISet has no base!");
            }
            throw std::runtime_error(message);
        }
    }
    const indices &base() const {
        throw_if_no_base();
        return _base->mapping;
    }
    indices base(const sptr &parent) const {
        throw_if_no_base();
        auto new_base = _base->mapping;  // copy
        for (auto level = _base->is; level != parent; level = _base->is) {
            level->throw_if_no_base("Parent not found!");
            const auto &level_map = level->_base->mapping;
            for (auto &&k : new_base) {
                k = level_map[k];
            }
        }
        return new_base;
    }
    //
    // -- construction --
    //
    // nodisard so that the returned pointer is actually used
    [[nodiscard]] static sptr make(int n) {
        // Not using std::make_shared<ISet> because the constructor is private
        // and cannot be called from std::shared_ptr<ISet>
        return std::shared_ptr<ISet>{new ISet(n)};
    }
    [[nodiscard]] static sptr make(int n, sptr super, indices &&ijk) {
        return std::shared_ptr<ISet>{
            new ISet(n, super, std::forward<indices>(ijk))};
    }
    [[nodiscard]] static sptr make(int n, sptr super, const indices &ijk) {
        return std::shared_ptr<ISet>{new ISet(n, super, ijk)};
    }

   private:
    // Constructor are made private
    ISet(int n) : size{n} { assert(n > 0); }
    // Move constructor for std::vector (to avoid copy if possible)
    ISet(int n, sptr super, indices &&ijk)
        : size{n}, _base{{super, std::forward<indices>(ijk)}} {
        assert(n > 0);
        assert(ijk.size() == static_cast<std::size_t>(n));
        assert(std::all_of(std::begin(ijk), std::end(ijk),
                           [n](auto i) { return (0 <= i) && (i < n); }));
    }
    ISet(int n, sptr super, const indices &ijk) : size{n}, _base{{super, ijk}} {
        assert(n > 0);
        assert(ijk.size() == static_cast<std::size_t>(n));
        assert(std::all_of(std::begin(ijk), std::end(ijk),
                           [n](auto i) { return (0 <= i) && (i < n); }));
    }
};

auto make_iset(int n) { return ISet::make(n); }
auto make_iset(int n, ISet::sptr super, ISet::indices &&ijk) {
    return ISet::make(n, super, std::forward<ISet::indices>(ijk));
}
auto make_iset(int n, ISet::sptr super, const ISet::indices &ijk) {
    return ISet::make(n, super, ijk);
}

// permet d'utiliser les fonctions sans d�referencer le pointeur
auto begin(const ISet::sptr &sp) {
    assert(sp);
    return sp->begin();
}
auto end(const ISet::sptr &sp) {
    assert(sp);
    return sp->end();
}
auto base(const ISet::sptr &sp) {
    assert(sp);
    return sp->base();
}
auto base(const ISet::sptr &sp, const ISet::sptr &parent) {
    assert(sp);
    return sp->base(parent);
}

}  // namespace icus
