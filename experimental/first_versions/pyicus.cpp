#include <pybind11/pybind11.h>

#include "ISet.h"

namespace py = pybind11;

PYBIND11_MODULE(icus, module) {
    using namespace icus;

    // cf.
    // https://pybind11.readthedocs.io/en/stable/advanced/smart_ptrs.html#std-shared-ptr
    auto pyclass = py::class_<ISet, ISet::sptr>{module, "ISet"};
    pyclass.def(py::init(py::overload_cast<int>(&make_iset)));
    pyclass.def("__iter__", [](const ISet::sptr &self) {
        return py::make_iterator(begin(self), end(self));
    });
}
