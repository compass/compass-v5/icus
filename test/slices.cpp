#include <icus/Slice.h>
#include <icus/types.h>

#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <ranges>

TEST_CASE("Slices") {
    using namespace icus;

    CHECK(std::forward_iterator<icus::Slice<index_type>::iterator>);
    CHECK(std::ranges::range<icus::Slice<index_type>>);
    CHECK(std::ranges::equal(Slice<index_type>{2, 7}, std::views::iota(2, 7)));
}
