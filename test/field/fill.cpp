#include <catch2/catch_test_macros.hpp>

#include "icus/Field.h"

TEST_CASE("Filling fields") {
    using namespace icus;
    using namespace std::views;

    constexpr int n = 10;

    const ISet I{n};
    auto F = make_field(I).fill(iota(0, n));
    auto J = I.extract(iota(0, n) | stride(2));
    CHECK(J.size() == n / 2);

    const ISet L{4};
    auto K = L.extract(iota(0, 2));
    std::vector<std::array<double, 2>> values{{1., 2.}, {3., 4.}};
    auto Fpos = make_field<std::array<double, 2>>(K).fill(values);
    auto Fpos1 = make_field<std::array<double, 2>>(L);
    Fpos1.view(K).fill(Fpos);
}
