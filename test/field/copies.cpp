#include <catch2/catch_test_macros.hpp>

#include "icus/Field.h"

TEST_CASE("Copy field") {
    using namespace icus;
    using ints = std::vector<int>;
    using reals = std::vector<double>;

    constexpr int n = 4;
    const ISet I{n}, J{n};
    auto K = I.extract(0, n);
    CHECK(I == K);  // I and K are are actually referencing the same elements
    CHECK(I != J);

    K = J;
    auto color_K = make_field(K, ints{4, 5, 6, 7});
    K = I;
    CHECK(I == K);  // K is not const and can thus be assigned I
    CHECK(color_K.support == J);
    CHECK(color_K.support != I);
    // color_K.support still points towards the old memory address of K,
    // not the new one - EXPEXCTED

    auto color_I = make_field(I, ints{0, 1, 2, 3});
    color_I = make_field(J, ints{4, 5, 6, 7});
    CHECK(color_I.support == J);
    CHECK(color_I == ints{4, 5, 6, 7});
    // Same values type before and after the second make_field instruction
    // --> pointer copy, the support and values of color_I are modified -
    // EXPECTED
    // color_I = make_field(I); // fails because default field type is double
    color_I = make_field<int>(I);
    CHECK(color_I.support == I);
    // Different values type before and after the third make_field instruction
    // (default type of make_field is double) --> copy-and-convert values, but
    // do not modify the support

    auto color_I2 = make_field(I);
    color_I2 = make_field(J);
    CHECK(color_I2.support == J);
    // color_I2 = make_field(I, ints{0, 1, 2, 3}); // fails because type
    // mismatch
    color_I2 = make_field(I, reals{0, 1, 2, 3});
    CHECK(color_I2.support == I);
    CHECK(color_I2 == reals{0, 1, 2, 3});
    // The second make_field instruction does change
    // the support. The third one only change the
    // values and not the support (same reason as above)

    // auto color_I3 = make_field(I, ints{0, 1, 2, 3});
    // auto color_J = make_field(J, ints{4, 5, 6, 7});
    // color_J = color_I3;
    // CHECK(color_J.support == color_I3.support);
    // CHECK(color_J == color_I3);
    // color_J.fill(ints{4, 5, 6, 7});
    // CHECK(color_I3 == color_J);
    // color_J.support = J;
    // CHECK(color_I3.support != color_J.support);
    // Copy by reference : shared memory for the values
    // of fields color_I and color_J BUT NOT for their support

    // auto color_I4 = make_field(I, ints{0, 1, 2, 3});
    // auto color_J2 = color_I4.share(J);
    // CHECK(color_I4.support == I);
    // CHECK(color_J2.support == J);
    // CHECK(color_J2 == color_I4);
    // color_J2 = ints{4, 5, 6, 7};
    // CHECK(color_J2 == color_I4);
    // // Shared memory for the values of color_I and color_J
    // // (but not for the support)

    // auto color_J3 = color_I4.copy();
    // CHECK(color_J3.support == I);
    // CHECK(color_J3 == color_I4);
    // color_J3 = ints{0, 1, 2, 3};
    // CHECK(color_I4 != color_J3);
    // color_J3.support = J;
    // CHECK(color_J3.support != color_I4.support);
    // // color_J is initialized with the support and values of
    // // color_I but their memory is not shared

    // auto color_J4 = color_I4.copy(J);
    // CHECK(color_J4.support == J);
    // CHECK(color_J4 == color_I4);
    // // color_J is initialized with support J and the values of color_I

    // auto color_J5 = make_field(J);
    // color_J5 = color_I4.copy();
    // CHECK(color_I4.support == I);
    // CHECK(color_J5.support == J);
    // CHECK(color_J5 == color_I4);
    // // Same behaviour than make_field : color_I has type "int"
    // // and color_J has default type "double", so instruction
    // // color_I.copy() only copy the values (with type
    // // conversion) but forgot about the support...
}
