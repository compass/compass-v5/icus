#include <catch2/catch_test_macros.hpp>

#include "icus/Field.h"

TEST_CASE("Field enumeration") {
    using namespace icus;
    using std::views::iota;

    auto I = ISet(4);
    auto color = make_field<int>(I).fill(iota(0, (int)I.size()));
    int k = 0;
    for (auto&& [i, c] : color.enumerate()) {
        CHECK(i == (index_type)k);
        CHECK(c == k);
        ++k;
    }
    k = 0;
    for (auto&& [i, c] : enumerate(color)) {
        CHECK(i == (index_type)k);
        CHECK(c == k);
        ++k;
    }
}
