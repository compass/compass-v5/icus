#include <catch2/catch_test_macros.hpp>

#include "icus/Field.h"

TEST_CASE("Make field") {
    using namespace icus;
    using std::views::iota;
    using std::views::zip;
    using ints = std::initializer_list<int>;
    using reals = std::initializer_list<double>;

    constexpr int n = 4;
    auto I = ISet(n);
    auto color = make_field(I, ints{0, 1, 2, 3});
    static_assert(std::is_same_v<typename decltype(color)::value_type, int>);
    for (auto&& [i, c] : zip(iota(0, (int)n), color)) {
        CHECK(i == c);
    }
    auto numbers = make_field(I, reals{0, 1, 2, 3});
    static_assert(
        std::is_same_v<typename decltype(numbers)::value_type, double>);
}
