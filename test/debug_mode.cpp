// Because we use C assertions to test things in
// the sources files, the NDEBUG macro must NOT
// be defined. Because if NDEBUG is defined as a
// macro name then assert does nothing
// cf. https://en.cppreference.com/w/cpp/error/assert

#include <catch2/catch_test_macros.hpp>

TEST_CASE("debug_mode") {
#ifdef NDEBUG
    CHECK(false);
#else
    CHECK(true);
#endif
}
