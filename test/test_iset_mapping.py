import numpy as np
from icus import ISet
import pytest


def test_modify_mapping():
    I = ISet(10)
    J = I.extract(np.arange(0, 10, 2))
    m = J.mapping()
    assert np.all(m == np.arange(0, 10, 2))
    # Mapping does not support item assignment
    with pytest.raises(TypeError):
        m[0] = 42
    a = J.mapping_as_array()
    assert not a.flags["WRITEABLE"]
    with pytest.raises(ValueError):
        a[0] = 42
