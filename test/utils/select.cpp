#include "icus/utils/select.h"

#include <catch2/catch_test_macros.hpp>

#include "icus/Connectivity.h"

TEST_CASE("Select field color") {
    using namespace icus;
    using std::views::iota;

    auto I = ISet(4);
    auto J = ISet(3);
    std::vector<std::vector<index_type>> targets{{0, 1, 3}, {1, 0}, {2, 1}};
    auto JxI = make_connectivity(J, I, targets);
    auto I_color = make_field<int>(I).fill(iota(0, (int)I.size()));
    auto J_color = icus::select(I_color, JxI);
    CHECK(J_color.support == J);
    CHECK(J_color(0) == 0);
    CHECK(J_color(1) == 0);
    CHECK(J_color(2) == 1);
}
