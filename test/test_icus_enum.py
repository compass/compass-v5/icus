import numpy as np
import icus
import icus_field_demo as demo


def test_enum_site():
    BC = demo.BC
    n = 10
    I = icus.ISet(n)
    F = icus.field(I, demo.Site)
    p = F.as_array()
    p.pressure[:] = BC.neumann
    p.temperature[:] = BC.dirichlet
    p.pressure[1] = BC.dirichlet
    pressure_dirichlet = p.pressure == BC.dirichlet
    assert pressure_dirichlet[1] == True
    assert not np.any(pressure_dirichlet[2:])
    print(p[1].temperature)
    p[1].temperature = BC.neumann
    assert p[1].temperature == BC.neumann
    # values are int on the python side
    # so that they can be used to access element in arrays
    assert BC.dirichlet == 0
    print(p[BC.dirichlet])  # nonsense but just to showcase


def test_enum():
    I = icus.ISet(10)
    p = icus.field(I, demo.MyEnum)
    E = demo.MyEnum
    A, B, C = E.A, E.B, E.C
    p[:] = np.tile(E.A, I.size())
    p[0] = E(B)  # converts the integer value to a C++ enum struct
    assert p[0] == B
    p[0] = B  # ok too (thanks to operator overloading)
    assert p[0] == B
    p[I] = E(A)
    p[I] = A
    assert np.all(p == A)
    p[:] = B  # (slightly) more efficient
    assert np.all(p == B)
    p[...] = C  # as p[:]
    assert np.all(p == C)
    p[0] = B
    assert p[0] == B
    p[2] = A
    assert p[0] == B and p[1] == C and p[2] == A
    p[:] = A
    J = I.extract([1, 2, 3])
    p[J] = C
    assert p[1] == C and p[2] == C and p[3] == C
    a = p.as_array()
    assert a[0] == A
    assert a[1] == C
    assert p.dtype == demo.MyEnum
