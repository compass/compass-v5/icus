## Plot into a png file the solution of the 1D heat equation
## computed by either solve_1Dheat.cpp or test_solve_1Dheat.py
import numpy as np
import matplotlib.pyplot as plt

dx = 2.0 / 100  # change to the value used in the solver
inputfile = f"approx_solution_{dx:g}.dat"
outputfile = f"approx_solution_{dx:g}.png"

x, dx, sol_app, sol_ex = np.genfromtxt(
    inputfile, delimiter=",", unpack=True, skip_header=1
)
x_min, x_max = 0, np.sum(dx)

plt.figure(figsize=(4, 4))
plt.plot(x, sol_app, "-r", label="approximate")
plt.plot(x, sol_ex, "--k", label="exact")
plt.xlim([x_min, x_max])
plt.xlabel("Position")
plt.ylabel("Temperature (°C)")
plt.title("Solution of the 1D heat equation\n using icus and TPFA scheme")
plt.legend(loc="best")

plt.savefig(outputfile)
