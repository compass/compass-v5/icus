## This script compute the solution of the 1D heat problem and return
## the infinity norm error with respect to the exact solution. Optionnally,
## the exact and approximate solution can be written to a file (using
## function write_to_file) to be later plotted using the script
## plot_solution.py.
from icus import ISet, field, Connectivity, make_connectivity
import numpy as np
import math


## Generate random cell width according to a uniform law in the range [w-dw , w+dw]
def random_cells_width(cells, w, dw):
    assert w >= 0
    assert (dw >= 0) & (dw < w)

    rng = np.random.default_rng()
    cells_width = field(cells)
    cells_width[:] = w + dw * rng.uniform(-1, 1, cells.size())
    for i in cells:
        assert (cells_width[i] >= w - dw) & (cells_width[i] <= w + dw)

    return cells_width


## Compute coordinates of the cells center based on cells width
def cells_center_from_width(cells_width):
    cells_center = field(cells_width.support())
    for i in cells_center.support():
        if i == 0:
            cells_center[i] = cells_width[i] / 2.0
        else:
            cells_center[i] = (
                cells_center[i - 1] + (cells_width[i - 1] + cells_width[i]) / 2.0
            )
    return cells_center


## Compute position of the faces based on cells width
def faces_x_from_cells_width(faces, cells_width):
    assert faces.size() - 1 == cells_width.size()

    faces_x = field(faces)
    for i in faces:
        if i == 0:
            faces_x[i] = 0.0
        else:
            faces_x[i] = faces_x[i - 1] + cells_width[i - 1]
    return faces_x


## Create an icus field with support I, containing the values of F shuffled according to the vector shf
def shuffle_field(I, F, shf):
    # Check that arguments have compatible sizes
    assert I.size() == F.size()
    assert I.size() == len(shf)

    F_shuffled = field(I)
    for i in I:
        F_shuffled[shf[i]] = F[i]

    assert F_shuffled.support() == I
    for i in I:
        assert F_shuffled[shf[i]] == F[i]

    return F_shuffled


## Reconstruct the list of neighbours according to the shuffle/reordering
## sources --> shf_sources   and   targets --> shf_targets
def shuffle_neighbours(neighbours, shf_sources, shf_targets):
    assert len(neighbours) == len(shf_sources)

    neighbours_shuffled = []
    # use argsort to invert the permutation shf_sources
    for i in np.argsort(shf_sources):
        targets = neighbours[i]
        assert (max(targets) < len(shf_targets)) & (min(targets) >= 0)
        neighbours_shuffled.append([shf_targets[j] for j in targets])

    return neighbours_shuffled


## Print mesh characteristics
def print_mesh(
    title,
    cells_width,
    cells_center,
    faces_x,
    faces_interior,
    faces_wall,
    cells_faces,
    walls_cells,
    intfaces_cells,
):
    print(title)
    print("Cells width :", *[round(i, 5) for i in cells_width])
    print("Cells center :", *[round(i, 5) for i in cells_center])
    print("Faces position :", *[round(i, 5) for i in faces_x])
    print("Interior faces :", *faces_interior.mapping())
    print("Exterior (wall) faces :", *faces_wall.mapping())
    print("Connections :")
    for i, targets in enumerate(cells_faces.as_array()):
        print("     cell", i, "is connected to faces", *targets)
    for i, j in enumerate(faces_wall.mapping()):
        targets = walls_cells.as_array()[i]
        t = targets[0]
        print(
            "     wall face",
            j,
            "is connected to cell",
            *targets,
            "at position",
            cells_center[t],
        )
    for i, j in enumerate(faces_interior.mapping()):
        targets = intfaces_cells.as_array()[i]
        print("     interior face", j, "is connected to cells", *targets)


## Write solution to file. The data in the file thus created can be plotted
## using the script plot_solution.py
def write_to_file(cells_center, cells_width, cells_Tapprox, cells_Texact, dx):
    filename = f"./test/1D_heat/approx_solution_{dx:g}.dat"
    solfile = open(filename, "w")

    solfile.write(
        "# cell center x_i, cell width dx_i, approximate cell temperature T_i, exact cell temperature T(x_i)\n"
    )
    for i in cells_center.support():
        solfile.write(
            f"{cells_center[i]:g}, {cells_width[i]:g}, {cells_Tapprox[i]:g}, {cells_Texact[i]:g}\n"
        )

    solfile.close()


## Solve the 1D heat problem for a spatial domain discretized into Nx cells of
## average width dx, with source term corresponding to a known exact solution
## either linear or sinusoidal.
## Return the infinity norm error between the exact and approximate solutions
## and, if write_sol=True, write both solutions to a data file to enable future
## plotting by script plot_solution.py.
def solve_1D_heat_equation(Nx, dx, sol_exact, source_term, write_sol=False):
    ## Create a trivially ordered mesh using icus : Nx cells and Nx+1 faces
    cells_sorted = ISet(Nx)
    faces_sorted = ISet(Nx + 1)
    faces_sorted_interior = faces_sorted.extract(np.arange(1, faces_sorted.size() - 1))
    faces_sorted_wall = faces_sorted.substract(faces_sorted_interior)
    assert faces_sorted_wall.size() == 2  # only be two walls in 1D

    ## Create connectivities for this ordered mesh
    cells_faces_sorted = make_connectivity(
        cells_sorted, faces_sorted, [[i, i + 1] for i in range(cells_sorted.size())]
    )
    faces_cells_sorted = cells_faces_sorted.transpose()
    intfaces_cells_sorted = faces_cells_sorted.extract(2, faces_sorted_interior)
    walls_cells_sorted = faces_cells_sorted.extract(1, faces_sorted_wall)
    assert cells_faces_sorted.is_regular
    assert intfaces_cells_sorted.is_regular
    assert walls_cells_sorted.is_regular

    ## Generate uniform cells width (default)
    # auto cells_sorted_width = field(cells_sorted) = dx
    # double domain_length_exact = Nx*dx

    ## Generate random cell width in the range [0.9*dx , 1.1*dx]
    cells_sorted_width = random_cells_width(cells_sorted, dx, 0.1 * dx)
    domain_length_exact = 0
    for i in cells_sorted:
        domain_length_exact += cells_sorted_width[i]

    ## Compute the coordinates of the cells center based on cells width
    cells_sorted_center = cells_center_from_width(cells_sorted_width)

    ## Compute the coordinates of the faces based on cells width
    faces_sorted_x = faces_x_from_cells_width(faces_sorted, cells_sorted_width)
    assert faces_sorted_x[faces_sorted.size() - 1] == domain_length_exact

    ## Generate dirichlet boundary condition on the wall faces
    T_left = sol_exact(0, domain_length_exact)
    T_right = sol_exact(domain_length_exact, domain_length_exact)
    faces_sorted_wall_Temp = field(faces_sorted_wall)
    faces_sorted_wall_Temp[:] = np.array([T_left, T_right])

    ## Set constant uniform conductivity inside the domain
    cells_sorted_conductivity = field(cells_sorted)
    cells_sorted_conductivity[:] = np.ones(cells_sorted.size())

    ## Shuffle all cells to simulate randomness of mesh indices
    shf_cells = np.random.permutation(cells_sorted.size())  # shuffling vector
    cells = ISet(cells_sorted.size())
    cells_width = shuffle_field(cells, cells_sorted_width, shf_cells)
    cells_center = shuffle_field(cells, cells_sorted_center, shf_cells)
    cells_conductivity = shuffle_field(cells, cells_sorted_conductivity, shf_cells)

    ## Shuffle all faces to simulate randomness of mesh indices
    shf_faces = np.random.permutation(faces_sorted.size())  # shuffling vector
    faces = ISet(faces_sorted.size())
    faces_interior = faces.extract(
        np.sort(shf_faces[[*faces_sorted_interior.mapping()]])
    )  # NB : np.sort isn't strictly necessary, only for cosmetic purpose when printing mesh
    faces_wall = faces.substract(faces_interior)
    assert faces_wall.size() == 2

    faces_x = shuffle_field(faces, faces_sorted_x, shf_faces)
    faces_wall_Temp = shuffle_field(
        faces_wall,
        faces_sorted_wall_Temp,
        np.argsort(shf_faces[[*faces_sorted_wall.mapping()]]),
    )  # NB : np.argsort "renumber" the sub-permutation continuously starting from 0

    ## Create connectivity between shuffled cells and faces
    neigh = shuffle_neighbours(
        [[i, i + 1] for i in range(cells.size())], shf_cells, shf_faces
    )
    cells_faces = make_connectivity(cells, faces, neigh)
    faces_cells = cells_faces.transpose()
    intfaces_cells = faces_cells.extract(2, faces_interior)
    walls_cells = faces_cells.extract(1, faces_wall)
    assert cells_faces.is_regular
    assert intfaces_cells.is_regular
    assert walls_cells.is_regular

    ## Print meshes to check their validity
    # print_mesh("--- Sorted mesh ---", cells_sorted_width, cells_sorted_center,
    #           faces_sorted_x, faces_sorted_interior, faces_sorted_wall,
    #           cells_faces_sorted, walls_cells_sorted, intfaces_cells_sorted)
    # print_mesh("--- Shuffled mesh ---", cells_width, cells_center,
    #           faces_x, faces_interior, faces_wall,
    #           cells_faces, walls_cells, intfaces_cells)

    ## Create jacobian matrix and RHS vector for the stationnary 1D heat equation
    Jacobian = np.zeros((cells.size(), cells.size()))
    RHS = np.zeros(cells.size())
    cells_Texact = field(cells)
    cells_Tapprox = field(cells)

    # Discretized exact solution and corresponding source term
    for i in cells:
        cells_Texact[i] = sol_exact(cells_center[i], domain_length_exact)
        RHS[i] = cells_conductivity[i] * source_term(
            cells_center[i], domain_length_exact
        )

    # Core of the jacobian matrix
    for i, neighbours in enumerate(intfaces_cells.as_array()):
        # List cells on each side of face i
        assert len(neighbours) == 2
        K = neighbours[0]
        L = neighbours[1]

        # Compute transmissivity coeff :
        #       harmonic mean conductivity of both cells
        #       divided by distance between the two cell centers
        a_K = cells_conductivity[K] / cells_width[K]
        a_L = cells_conductivity[L] / cells_width[L]
        C_KL = 2.0 * a_K * a_L / (a_K + a_L)

        # Fill core of the jacobian matrix
        Jacobian[K, K] += C_KL / cells_width[K]
        Jacobian[K, L] -= C_KL / cells_width[K]
        Jacobian[L, L] += C_KL / cells_width[L]
        Jacobian[L, K] -= C_KL / cells_width[L]

    # Boundary conditions
    for i, neighbours in enumerate(walls_cells.as_array()):
        # Find cell touching the wall i
        assert len(neighbours) == 1
        K = neighbours[0]

        # Compute transmissivity coeff : cell conductivity divided by the cell-wall distance
        C_Kwall = 2.0 * cells_conductivity[K] / cells_width[K]

        # Account for boundary conditions in Jacobian and RHS
        Jacobian[K, K] += C_Kwall / cells_width[K]
        RHS[K] += C_Kwall * faces_wall_Temp[i] / cells_width[K]

    ## Solve stationnary problem and compare with analytical solution
    cells_Tapprox[:] = np.linalg.solve(Jacobian, RHS)
    error = 0
    for i in cells:
        error = max(error, abs(cells_Tapprox[i] - cells_Texact[i]))

    ## Sort solution and write it in a text file
    if write_sol:
        cells_sorted_Texact = field(cells_sorted)
        cells_sorted_Tapprox = field(cells_sorted)
        for i in cells_sorted:
            cells_sorted_Texact[i] = cells_Texact[shf_cells[i]]
            cells_sorted_Tapprox[i] = cells_Tapprox[shf_cells[i]]
        write_to_file(
            cells_sorted_center,
            cells_sorted_width,
            cells_sorted_Tapprox,
            cells_sorted_Texact,
            dx,
        )

    return error


## Compute best fitting linear function for data (x,y) using Least Square
def linear_regression(x, y):
    assert len(x) == len(y)
    N = len(x)

    sum_x = np.sum(x)
    sum_y = np.sum(y)
    sum_xy = x.dot(y)
    sum_x_square = x.dot(x)

    denominator = N * sum_x_square - sum_x * sum_x
    coeff = (N * sum_xy - sum_x * sum_y) / denominator
    constTerm = (sum_y * sum_x_square - sum_x * sum_xy) / denominator

    SSE = np.sum((coeff * x + constTerm - y) ** 2)  # sum of squared errors
    RMSE = (SSE / N) ** 0.5  # root mean square error

    return coeff, constTerm, RMSE


## Compute best fitting power law function for data (x,y) using log10 conversion
def powerlaw_regression(x, y):
    assert len(x) == len(y)
    assert np.all(x > 0) & np.all(y > 0)
    N = len(x)

    power, coeff, _ = linear_regression(np.log10(x), np.log10(y))
    coeff = 10**coeff

    SSE = np.sum((coeff * x**power - y) ** 2)  # sum of squared errors
    RMSE = (SSE / N) ** 0.5  # root mean square error

    return power, coeff, RMSE


def test_solve_1Dheat_using_icus():
    ## Define problem constants
    domain_length = 2  # length of the spatial domain
    T_left = 0  # boundary condition on the left
    T_right = 10  # boundary condition on the right
    linear_func = lambda x, L: T_left + (T_right - T_left) * x / L
    linear_func_dd = lambda x, L: 0.0
    sinus_func = lambda x, L: T_left + (T_right - T_left) * np.sin(np.pi * x / (2 * L))
    sinus_func_dd = (
        lambda x, L: (T_right - T_left)
        * (np.pi / (2 * L)) ** 2
        * np.sin(np.pi * x / (2 * L))
    )

    ## Check that the approximate solution is (quasi) exact in the linear case
    Nx = 100
    # NB : the number of cells in the spatial discretisation must be at least
    # equal to 2, otherwise there is no "interior face" and the code crash
    write = True  # write solution into file for future plotting with plot_solution.py
    error = solve_1D_heat_equation(
        Nx, domain_length / Nx, linear_func, linear_func_dd, write
    )
    assert error < 1.0e-12

    ## Check that, for the non-linear case, the convergence rate of the error relatively
    ## to dx (that is r such that error = C * dx**r) is approximately equal to 2
    Niter = 8  # number of iterations for computing the convergence rate
    list_dx = np.zeros(Niter)  # average cell width for each iteration
    list_error = np.zeros(Niter)  # approximation error for each iteration
    for i in range(Niter):
        Nx = 2 ** (i + 1)
        list_dx[i] = domain_length / Nx
        list_error[i] = solve_1D_heat_equation(
            Nx, domain_length / Nx, sinus_func, sinus_func_dd
        )

    r, C, RMSE = powerlaw_regression(list_dx, list_error)
    assert abs(r - 2) < 0.1
    assert RMSE < 0.05

    # print(r,C,RMSE)
    # For Niter= 8 : r=1.9376 C=0.7032 RMSE=0.0130 (in 0.313 sec)
    # For Niter=10 : r=1.9748 C=0.8053 RMSE=0.0111 (in 0.409 sec)
    # For Niter=12 : r=1.9502 C=0.6877 RMSE=0.0235 (in 2.070 sec)
