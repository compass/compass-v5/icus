/* This script compute the solution of the 1D heat problem and return
   the infinity norm error with respect to the exact solution. Optionnally,
   the exact and approximate solution can be written to a file (using
   function write_to_file) to be later plotted using the script
   plot_solution.py.
*/
#include <Eigen/Dense>
#include <catch2/catch_test_macros.hpp>
#include <cmath>
#include <fstream>
#include <numbers>
#include <random>

#include "icus/Connectivity.h"
#include "icus/Field.h"
#include "icus/ISet.h"
using Eigen::MatrixXd;
using Eigen::VectorXd;
using namespace icus;

/* Create connectivity matrix between cells and faces */
Connectivity<ISet, ISet> connect_cells_to_faces(
    ISet cells, ISet faces, const bool shuffle = false,
    const std::vector<index_type> &shf_cells = {0},
    const std::vector<index_type> &shf_faces = {0}) {
    CHECK(faces.size() == cells.size() + 1);

    // Construct list of the cell neighbours, for each cell
    std::vector<std::array<index_type, 2>> neighbours(cells.size());
    if (shuffle == false) {
        for (auto &&i : cells) neighbours[i] = {i, i + 1};
    } else {
        CHECK(shf_cells.size() == cells.size());
        CHECK(shf_faces.size() == faces.size());
        for (auto &&i : cells)
            neighbours[shf_cells[i]] = {shf_faces[i], shf_faces[i + 1]};
    }

    auto cells_faces = make_connectivity(cells, faces, neighbours);
    for (auto &&[i, targets] : cells_faces.targets_by_source())
        CHECK(targets.size() == 2);

    return cells_faces;
}

/* Generate random cell width according to a uniform law in the range
   [w-dw , w+dw] */
Field<double> random_cells_width(ISet cells, const double w, const double dw) {
    CHECK((w >= 0 && "Variable `w' must be positive"));
    CHECK((dw >= 0 && "Variable `dw' must be positive"));
    CHECK((dw < w && "Variable `dw' must be smaller than variable `w'"));

    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(-1, 1);

    auto cells_width = make_field(cells);
    for (auto &&i : cells) {
        cells_width(i) = w + dw * distribution(generator);
        CHECK(std::abs(cells_width(i) - w) <= dw);
    }
    return cells_width;
}

/* Compute coordinates of the cells center based on cells width */
Field<double> cells_center_from_width(Field<double> cells_width) {
    auto cells_center = make_field(cells_width.support);
    for (auto &&i : cells_center.support) {
        if (i == 0)
            cells_center(i) = cells_width(i) / 2.0;
        else
            cells_center(i) = cells_center(i - 1) +
                              (cells_width(i - 1) + cells_width(i)) / 2.0;
    }
    return cells_center;
}

/* Compute position of the faces based on cells width */
Field<double> faces_x_from_cells_width(ISet faces,
                                       const Field<double> cells_width) {
    CHECK((faces.size() - 1 == cells_width.support.size() &&
           "Input variables have incompatible sizes"));

    auto faces_x = make_field(faces);
    for (auto &&i : faces) {
        if (i == 0)
            faces_x(i) = 0.0;
        else
            faces_x(i) = faces_x(i - 1) + cells_width(i - 1);
    }
    return faces_x;
}

/* Shuffle (i.e. generate a random permutation of) the range {0, ..., N-1} */
std::vector<index_type> random_shuffle(const index_type N) {
    std::vector<index_type> shf;
    for (index_type i = 0; i < N; i++) shf.push_back(i);
    std::shuffle(shf.begin(), shf.end(), std::default_random_engine(0));
    return shf;
}

/* Create an icus field with support I, containing the values of F shuffled
   according to the vector shf */
Field<double> shuffle_field(ISet I, Field<double> F,
                            const std::vector<index_type> &shf) {
    // Check that arguments have compatible sizes
    CHECK(I.size() == F.support.size());
    CHECK(F.support.size() == shf.size());

    auto F_shuffled = make_field(I);
    for (auto &&i : I) F_shuffled(shf[i]) = F(i);

    CHECK(F_shuffled.support == I);
    for (auto &&i : I) CHECK(F_shuffled(shf[i]) == F(i));

    return F_shuffled;
}

/* Print mesh characteristics */
void print_mesh(std::string title, Field<double> cells_width,
                Field<double> cells_center, Field<double> faces_x,
                ISet faces_interior, ISet faces_wall,
                Connectivity<ISet, ISet> cells_faces,
                Connectivity<ISet, ISet> walls_cells,
                Connectivity<ISet, ISet> intfaces_cells) {
    compass::utils::Pretty_printer print;
    print(title);
    print("Cells width :", cells_width);
    print("Cells center :", cells_center);
    print("Faces position :", faces_x);
    print("Interior faces :", faces_interior.mapping());
    print("Exterior (wall) faces :", faces_wall.mapping());
    print("Connections :");
    for (auto &&[i, targets] : cells_faces.targets_by_source()) {
        print("    cell", i, "is connected to faces", targets);
    }
    for (auto &&[i, j] : enumerate_mapping(faces_wall)) {
        auto targets = walls_cells.targets_by_source(i);
        print("    face", j, "is connected to cell", targets);
    }
    for (auto &&[i, j] : enumerate_mapping(faces_interior)) {
        auto targets = intfaces_cells.targets_by_source(i);
        print("    face", j, "is connected to cells", targets);
    }
}

/* Write solution to file. The data in the file thus created can be plotted
   using the script plot_solution.py */
void write_to_file(Field<double> cells_center, Field<double> cells_width,
                   Field<double> cells_Tapprox, Field<double> cells_Texact,
                   double dx) {
    std::string filename = "./test/1D_heat/approx_solution_";
    std::string dx_str = std::to_string(dx);
    dx_str.erase(dx_str.find_last_not_of('0') + 1, std::string::npos);
    dx_str.erase(dx_str.find_last_not_of('.') + 1, std::string::npos);
    filename.append(dx_str);
    filename.append(".dat");

    std::ofstream solfile;
    solfile.open(filename);

    solfile << "# cell center x_i, cell width dx_i, approximate cell "
               "temperature T_i, exact cell temperature T(x_i)\n";
    for (auto &&i : cells_center.support) {
        solfile << cells_center(i) << ", " << cells_width(i) << ", "
                << cells_Tapprox(i) << ", " << cells_Texact(i) << "\n";
    }

    solfile.close();
}

/* Solve the 1D heat problem for a spatial domain discretized into Nx cells of
   average width dx, with source term corresponding to a known exact solution
   either linear or sinusoidal.
   Return the infinity norm error between the exact and approximate solutions
   and, if write_sol=True, write both solution to a data file to enable future
   plotting by script plot_solution.py */
double solve_1D_heat_equation(const int Nx, const double dx, auto sol_exact,
                              auto source_term, const bool write_sol = false) {
    /* Create a trivially ordered mesh using icus : Nx cells and Nx+1 faces */
    auto cells_sorted = ISet(Nx);
    auto faces_sorted = ISet(Nx + 1);
    auto faces_sorted_interior =
        faces_sorted.extract(1, faces_sorted.size() - 1);
    auto faces_sorted_wall = faces_sorted.substract(faces_sorted_interior);
    CHECK(faces_sorted_wall.size() == 2);  // only two walls in 1D

    /* Create connectivities for this ordered mesh */
    auto cells_faces_sorted =
        connect_cells_to_faces(cells_sorted, faces_sorted);
    auto faces_cells_sorted = cells_faces_sorted.transpose();
    auto intfaces_cells_sorted =
        faces_cells_sorted.extract(faces_sorted_interior, cells_sorted);
    auto walls_cells_sorted =
        faces_cells_sorted.extract(faces_sorted_wall, cells_sorted);
    for (auto &&[i, targets] : intfaces_cells_sorted.targets_by_source())
        CHECK(targets.size() == 2);
    for (auto &&[i, targets] : walls_cells_sorted.targets_by_source())
        CHECK(targets.size() == 1);

    /* Generate uniform cells width (default) */
    // auto cells_sorted_width = make_field(cells_sorted) = dx;
    // double domain_length_exact = Nx*dx;

    /* Generate random cell width in the range [0.9*dx , 1.1*dx] */
    auto cells_sorted_width = random_cells_width(cells_sorted, dx, 0.1 * dx);
    double domain_length_exact = 0;
    for (auto &&i : cells_sorted) domain_length_exact += cells_sorted_width(i);

    /* Compute the coordinates of the cells center based on cells width */
    auto cells_sorted_center = cells_center_from_width(cells_sorted_width);

    /* Compute the coordinates of the faces based on cells width */
    auto faces_sorted_x =
        faces_x_from_cells_width(faces_sorted, cells_sorted_width);
    CHECK(faces_sorted_x(faces_sorted.size() - 1) == domain_length_exact);

    /* Generate dirichlet boundary condition on the wall faces */
    double T_left = sol_exact(0, domain_length_exact);
    double T_right = sol_exact(domain_length_exact, domain_length_exact);
    auto faces_sorted_wall_Temp =
        make_field(faces_sorted_wall)
            .fill(std::array<double, 2>{T_left, T_right});

    /* Set constant uniform conductivity inside the domain */
    auto cells_sorted_conductivity = make_field(cells_sorted).fill(1.0);

    /* Shuffle all cells to simulate randomness of mesh indices */
    auto shf_cells = random_shuffle(cells_sorted.size());  // shuffling vector
    auto cells = ISet(cells_sorted.size());
    auto cells_width = shuffle_field(cells, cells_sorted_width, shf_cells);
    auto cells_center = shuffle_field(cells, cells_sorted_center, shf_cells);
    auto cells_conductivity =
        shuffle_field(cells, cells_sorted_conductivity, shf_cells);

    /* Shuffle all faces to simulate randomness of mesh indices */
    auto shf_faces = random_shuffle(faces_sorted.size());  // shuffling vector
    auto faces = ISet(faces_sorted.size());
    auto faces_x = shuffle_field(faces, faces_sorted_x, shf_faces);

    std::vector<index_type> shf_walls;
    std::vector<double> shf_walls_Temp;
    for (auto &&[i, j] : enumerate_mapping(faces_sorted_wall)) {
        shf_walls.push_back(shf_faces[j]);
        shf_walls_Temp.push_back(faces_sorted_wall_Temp(i));
    }
    auto faces_wall = faces.extract(shf_walls);
    auto faces_interior = faces.substract(faces_wall);
    CHECK(faces_wall.size() == 2);
    auto faces_wall_Temp = make_field(faces_wall).fill(shf_walls_Temp);

    /* Create connectivity between shuffled cells and faces */
    auto cells_faces =
        connect_cells_to_faces(cells, faces, true, shf_cells, shf_faces);
    auto faces_cells = cells_faces.transpose();
    auto intfaces_cells = faces_cells.extract(faces_interior, cells);
    auto walls_cells = faces_cells.extract(faces_wall, cells);
    for (auto &&[i, targets] : intfaces_cells.targets_by_source())
        CHECK(targets.size() == 2);
    for (auto &&[i, targets] : walls_cells.targets_by_source())
        CHECK(targets.size() == 1);

    /* Print meshes to check their validity */
    // print_mesh("--- Sorted mesh ---", cells_sorted_width,
    // cells_sorted_center,
    //            faces_sorted_x, faces_sorted_interior, faces_sorted_wall,
    //            cells_faces_sorted, walls_cells_sorted,
    //            intfaces_cells_sorted);
    // print_mesh("--- Shuffled mesh ---", cells_width, cells_center,
    //            faces_x, faces_interior, faces_wall,
    //            cells_faces, walls_cells, intfaces_cells);

    /* Create jacobian matrix and RHS vector for the stationnary 1D heat
       equation */
    MatrixXd Jacobian = MatrixXd::Zero(cells.size(), cells.size());
    VectorXd RHS = VectorXd::Zero(cells.size());
    auto cells_Texact = make_field(cells);
    auto cells_Tapprox = make_field(cells);

    // Discretized exact solution and corresponding source term
    for (auto &&i : cells) {
        cells_Texact(i) = sol_exact(cells_center(i), domain_length_exact);
        RHS(i) = cells_conductivity(i) *
                 source_term(cells_center(i), domain_length_exact);
    }

    // Core of the jacobian matrix
    for (auto &&[i, neighbours] : intfaces_cells.targets_by_source()) {
        /* List cells on each side of face i */
        CHECK(neighbours.size() == 2);
        auto K = neighbours[0];
        auto L = neighbours[1];

        /* Compute transmissivity coeff :
                harmonic mean conductivity of both cells
                divided by distance between the two cell centers
        */
        auto a_K = cells_conductivity(K) / cells_width(K);
        auto a_L = cells_conductivity(L) / cells_width(L);
        auto C_KL = 2.0 * a_K * a_L / (a_K + a_L);

        /* Fill core of the jacobian matrix */
        Jacobian(K, K) += C_KL / cells_width(K);
        Jacobian(K, L) -= C_KL / cells_width(K);
        Jacobian(L, L) += C_KL / cells_width(L);
        Jacobian(L, K) -= C_KL / cells_width(L);
    }

    // Boundary conditions
    for (auto &&[i, neighbours] : walls_cells.targets_by_source()) {
        /* Find cell touching the wall i */
        CHECK(neighbours.size() == 1);
        auto K = neighbours[0];

        /* Compute transmissivity coeff : cell conductivity divided by the
           cell-wall distance */
        auto C_Kwall = 2.0 * cells_conductivity(K) / cells_width(K);

        /* Account for boundary conditions in Jacobian and RHS */
        Jacobian(K, K) += C_Kwall / cells_width(K);
        RHS(K) += C_Kwall * faces_wall_Temp(i) / cells_width(K);
    }

    /* Solve stationnary problem and compare with analytical solution */
    cells_Tapprox.fill(Jacobian.partialPivLu().solve(RHS));
    double error = 0;
    for (auto &&i : cells)
        error = std::max(error, std::abs(cells_Tapprox(i) - cells_Texact(i)));

    /* Sort solution and write it in a text file */
    if (write_sol == true) {
        auto cells_sorted_Texact = make_field(cells_sorted);
        auto cells_sorted_Tapprox = make_field(cells_sorted);
        for (auto &&i : cells_sorted) {
            cells_sorted_Texact(i) = cells_Texact(shf_cells[i]);
            cells_sorted_Tapprox(i) = cells_Tapprox(shf_cells[i]);
        }
        write_to_file(cells_sorted_center, cells_sorted_width,
                      cells_sorted_Tapprox, cells_sorted_Texact, dx);
    }

    return error;
}

/* Linear function and its second derivative */
double linear_func(double x, double L, double T_left, double T_right) {
    double f = T_left + (T_right - T_left) * x / L;
    return f;
}
double linear_func_dd(double, double, double, double) { return 0.0; }

/* Sinusoidale function and its second derivative */
double sinus_func(double x, double L, double T_left, double T_right) {
    double freq = std::numbers::pi / (2 * L);
    double f = T_left + (T_right - T_left) * sin(freq * x);
    return f;
}
double sinus_func_dd(double x, double L, double T_left, double T_right) {
    double freq = std::numbers::pi / (2 * L);
    double f = (T_right - T_left) * pow(freq, 2) * sin(freq * x);
    return f;
}

/* Compute best fitting linear function for data (x,y) using Least Square */
std::tuple<double, double, double> linear_regression(VectorXd x, VectorXd y) {
    CHECK(x.size() == y.size());
    int N = x.size();

    double sum_x = x.sum();
    double sum_y = y.sum();
    double sum_xy = x.dot(y);
    double sum_x_square = x.dot(x);

    double denominator = (N * sum_x_square - sum_x * sum_x);
    double coeff = (N * sum_xy - sum_x * sum_y) / denominator;
    double constTerm = (sum_y * sum_x_square - sum_x * sum_xy) / denominator;

    double SSE = 0;  // sum of squared errors
    for (int i = 0; i < N; i++) {
        SSE += pow(coeff * x(i) + constTerm - y(i), 2);
    }
    double RMSE = sqrt(SSE / N);  // root mean square error

    return std::make_tuple(coeff, constTerm, RMSE);
}

/* Compute best fitting power law function for data (x,y) using log10 conversion
 */
std::tuple<double, double, double> powerlaw_regression(VectorXd x, VectorXd y) {
    CHECK(x.size() == y.size());
    int N = x.size();

    VectorXd xlog = VectorXd::Zero(N);
    VectorXd ylog = VectorXd::Zero(N);
    for (int i = 0; i < N; i++) {
        CHECK((x(i) > 0 && y(i) > 0));
        xlog(i) = log10(x(i));
        ylog(i) = log10(y(i));
    }

    auto [power, coeff, temp] = linear_regression(xlog, ylog);
    coeff = pow(10, coeff);

    double SSE = 0;  // sum of squared errors
    for (int i = 0; i < N; i++) {
        SSE += pow(coeff * pow(x(i), power) - y(i), 2);
    }
    double RMSE = sqrt(SSE / N);  // root mean square error

    return std::make_tuple(power, coeff, RMSE);
}

TEST_CASE("Solve the 1D heat problem using icus") {
    compass::utils::Pretty_printer print;
    double domain_length = 2;  // length of the spatial domain
    double T_left = 0;         // boundary condition on the left
    double T_right = 10;       // boundary condition on the right
    auto sol_exact_linear = std::bind(linear_func, std::placeholders::_1,
                                      std::placeholders::_2, T_left, T_right);
    auto source_term_linear = std::bind(linear_func_dd, std::placeholders::_1,
                                        std::placeholders::_2, T_left, T_right);
    auto sol_exact_sinus = std::bind(sinus_func, std::placeholders::_1,
                                     std::placeholders::_2, T_left, T_right);
    auto source_term_sinus = std::bind(sinus_func_dd, std::placeholders::_1,
                                       std::placeholders::_2, T_left, T_right);

    /* Check that the approximate solution is exact in the linear case */
    int Nx = 100;  // NB : the number of cells in the spatial discretisation
                   // must be at least equal to 2, otherwise there is no
                   // "interior face" and the code crash
    bool write = true;  // write solution into data file for future plotting
                        // by script plot_solution.py
    double error = solve_1D_heat_equation(
        Nx, domain_length / Nx, sol_exact_linear, source_term_linear, write);
    CHECK(error < 1.0e-12);
    print("Maximum error for linear case :", error);

    /* Check that, for the non-linear case, the convergence rate of the error
       relatively to dx (that is r such that error = C * dx**r) is approximately
       equal to 2 */
    const int Niter = 8;
    VectorXd list_dx = VectorXd::Zero(Niter);
    VectorXd list_error = VectorXd::Zero(Niter);
    for (int i = 0; i < Niter; i++) {
        Nx = pow(2, i + 1);
        list_dx(i) = domain_length / Nx;
        list_error(i) = solve_1D_heat_equation(
            Nx, domain_length / Nx, sol_exact_sinus, source_term_sinus);
    }

    auto [r, C, RMSE] = powerlaw_regression(list_dx, list_error);
    CHECK(std::abs(r - 2) < 0.05);
    CHECK(RMSE < 0.05);

    print("Convergence for sinusoidal case :", r, C, RMSE);
    // For Niter= 8 : r=1.96428 C=0.77449  RMSE=0.00945037 (in  0.01 sec)
    // For Niter=10 : r=1.97096 C=0.783358 RMSE=0.0109472  (in  0.55 sec)
    // For Niter=12 : r=1.97538 C=0.790972 RMSE=0.0120412  (in 59.30 sec)
}
