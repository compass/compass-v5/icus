import icus
import numpy as np


def test_cross_platforms_fields_of_integers():
    I = icus.ISet(10)

    def check_labels(f):
        assert type(f) is icus.Labels
        icus.bindings._check_labels(f)

    def check_C_int(f):
        assert type(f) is icus.FieldInt32
        icus.bindings._check_field_C_int(f)

    def check_C_long(f):
        assert type(f) is icus.FieldLong
        icus.bindings._check_field_C_long(f)

    def check_int16(f):
        assert type(f) is icus.FieldInt16
        icus.bindings._check_field_int16(f)

    def check_int32(f):
        assert type(f) is icus.FieldInt32
        icus.bindings._check_field_int32(f)

    def check_int64(f):
        assert type(f) is icus.FieldInt64
        icus.bindings._check_field_int64(f)

    check_labels(icus.Labels(I))
    check_C_long(icus.FieldLong(I))
    check_int16(icus.field(I, np.int16))
    check_int16(icus.field(I, "int16"))
    check_int16(icus.FieldInt16(I))
    check_int32(icus.field(I, np.int32))
    check_int32(icus.field(I, "int32"))
    check_int32(icus.FieldInt32(I))
    check_int64(icus.field(I, np.int64))
    check_int64(icus.field(I, "int64"))
    check_int64(icus.FieldInt64(I))
    # try NOT to use the following type specifications
    # which follow numpy but are platform dependent
    # be explicit using int16, int32 or int64
    check_C_int(icus.field(I, "i"))
    check_C_int(icus.field(I, "int32"))
    if icus.long_is_int64:
        check_C_long(icus.field(I, "l"))
        check_C_long(icus.field(I, "long"))
        check_C_long(icus.field(I, int))
    else:
        check_C_int(icus.field(I, "l"))
        check_C_int(icus.field(I, "long"))
        check_C_int(icus.field(I, int))
    if np.int_ == np.int64:
        check_int64(icus.field(I, np.int_))
    else:
        assert np.int_ == np.int32
        check_int32(icus.field(I, np.int_))
