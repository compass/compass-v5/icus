#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>

#include "icus/ISet.h"

using ivector = std::vector<icus::index_type>;

TEST_CASE("Sub iset") {
    // Say we have the following numbering of a 4x4 checkerboard:
    //  0  1  2  3
    //  4  5  6  7
    //  8  9 10 11
    // 12 13 14 15

    using namespace icus;

    auto checkerboard = ISet(16);

    // test loops
    index_type k = 0;
    for (auto &&i : checkerboard) {
        CHECK(i == k);
        ++k;
    }

    ivector map_edge2board{0, 1, 2, 3, 4, 7, 8, 11, 12, 13, 14, 15};
    auto edge = checkerboard.extract(map_edge2board);

    k = 0;
    for (auto &&i : edge.mapping()) {
        CHECK(i == map_edge2board[k]);
        ++k;
    }

    ivector map_corners2edge{0, 3, 8, 11};
    auto corners_from_edge = edge.extract(map_corners2edge);
    k = 0;
    for (auto &&i : corners_from_edge.mapping()) {
        CHECK(i == map_corners2edge[k]);
        ++k;
    }

    auto corners = corners_from_edge.rebase(checkerboard);
    k = 0;
    for (auto &&i : corners.mapping()) {
        CHECK(i == map_edge2board[map_corners2edge[k]]);
        ++k;
    }
    const auto &corners_mapping = corners.mapping();
    for (auto &&[k, m] : std::views::zip(corners, corners.mapping())) {
        CHECK(corners_mapping[k] == m);
    }
}
