#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"

TEST_CASE("Enumerate mapping") {
    using namespace icus;

    auto I = ISet(10);
    auto J = I.extract(std::initializer_list<index_type>{0, 2, 4, 6, 8});
    index_type k = 0;
    for (auto&& [i, j] : enumerate_mapping(J)) {
        CHECK(i == k);
        CHECK(j == 2 * k);
        ++k;
    }
}
