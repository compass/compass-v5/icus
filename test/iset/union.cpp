#include <iostream>
#include <map>

#include "catch2/catch_test_macros.hpp"
#include "icus/ISet.h"

TEST_CASE("Union") {
    using namespace icus;

    //                                               ┌─────────────────┐
    //    ┌──────────────┬────────┬──────────────────┤   boulangerie   |
    //    |              |        |                  └────────┬────────┘
    //    |              |        |                           │
    //    |              |        |                           │
    // promo    extension_promo   |                           │

    //     ┌──────────────── viennoiseries        ┌─────────pains──────────┐
    //     │                     │                │           │            │
    //     │                     │                │           │            │

    // croissants     pains_aux_raisins   pains_blancs   pain_aux_c    baguettes
    //
    //                                  /    │          │       │
    //                                 /     │          │       │
    //
    //                       miches_b  baguettes_b  miches_c  baguettes_c

    using indices = ISet::mapping_t::vector;

    auto croissants = ISet(6);
    auto pains_aux_raisins = ISet(12);
    std::cout << croissants << std::endl;
    // Union without a base
    auto viennoiseries = join(croissants, pains_aux_raisins);
    CHECK(croissants.has_base());
    CHECK(pains_aux_raisins.has_base());
    CHECK(viennoiseries.size() == 18);

    auto pains_blancs = ISet(20);
    auto miches_b = pains_blancs.extract(0, 10);
    auto baguettes_b = pains_blancs.extract(10, 20);

    auto pains_aux_cereales = ISet(10);
    auto miches_c = pains_aux_cereales.extract(0, 5);
    auto baguettes_c = pains_aux_cereales.extract(5, 10);
    CHECK(std::ranges::equal(baguettes_c.resolve_mapping(pains_aux_cereales),
                             indices{5, 6, 7, 8, 9}));
    // Union with no common ancestor
    auto baguettes = join(baguettes_b, baguettes_c);
    CHECK(baguettes.size() == 15);
    CHECK_FALSE(baguettes_b.is_included_in(pains_aux_cereales));
    CHECK(baguettes_b.is_included_in(baguettes));
    // empty iset
    auto empty = ISet();
    CHECK(empty.is_included_in(baguettes));

    CHECK(pains_aux_cereales.has_base());
    // A base to pain_blanc and pains_aux_cereales was created
    // by the union of baguette_b and baguette_c
    auto pains = pains_aux_cereales.parent();
    CHECK(pains == pains_blancs.parent());
    auto lca = lowest_common_ancestor(baguettes_b, miches_c);
    CHECK(lca);
    CHECK(*lca == pains);
    // Union with common ancestor (boulangerie)
    auto boulangerie = join(viennoiseries, pains);
    auto promo = join(croissants, baguettes);
    // Union with common ancestor (boulangerie),
    // duplicates are removed
    auto extension_promo = join(promo, viennoiseries);
    CHECK(extension_promo.size() ==
          33);  // not 39 because croissants is a sub-iset of viennoiseries
    std::cout << miches_b << std::endl;
    std::cout << extension_promo << std::endl;

    CHECK(intersect(pains_blancs, baguettes, pains).size() ==
          baguettes_b.size());
    CHECK(intersect(pains_blancs, baguettes, pains).parent() == pains);
    CHECK(intersect(pains_blancs, baguettes, boulangerie).parent() ==
          boulangerie);

    CHECK(pains_aux_cereales.substract(baguettes_c) == miches_c);
    CHECK(pains_aux_cereales.substract(croissants) == pains_aux_cereales);
}
