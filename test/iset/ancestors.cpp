#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"

TEST_CASE("ISet ancestors") {
    using namespace icus;

    ISet E;  // empty set
    CHECK_FALSE(E);
    ISet I{10};
    auto J = I.extract(0, 3);
    CHECK_FALSE(E.has_ancestor(E));
    CHECK_FALSE(I.has_ancestor(I));
    CHECK_FALSE(I.has_base());
    CHECK(I.has_no_base());
    CHECK_FALSE(J.has_ancestor(J));
    CHECK_FALSE(I.has_ancestor(E));
    CHECK_FALSE(J.has_ancestor(E));
    CHECK(J.has_ancestor(I));
    CHECK(E.ancestors().size() == 0);
    CHECK(E.ancestors().empty());
    CHECK(I.ancestors().size() == 0);
    CHECK(I.ancestors().empty());
    CHECK(J.ancestors().size() == 1);
}
