#include <icus/ISet.h>

#include <catch2/catch_test_macros.hpp>

TEST_CASE("Inclusion between isets") {
    using namespace icus;

    ISet A{2};
    ISet B{2};
    CHECK_FALSE(A.is_included_in(B));
    CHECK_FALSE(B.is_included_in(A));
    ISet rA = A.rebase(A);
    CHECK(rA == A);
    CHECK(A == rA);
    CHECK(rA.is_included_in(A));
    CHECK(A.is_included_in(rA));
    ISet rB = B.rebase(B);
    CHECK_FALSE(rB.is_included_in(rA));
}
