#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"

TEST_CASE("Empty isets") {
    using namespace icus;
    auto J = ISet{};
    CHECK(J.empty());
    auto Iempty = ISet{};
    CHECK(Iempty.empty());
    CHECK(J == Iempty);
    CHECK_FALSE(Iempty);
    auto Is = ISet(7);
    CHECK(Iempty != Is);
    // resolve_mapping
    auto empty_map = Iempty.resolve_mapping(Iempty);
    CHECK(empty_map.size() == 0);
    CHECK(empty_map.empty());
    auto empty_map2 = Iempty.resolve_mapping(Is);
    CHECK(empty_map2.size() == 0);
    CHECK(empty_map2.empty());
    // raises error at runtime: assert(Iempty)
    // Is.resolve_mapping(Iempty);
    // join
    CHECK(join_no_base(Iempty, Is) == Is);
    auto join0 = join(Iempty, Is);
    CHECK(join0 == Is);
    auto join1 = join(Is, Iempty);
    CHECK(join1 == Is);
    auto join2 = join(ISet(), Iempty);
    CHECK(join2.empty());
    CHECK(join_no_base(Iempty, Iempty).empty());
    CHECK(join(Iempty, Iempty).empty());
    // is included in
    CHECK(Iempty.is_included_in(Is));
    CHECK_FALSE(Is.is_included_in(Iempty));
    CHECK(Iempty.is_included_in(ISet()));
    // extract with mask of false, gives empty iset
    auto Lempty = Is.extract(std::vector<bool>(Is.size(), false));
    CHECK(Lempty.empty());
    CHECK_FALSE(Lempty.has_base());
    // extract with empty list
    auto Jempty = Is.extract(0, 0);
    CHECK(Jempty.empty());
    CHECK_FALSE(Jempty.has_base());
    auto Kempty = Is.extract(ISet::mapping_t());
    CHECK(Kempty.empty());
    CHECK_FALSE(Kempty.has_base());
    CHECK(Jempty == Kempty);
    CHECK(std::ranges::empty(Kempty));
    // rebase empty iset
    ISet E;       // empty
    ISet NE{10};  // non-empty
    auto I = E.rebase(NE);
    CHECK_FALSE(I);
    CHECK(I.empty());
    CHECK(std::ranges::empty(E));
    // unicity of empty set
    CHECK(ISet{} == ISet{});
    std::set<ISet> S;
    S.insert(ISet{});
    S.insert(ISet{});
    CHECK(S.size() == 1);
    // reorder
    CHECK(Iempty.reorder(empty_map) == ISet{});
    CHECK_FALSE(Iempty.reorder(empty_map));
    // ancestors
    CHECK_FALSE(Iempty.has_ancestor(Iempty));
    // intersect
    CHECK(intersect(NE, E, NE).empty());
}
