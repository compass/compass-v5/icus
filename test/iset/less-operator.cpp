#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"

TEST_CASE("Less operator") {
    using namespace icus;

    std::less<ISet> less;

    auto I = ISet(10);
    auto J = ISet(10);

    CHECK(less(I, J) != less(J, I));
}
