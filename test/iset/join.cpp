#include "catch2/catch_test_macros.hpp"
#include "icus/ISet.h"

TEST_CASE("Join isets") {
    using namespace icus;

    ISet isets[3] = {ISet{5}, ISet{10}, ISet{2}};
    auto &[I, J, K] = isets;
    auto parent = join_no_base(isets);
    CHECK(parent.size() == 17);
    CHECK(I.has_ancestor(parent));
    CHECK(J.has_ancestor(parent));
    CHECK(K.has_ancestor(parent));

    // join with empty iset
    auto empty = ISet();
    CHECK(join(J, empty) == J);
    CHECK(join(empty, empty) == ISet());
    CHECK_FALSE(join(empty, empty));
}
