#include <catch2/catch_test_macros.hpp>
#include <map>

#include "icus/ISet.h"

TEST_CASE("Isets as map keys") {
    using namespace icus;

    std::map<ISet, size_type> imap;

    auto add = [&imap](const ISet &I) { imap[I] = I.size(); };

    auto I = ISet(10);
    add(I);
    auto J = ISet(I);
    CHECK(imap.find(I) != imap.end());
    CHECK(I == J);
    CHECK(imap.find(J) != imap.end());
    J = ISet(12);
    CHECK(I != J);
    add(J);
    CHECK(imap.size() == 2);
    add(J.extract(9, 11));
    CHECK(imap.size() == 3);
}
