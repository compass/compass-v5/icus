#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <map>

#include "icus/ISet.h"

TEST_CASE("Genealogy") {
    using namespace icus;
    using indices = ISet::mapping_t::vector;
    auto lycee = ISet(300);
    auto personnel = lycee.extract(0, 70);
    CHECK(personnel.has_ancestor(lycee));
    CHECK(personnel.is_included_in(lycee));
    auto etudiants = lycee.extract(70, 300);
    auto administration = personnel.extract(0, 20);
    CHECK(administration.is_included_in(personnel));
    auto agents_entretien = personnel.extract(20, 30);
    auto professeurs = personnel.extract(30, 70);
    auto profs_de_maths = professeurs.extract(indices{0, 3, 6, 9, 12, 15, 18});
    auto profs_d_anglais =
        professeurs.extract(indices{1, 4, 7, 10, 13, 16, 19});
    auto profs_de_sport = professeurs.extract(indices{2, 11, 29, 33});

    CHECK_FALSE(professeurs.has_ancestor(professeurs));
    CHECK(professeurs.has_ancestor(personnel));
    CHECK_FALSE(profs_de_sport.has_ancestor(profs_de_sport));
    CHECK(profs_de_sport.has_ancestor(professeurs));
    CHECK(profs_de_sport.has_ancestor(personnel));
    CHECK_FALSE(profs_de_sport.has_ancestor(profs_de_maths));
    auto empty = ISet{};
    CHECK_FALSE(empty.has_ancestor(empty));

    auto a = profs_d_anglais.ancestors();
    // FIXME: check output
    for (auto i : a) {
        std::cout << i.size() << " ";
    }
    std::cout << std::endl;

    auto c = lowest_common_ancestor(agents_entretien, profs_de_maths);
    REQUIRE(c);
    CHECK(*c == personnel);
    auto sans_rapport = ISet(42);
    auto c2 = lowest_common_ancestor(agents_entretien, sans_rapport);
    CHECK_FALSE(c2);

    auto profs_d_anglais_melange =
        profs_d_anglais.reorder({6, 5, 4, 3, 2, 1, 0});
    // not the same ISet (because not the same order)
    CHECK(profs_d_anglais != profs_d_anglais_melange);
    // same set
    CHECK((profs_d_anglais.is_included_in(profs_d_anglais_melange) &&
           profs_d_anglais.size() == profs_d_anglais_melange.size()));
    CHECK(profs_d_anglais != profs_de_sport);
    CHECK(empty.reorder(ISet::mapping_t{}) == ISet{});

    ISet I{10};
    auto J = I.extract(0, 5);
    auto K = I.extract(5, 9);
    auto L = J.extract(1, 3);
    CHECK_FALSE(L.empty());
    auto lca = lowest_common_ancestor(I, J, K, L);
    // clang-format off
    CHECK(std::is_same_v<decltype(lca), std::optional<ISet>>);
    CHECK(lca); // check the optional via the implicit conversion to bool
    CHECK(lca.has_value()); // check the optional explicitly
    CHECK(lca == I); // the comparison is licit thanks to the definition of std::optional operators
    CHECK(*lca == I); // as lca contains a value this form is licit (but less expressive)
    // clang-format on
    auto M = K.extract({2, 0});
    CHECK(std::ranges::equal(M.resolve_mapping(I), indices{7, 5}));
}
