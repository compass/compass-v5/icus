#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"

TEST_CASE("Iset intersection") {
    using namespace icus;
    auto mesh = ISet(20);
    auto sites = mesh.extract(0, 12);
    auto cells = mesh.extract(0, 5);
    auto nodes = mesh.extract(15, 20);
    auto empty_iset = intersect(sites, nodes, sites);
    CHECK(empty_iset.size() == 0);
    CHECK(empty_iset.empty());
    auto empty_iset2 = intersect_siblings(sites, nodes);
    CHECK(empty_iset2.size() == 0);
    CHECK(empty_iset2.empty());

    auto non_empty_iset = intersect(sites, cells, sites);
    CHECK(non_empty_iset.size() == 5);
    CHECK(non_empty_iset.parent() == sites);
    auto non_empty_iset2 = intersect_siblings(sites, cells);
    CHECK(non_empty_iset2 == non_empty_iset);
    CHECK(non_empty_iset2.parent() == mesh);

    auto emp = ISet();
    CHECK(intersect(emp, cells, cells).empty());
    CHECK(intersect(cells, emp, emp).empty());
    CHECK(intersect_siblings(emp, cells).empty());

    ISet I{10}, J{10};
    CHECK_FALSE(are_disjoint_sets(I, I));
    CHECK(are_disjoint_sets(I, J));
    CHECK(are_disjoint_sets(J, I));
    auto K = I.extract(0, 5);
    CHECK_FALSE(are_disjoint_sets(I, K));
    CHECK_FALSE(are_disjoint_sets(K, I));
    auto L = I.extract(5, 10);
    CHECK(are_disjoint_sets(K, L));
    CHECK(are_disjoint_sets(L, K));
}
