#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"

TEST_CASE("Iset mappings") {
    using namespace icus;

    static_assert(std::forward_iterator<Mapping::iterator>);
    static_assert(std::ranges::range<Mapping>);

    Mapping M{Slice<index_type>{3, 7}};
    CHECK(std::ranges::equal(M, Slice{3, 7}));
    CHECK(std::ranges::equal(M, std::views::iota(3, 7)));
    auto I = ISet(10);
    auto il = std::initializer_list<index_type>{1, 3, 5};
    auto J = I.extract(il);
    auto v = J.mapping_view();
    CHECK(J.size() == 3);
    index_type k = 0;
    for (auto&& i : il) {
        CHECK(v[k] == i);
        ++k;
    }
}
