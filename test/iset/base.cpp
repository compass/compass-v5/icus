#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"

TEST_CASE("ISet basic operations") {
    //! [Creating an ISet]
    using namespace icus;
    auto I = ISet(2);
    //! [Creating an ISet]
    auto Icopy = I;
    ISet Icopy2(I);
    auto J = ISet(4);
    I.set_base(J, {0, 2});
    CHECK(I.has_base());
    CHECK(I.parent() == J);
    CHECK(Icopy == I);
    CHECK(Icopy2 == I);
    CHECK_FALSE(I.has_ancestor(I));
    CHECK(I.has_ancestor(J));
    auto ancestors = I.ancestors();
    CHECK(ancestors.contains(J));
    CHECK(std::begin(ancestors) == J);
}

TEST_CASE("ISet equality") {
    using namespace icus;
    using indices = std::initializer_list<index_type>;
    // empty sets are equal
    CHECK(ISet() == ISet());
    auto I = ISet(3);
    auto J = I.extract(indices{2, 0});
    auto K = J.extract(indices{1, 0});
    auto L = I.extract(indices{0, 2});
    CHECK(I != J);
    CHECK(K == L);
}
