from icus import ISet, field
from icus_field_demo import X
import numpy as np
import pytest


def test_ellipsis_assignments():
    I = ISet(10)
    F = field(I, X)
    x = X()
    x.x = 3.14
    x.y[...] = [1, 2]
    # setting a field from an item value
    F[...] = x
    a = F.as_array()
    assert np.all(a.x == 3.14)
    assert np.all(a.y == [1.0, 2.0])
    a[0] = (2.5, (42, 43))
    # set all array from a record item
    F[...] = a[0]
    a = F.as_array()
    assert np.all(a.x == 2.5)
    assert np.all(a.y == [42.0, 43.0])
    a = np.zeros(I.size(), dtype=X.numpy_dtype)
    # setting field from record array of the same shape
    F[...] = a
    a = F.as_array()
    assert np.all(a.x == 0)
    assert np.all(a.y == [0, 0])
    # setting only part of a Field using a subiset
    J = I.extract(np.arange(0, I.size(), 2))
    F[J] = x
    a = F.as_array()
    assert np.all(a.x[::2] == 3.14)
    assert np.all(a.x[1::2] == 0)
    assert np.all(a.y[::2] == [1.0, 2.0])
    assert np.all(a.y[1::2] == [0, 0])

    b = np.zeros((I.size(), 3), dtype="d")
    # though the memory footprint of both arrays is the same
    assert a.nbytes == b.nbytes
    with pytest.raises(ValueError):
        # we cannot assign b to F
        F[...] = b
