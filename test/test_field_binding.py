from icus import ISet, field, Field, tensor
from icus_field_demo import X
import numpy as np
import pytest


def test_cpp_side_field_bindings():

    I = ISet(10)

    F = field(I)
    assert isinstance(F, Field)
    assert F.as_array().dtype == np.double
    F = field(I, dtype="f")
    assert F.as_array().dtype == np.float32
    F = field(I, dtype=np.float32)
    assert isinstance(F, Field)
    assert F.as_array().dtype == np.float32
    F = field(I, dtype=np.float64)
    assert F.as_array().dtype == np.float64

    F = field(I, dtype=X)
    J = ISet(10)
    FJ = F.share(J)

    assert F.size() == I.size()
    assert F.support() == I

    indices = [1, 3, 5]
    J = I.extract(indices)

    view = F[J]

    assert len(indices) == view.size()

    Fi = field(J, "i")
    Fi = field(J, "int")

    Fi = field(I, "i")
    Fj = Fi.extract(J)
    Fj = Fi.view(J)

    FTJ = field(J, dtype=tensor((2, 2)))

    print(FTJ.as_array())
    a = FTJ.as_array()
    a[1] = np.diag((1, 2))
    print(FTJ.as_array())

    print(tensor((3, 3), dtype="i"))

    # tensor is as facility that is meant to be compliant with dlpack
    # cf. DLDataTypeCode enum it has only 6 possible (numeric) types
    # (https://github.com/dmlc/dlpack/blob/ca4d00ad3e2e0f410eeab3264d21b8a39397f362/include/dlpack/dlpack.h#L137)
    with pytest.raises(TypeError):
        T = tensor((3, 3), dtype=X)


def test_tensor_constructor():
    T = tensor((2, 2), "d")
    t = T()
    assert t.shape() == (2, 2)
    assert t.as_array().shape == (2, 2)
    assert t.as_array().dtype == np.double
    I = ISet(10)
    F = field(I, T)
    a = F.as_array()
    F[...] = np.array(((1, 2), (3, 4)))
    assert np.all(a == ((1, 2), (3, 4)))
    F[1] = np.ones((2, 2))
    assert np.all(a[1] == 1)
    J = I.extract([0, 3])
    F[J] = np.array([((4, 3), (2, 1)), ((5, 6), (7, 8))])
    assert np.all(a[0] == ((4, 3), (2, 1)))
    assert np.all(a[3] == ((5, 6), (7, 8)))
    FK = F.share(ISet(10))
    assert FK.support() != I


def test_field_array_assignement():

    I = ISet(3)
    J = I.extract([0, 2])
    F = field(I, tensor((2, 2)))
    a = np.arange(3 * 4).reshape((3, 2, 2))
    F[...] = a
    f = F.as_array()
    assert np.all(a == f)
    b = -np.arange(2 * 4).reshape((2, 2, 2))
    F[J] = b
    assert np.all(f[0] == b[0])
    assert np.all(f[1] == a[1])
    assert np.all(f[2] == b[1])
    K = J.extract([1, 0])
    F[K] = b
    assert np.all(f[0] == b[1])
    assert np.all(f[1] == a[1])
    assert np.all(f[2] == b[0])
