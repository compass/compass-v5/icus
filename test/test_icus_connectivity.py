from icus import (
    ISet,
    Connectivity,
    make_connectivity,
)
import numpy as np


def test_connectivity():
    I = ISet(3)
    J = ISet(6)

    targets = [list(l) for l in [[0], np.arange(2), range(3)]]
    C = Connectivity(I, J, targets)
    assert all([all(t == l) for t, l in zip(C.targets(), targets)])
    assert all([all(C.targets_by_source(i) == targets[i]) for i in I])
    view = C.targets()  # a python proxy to iterate over targets / or acces them
    assert all([all(t == l) for t, l in zip(view, targets)])
    assert all([all(view[i] == targets[i]) for i in I])

    assert not C.is_regular
    assert C.image() == J.extract(np.arange(I.size()))

    targets = [[0], [0, 1, 2], [2]]
    C = make_connectivity(I, J, targets)
    assert not C.is_regular
    assert all([all(t == l) for t, l in zip(C.targets(), targets)])
    assert all([all(C.targets_by_source(i) == targets[i]) for i in I])
    view = C.targets()  # a python proxy to iterate over targets / or acces them
    assert all([all(t == l) for t, l in zip(view, targets)])
    assert all([all(view[i] == targets[i]) for i in I])

    RC = Connectivity(I, J, np.reshape(np.arange(I.size() * 2), (I.size(), 2)))
    assert RC.is_regular
    # regular connectivity can be accessed with "as_array" or "targets"
    assert all([np.all(t == l) for t, l in zip(RC.targets(), RC.as_array())])
    assert all([all(RC.targets_by_source(i) == [i * 2, i * 2 + 1]) for i in I])
    view = RC.targets()  # a python proxy to iterate over targets / or acces them
    assert all([all(view[i] == [2 * i, 2 * i + 1]) for i in I])
    assert RC.targets_size() == 2

    RC = make_connectivity(I, J, [[0, 1]] * I.size())
    assert RC.is_regular
    assert RC.targets_size() == 2
    assert RC.as_array().shape == (3, 2)
    assert RC.source() == I
    assert RC.target() == J
