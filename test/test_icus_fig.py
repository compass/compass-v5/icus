import pytest

import numpy as np

import icus
from icus.fig import Zone, Data, Record, RecordShelf, DataKind, Scalar, Vector, Tensor
from icus.fig import _vector
import icus_field_demo as demo


def test_demo():
    L = 10
    Nx = 20

    # setup low-level iset/field
    cell = Zone(icus.ISet(Nx))
    face = Zone.from_size(Nx + 1)
    #
    dx = L / Nx
    field_face_x = icus.field(face.iset)
    field_face_x[...] = [i * dx for i in face.iset]
    field_cell_x = icus.field(cell.iset)
    field_cell_x[...] = [dx / 2 + i * dx for i in cell.iset]

    # setup high-level model definition
    x = Record("double", Scalar)
    x[...] = Data(field_face_x)
    x[...] = Data(field_cell_x)
    #
    new_data = lambda: RecordShelf(
        {
            "coefficients": {
                "diffusion": ("double", Scalar),
                "relaxation_time": ("double", Scalar),
            },
            "source_term": ("double", Scalar),
            "boundary_conditions": {
                "Dirichlet": ("double", Scalar),
                "Neumann": ("double", Scalar),
            },
            "initial_state": ("double", Scalar),
        }
    )

    # user setup specific model data
    left = x[face] <= 0
    right = x[face] >= L
    rock_a = x[cell] <= L / 2
    rock_b = x[cell] > L / 2
    source = (L / 8 <= x[cell]) & (x[cell] <= L * 3 / 8)
    #
    data = new_data()
    data.source_term[...] = 0
    data.source_term[source] = 1
    data.coefficients.diffusion[rock_a] = 1e-3
    data.coefficients.diffusion[rock_b] = 1e-1
    data.coefficients.relaxation_time[...] = 1
    data.boundary_conditions.Dirichlet[left | right] = 0
    data.initial_state[...] = np.sin(x[cell] * 2 * np.pi / L * 3)

    # retrieve values
    data.source_term[cell]
    data.coefficients.diffusion[cell]
    data.coefficients.relaxation_time[cell]
    data.initial_state[cell]
    #
    boundary = left | right
    assert not data.boundary_conditions.Neumann.definition_zone(boundary)
    assert data.boundary_conditions.Dirichlet.definition_zone(boundary) == boundary
    data.boundary_conditions.Dirichlet[boundary]


def test_datakind():
    #
    assert Scalar.convert(1) == 1
    assert Scalar.convert([1, 2, 3, 4]) == [1, 2, 3, 4]
    with pytest.raises(TypeError):
        Scalar.convert([[1, 2]])
    with pytest.raises(TypeError):
        Scalar.convert(1, is_array=True)
    with pytest.raises(TypeError):
        Scalar.convert([1, 2, 3, 4], is_array=False)
    #
    assert Vector(1).convert(2) == [2]
    assert Vector(1).convert([2]) == [2]
    assert Vector(3).convert([1, 2, 3]) == [1, 2, 3]
    assert Vector(3).convert([[1, 2, 3], [4, 5, 6]]) == [[1, 2, 3], [4, 5, 6]]
    with pytest.raises(TypeError):
        Vector(3).convert([1, 2])
    with pytest.raises(TypeError):
        Vector(3).convert([1, 2, 3], is_array=True)
    with pytest.raises(TypeError):
        Vector(3).convert([[1, 2, 3], [4, 5, 6]], is_array=False)
    #
    assert np.allclose(Tensor(2).convert([[1, 2], [3, 4]]), [[1, 2], [3, 4]])
    assert np.allclose(Tensor(2).convert([1, 2]), [[1, 0], [0, 2]])
    assert np.allclose(Tensor(2).convert(5), [[5, 0], [0, 5]])
    assert np.allclose(
        Tensor(2).convert([1, 2], is_array=True), [[[1, 0], [0, 1]], [[2, 0], [0, 2]]]
    )
    assert np.allclose(
        Tensor(2).convert([[1, 2], [3, 4]], is_array=1),
        [[[1, 0], [0, 2]], [[3, 0], [0, 4]]],
    )


def test_zones():
    idx = lambda ref, z: list(z.iset.rebase(ref.iset).mapping())
    void = lambda: Zone(icus.ISet())
    #
    X = Zone(icus.ISet(10))
    Y = Zone.from_size(7)
    Xa = Zone(X.iset.extract(list(X.iset)[::2]))
    Xb = Zone(X.iset.extract(list(X.iset)[:5]))
    Ya = Zone(Y.iset.extract(list(Y.iset)[::2]))
    Yb = Zone(Y.iset.extract(list(Y.iset)[:5]))
    #
    assert len(X) == 10
    assert len(Xa) == 5
    assert len(Xb) == 5
    assert len(Y) == 7
    assert len(Ya) == 4
    assert len(Yb) == 5
    assert len(void()) == 0
    assert len(Zone()) == 0
    #
    assert void() == void()
    #
    assert (X - Xa) | Xa == X
    assert (X - Xb) | Xb == X
    assert (Y - Ya) | Ya == Y
    assert (Y - Yb) | Yb == Y
    #
    assert idx(X, Xa) == [0, 2, 4, 6, 8]
    assert idx(X, Xb) == [0, 1, 2, 3, 4]
    assert idx(Y, Ya) == [0, 2, 4, 6]
    assert idx(Y, Yb) == [0, 1, 2, 3, 4]
    #
    assert idx(X, Xa & Xb) == [0, 2, 4]
    assert idx(Y, Ya & Yb) == [0, 2, 4]
    assert X & void() == void()
    #
    assert idx(X, Xa | Xb) == [0, 1, 2, 3, 4, 6, 8]
    assert idx(Y, Ya | Yb) == [0, 1, 2, 3, 4, 6]
    assert X | void() == X
    #
    assert idx(X, Xa - Xb) == [6, 8]
    assert idx(X, Xb - Xa) == [1, 3]
    assert idx(Y, Ya - Yb) == [6]
    assert idx(Y, Yb - Ya) == [1, 3]
    assert X - void() == X
    assert void() - X == void()
    #
    assert idx(X, Xa ^ Xb) == [1, 3, 6, 8]
    assert idx(Y, Ya ^ Yb) == [1, 3, 6]
    assert X ^ void() == X
    #
    assert not (Xa == Ya)
    assert Xa != Ya
    assert X != void()
    #
    assert len(X | Y) == 17
    assert len(Xa | Ya) == 9
    assert len(Xb | Yb) == 10
    assert len(Xa | Yb) == 10
    assert len(Xb | Ya) == 9
    #
    assert Xa <= X
    assert Xb <= X
    assert (Xa & Xb) <= Xa
    assert (Xa & Xb) <= Xb
    assert (Xa & Xb) <= X
    assert void() <= X
    assert void() <= void()
    assert not (X <= void())
    #
    XX = Zone(icus.ISet(3))
    YY = Zone.from_size(3)
    ZZ = XX & YY
    assert len(ZZ) == 0
    assert ZZ <= XX
    assert ZZ <= YY


def test_data():
    close = lambda d, a: isinstance(d, Data) and np.allclose(d.field.as_array(), a)
    field = lambda iset, values: (f := icus.field(iset), f.__setitem__(..., values))[0]
    same = lambda d1, d2: d1.zone == d2.zone == (d1 == d2)
    #
    s = icus.ISet(5)
    x = Data(field(s, [i for i in s]))
    #
    assert close(x, [0, 1, 2, 3, 4])
    assert close(-x, [0, -1, -2, -3, -4])
    assert close(x + x, [0, 2, 4, 6, 8])
    assert close(x + 1, [1, 2, 3, 4, 5])
    assert close(1 + x, [1, 2, 3, 4, 5])
    assert close((1 + x) - x, [1, 1, 1, 1, 1])
    assert close(x - (1 + x), [-1, -1, -1, -1, -1])
    assert close(x - 1, [-1, 0, 1, 2, 3])
    assert close(x * x, [0, 1, 4, 9, 16])
    assert close((x + 1) / (x + 1), [1, 1, 1, 1, 1])
    assert close(x**2, [0, 1, 4, 9, 16])
    assert close(np.square(x), [0, 1, 4, 9, 16])
    assert close(np.maximum(x, 5 - x), [5, 4, 3, 3, 4])
    assert np.max(x) == 4
    assert np.sum(x) == 10
    assert np.prod(x + 1) == 120
    #
    assert (x == 2) == Zone(s.extract([2]))
    assert (x > 2) == Zone(s.extract([3, 4]))
    assert (x >= 2) == Zone(s.extract([2, 3, 4]))
    assert (x < 2) == Zone(s.extract([0, 1]))
    assert (x <= 2) == Zone(s.extract([0, 1, 2]))
    assert (x**2 <= 3 * x) == Zone(s.extract([0, 1, 2, 3]))
    #
    y = x[x <= 3]
    z = x[2 <= x]
    assert same(y + z, Data(field(s.extract([2, 3]), [4, 6])))
    assert (y == z) == Zone(s.extract([2, 3]))
    #
    t = icus.ISet(3)
    y = Data(field(t, [i for i in t]))
    assert same(x + y, Data(field(icus.ISet(), [])))
    assert (x == y) == Zone(icus.ISet())
    assert (x == y) == Zone()

    X = Zone(icus.ISet(2))
    Y = Zone.from_size(2)
    a = Data(field(X.iset, [1, 2]))
    with pytest.raises(KeyError):
        a[Y]


def test_data_fancy_getitem():
    field = lambda iset, type, values: (
        f := icus.field(iset, type),
        f.__setitem__(..., values),
    )[0]
    new_X = lambda a, b, c: (
        x := demo.X(),
        setattr(x, "x", a),
        x.y.__setitem__(..., (b, c)),
    )[0]

    X = Zone(icus.ISet(6))

    a = Data(field(X.iset, "double", 0))
    assert np.allclose(a[X].field.as_array(), 0)
    assert X == (a == 0)

    ea, eb, ec = [demo.MyEnum(i) for i in range(3)]
    a = Data(field(X.iset, demo.MyEnum, ea))
    assert X == (a[X] == ea)

    t = icus.tensor((2,), "d")
    a = Data(field(X.iset, t, t()))
    assert np.allclose(a.field.as_array(), [0, 0])
    assert X == (a == [0, 0])
    #
    assert np.allclose(a[X, 0].field.as_array(), 0)
    assert np.allclose(a[X, 1].field.as_array(), 0)
    #
    assert X == (a[X, 0] == 0)
    assert X == (a[X, 1] == 0)

    t = icus.tensor((2, 2), "d")
    a = Data(field(X.iset, t, t()))
    assert np.allclose(a.field.as_array(), [[0, 0], [0, 0]])
    assert X == (a == [[0, 0], [0, 0]])
    #
    assert np.allclose(a[X, 0].field.as_array(), [0, 0])
    assert np.allclose(a[X, 1].field.as_array(), [0, 0])
    #
    assert X == (a[X, 0] == [0, 0])
    assert X == (a[X, 1] == [0, 0])
    #
    assert np.allclose(a[X, 0, 0].field.as_array(), 0)
    assert np.allclose(a[X, 0, 1].field.as_array(), 0)
    assert np.allclose(a[X, 1, 0].field.as_array(), 0)
    assert np.allclose(a[X, 1, 1].field.as_array(), 0)
    #
    assert X == (a[X, 0, 0] == 0)
    assert X == (a[X, 0, 1] == 0)
    assert X == (a[X, 1, 0] == 0)
    assert X == (a[X, 1, 1] == 0)

    a = Data(field(X.iset, demo.X, new_X(1, 2, 3)))
    #
    assert np.allclose(a[X].field.as_array().x, 1)
    assert np.allclose(a[X].field.as_array().y[:, 0], 2)
    assert np.allclose(a[X].field.as_array().y[:, 1], 3)
    #
    assert np.allclose(a[X, "x"].field.as_array(), 1)
    assert X == (a[X, "x"] == 1)
    #
    assert np.allclose(a[X, "y"].field.as_array(), [2, 3])
    assert X == (a[X, "y"] == [2, 3])
    assert X == (a[X, "y"][X, 0] == 2)
    assert X == (a[X, "y"][X, 1] == 3)


def test_data_fancy_setitem():
    field = lambda iset, type, values: (
        f := icus.field(iset, type),
        f.__setitem__(..., values),
    )[0]
    new_X = lambda a, b, c: (
        x := demo.X(),
        setattr(x, "x", a),
        x.y.__setitem__(..., (b, c)),
    )[0]

    X = Zone.from_size(6)
    Xa = Zone(X.iset.extract(list(X.iset)[::3]))
    Xb = Zone(X.iset.extract(list(X.iset)[:3]))

    a = Data(field(X.iset, "double", 0))
    assert X == (a == 0)
    #
    a[Xa] = 1
    assert np.allclose(a.field.as_array(), [1, 0, 0, 1, 0, 0])
    #
    a[Xb] = 2
    assert np.allclose(a.field.as_array(), [2, 2, 2, 1, 0, 0])
    #
    a[X - X] = 3
    assert np.allclose(a.field.as_array(), [2, 2, 2, 1, 0, 0])
    #
    a[:] = Data(field(X.iset, "double", -1))
    assert X == (a[:] == -1)

    ea, eb, ec = [demo.MyEnum(i) for i in range(3)]
    a = Data(field(X.iset, demo.MyEnum, ea))
    assert X == (a == ea)
    #
    a[Xa] = eb
    assert Xa == (a == eb)
    assert X - Xa == (a == ea)
    #
    a[:] = Data(field(X.iset, demo.MyEnum, eb))
    assert X == (a == eb)
    #
    a[:] = ec
    assert X == (a == ec)

    t = icus.tensor((2,), "d")
    v = lambda a, b: (x := t(), x.as_array().__setitem__(..., [a, b]))[0]
    a = Data(field(X.iset, t, t()))
    assert X == (a[..., 0] == 0)
    assert X == (a[..., 1] == 0)
    #
    a[Xa] = v(1, 2)
    assert Xa == (a[..., 0] == 1)
    assert Xa == (a[..., 1] == 2)
    assert X - Xa == (a[..., 0] == 0)
    assert X - Xa == (a[..., 1] == 0)
    #
    a[Xb, 0] = 3
    assert Xb == (a[..., 0] == 3)
    assert Xa - Xb == (a[..., 0] == 1)
    assert X - Xa - Xb == (a[..., 0] == 0)
    assert Xa == (a[..., 1] == 2)
    assert X - Xa == (a[..., 1] == 0)
    #
    a[:, 0] = Data(field(X.iset, "double", -1))
    assert X == (a[..., 0] == -1)
    assert Xa == (a[..., 1] == 2)
    assert X - Xa == (a[..., 1] == 0)
    #
    assert X == (a[0] == -1)
    assert Xa == (a[1] == 2)
    assert X - Xa == (a[1] == 0)
    #
    a[0] = -2
    assert X == (a[0] == -2)
    assert Xa == (a[1] == 2)
    assert X - Xa == (a[1] == 0)
    #
    a[0] = Data(field(X.iset, "double", -3))
    assert X == (a[0] == -3)
    assert Xa == (a[1] == 2)
    assert X - Xa == (a[1] == 0)

    t = icus.tensor((2, 2), "d")
    v = lambda a, b, c, d: (x := t(), x.as_array().__setitem__(..., [[a, b], [c, d]]))[
        0
    ]
    a = Data(field(X.iset, t, t()))
    assert X == (a == [[0, 0], [0, 0]])
    #
    a[Xa] = v(1, 2, 3, 4)
    assert Xa == (a[:, 0, 0] == 1)
    assert Xa == (a[:, 0, 1] == 2)
    assert Xa == (a[:, 1, 0] == 3)
    assert Xa == (a[:, 1, 1] == 4)
    #
    a[Xb, 0, 1] = 20
    assert Xb == (a[:, 0, 1] == 20)
    assert Xa - Xb == (a[:, 0, 1] == 2)
    assert X - Xa - Xb == (a[:, 0, 1] == 0)
    assert Xa == (a[:, 0, 0] == 1)
    assert Xa == (a[:, 1, 0] == 3)
    assert Xa == (a[:, 1, 1] == 4)
    assert X - Xa == (a[:, 0, 0] == 0)
    assert X - Xa == (a[:, 1, 0] == 0)
    assert X - Xa == (a[:, 1, 1] == 0)
    #
    a[:, 0, 1] = Data(field(X.iset, "double", -1))
    assert X == (a[:, 0, 1] == -1)
    assert Xa == (a[:, 0, 0] == 1)
    assert Xa == (a[:, 1, 0] == 3)
    assert Xa == (a[:, 1, 1] == 4)
    assert X - Xa == (a[:, 0, 0] == 0)
    assert X - Xa == (a[:, 1, 0] == 0)
    #
    assert X == (a[0, 1] == -1)
    assert Xa == (a[0, 0] == 1)
    assert Xa == (a[1, 0] == 3)
    assert Xa == (a[1, 1] == 4)
    assert X - Xa == (a[0, 0] == 0)
    assert X - Xa == (a[1, 0] == 0)
    #
    a[0, 1] = -2
    assert X == (a[0, 1] == -2)
    #
    a[0, 1] = Data(field(X.iset, "double", -3))
    assert X == (a[0, 1] == -3)

    a = Data(field(X.iset, demo.X, new_X(1, 2, 3)))
    assert X == (a[:, "x"] == 1)
    assert X == (a[:, "y"] == [2, 3])
    #
    a[Xa] = new_X(10, 20, 30)
    assert Xa == (a[:, "x"] == 10)
    assert Xa == (a[:, "y"] == [20, 30])
    #
    a[Xb, "x"] = 100
    assert Xb == (a[:, "x"] == 100)
    assert Xa - Xb == (a[:, "x"] == 10)
    assert X - Xa - Xb == (a[:, "x"] == 1)
    assert Xa == (a[:, "y"] == [20, 30])
    assert X - Xa == (a[:, "y"] == [2, 3])
    #
    a[:, "x"] = Data(field(X.iset, "double", -1))
    assert X == (a[:, "x"] == -1)
    assert Xa == (a[:, "y"] == [20, 30])
    assert X - Xa == (a[:, "y"] == [2, 3])
    #
    assert X == (a["x"] == -1)
    assert Xa == (a["y"] == [20, 30])
    assert X - Xa == (a["y"] == [2, 3])
    #
    a["x"] = -2
    assert X == (a["x"] == -2)
    assert Xa == (a["y"] == [20, 30])
    assert X - Xa == (a["y"] == [2, 3])
    #
    a["x"] = Data(field(X.iset, "double", -3))
    assert X == (a["x"] == -3)
    assert Xa == (a["y"] == [20, 30])
    assert X - Xa == (a["y"] == [2, 3])
    #
    v = lambda a, b: (
        x := icus.tensor((2,), "d")(),
        x.as_array().__setitem__(..., [a, b]),
    )[0]
    a["y"] = v(-10, -20)
    assert X == (a["y"][0] == -10)
    #
    assert X == (a.x == -3)
    assert X == (a.y == [-10, -20])
    #
    a.x = -4
    assert X == (a.x == -4)


def test_data_tensor_2_set_with_list():
    X = Zone(icus.ISet(6))
    a = Data(icus.field(X.iset, icus.tensor((2,))))
    a[:] = [1, 2]


def test_data_struc_with_subarray_set_one_comp():
    X = Zone.from_size(6)
    a = Data(icus.field(X.iset, demo.X))
    a["y", 0] = 1
    a.y[0] = 1
    assert X == (a.y[0] == 1)


def test_data_struc_with_subarray_set_all_subarray():
    X = Zone(icus.ISet(6))
    a = Data(icus.field(X.iset, demo.X))
    a.y = _vector([1, 2])
    assert X == (a.y[0] == 1)
    assert X == (a.y[1] == 2)
    a.y = [10, 20]
    assert X == (a.y[0] == 10)
    assert X == (a.y[1] == 20)


def test_data_tensor_ufunc():
    X = Zone.from_size(6)
    Y = Zone(X.iset.extract([5]))
    a = Data(icus.field(X.iset, icus.tensor((2,))))
    np.asarray(a)[:, 0] = np.arange(len(X))
    np.asarray(a)[:, 1] = np.arange(len(X)) + 10
    assert np.allclose(
        a,
        [[0.0, 10.0], [1.0, 11.0], [2.0, 12.0], [3.0, 13.0], [4.0, 14.0], [5.0, 15.0]],
    )
    # 1d formula : scalar data -> scalar data
    assert np.allclose(a[0] + a[1], np.arange(len(X)) * 2 + 10)
    assert Y == (a[0] * 3 >= a[1])
    # 2d formula : tensor data -> tensor data
    assert np.allclose(
        2 * a,
        [[0.0, 20.0], [2.0, 22.0], [4.0, 24.0], [6.0, 26.0], [8.0, 28.0], [10.0, 30.0]],
    )
    assert np.allclose(
        a + [100, 200],
        [
            [100.0, 210.0],
            [101.0, 211.0],
            [102.0, 212.0],
            [103.0, 213.0],
            [104.0, 214.0],
            [105.0, 215.0],
        ],
    )
    # sum
    assert np.sum(a) == 90
    assert np.sum(a[0]) == 15
    assert np.sum(a[1]) == 75
    assert list(np.sum(a, axis=0)) == [15, 75]
    assert X == (np.sum(a, axis=1) == a[0] + a[1])
    assert X == (np.sum(a**2, axis=1) == a[0] ** 2 + a[1] ** 2)
    # min/max
    assert list(np.min(a, axis=0)) == [0, 10]
    assert list(np.max(a, axis=0)) == [5, 15]
    # matmul is ufunc
    assert X == (a @ np.array([100, 1]) == a[0] * 100 + a[1])


def test_record():
    data = lambda zone, values: (
        d := Data.from_zone(zone),
        # todo: use d.__setitem__ instead of d.field.__setitem__
        d.field.__setitem__(..., values),
    )[0]
    same = lambda d1, d2: d1.zone == d2.zone == (d1 == d2)

    void = Zone()
    X = Zone(icus.ISet(6))
    Xa = Zone(X.iset.extract(list(X.iset)[::3]))
    Xb = Zone(X.iset.extract(list(X.iset)[:3]))
    Y = Zone.from_size(4)
    Ya = Zone(Y.iset.extract(list(Y.iset)[::2]))
    Yb = Zone(Y.iset.extract(list(Y.iset)[:2]))
    vx = data(X, [i for i in X.iset])
    vy = data(Y, [i for i in Y.iset])

    r = Record()
    r[...] = 1
    assert same(r[X], data(X, [1, 1, 1, 1, 1, 1]))
    assert same(r[Xa], data(Xa, [1, 1]))
    assert same(r[Xb], data(Xb, [1, 1, 1]))
    assert same(r[Y], data(Y, [1, 1, 1, 1]))
    assert same(r[Ya], data(Ya, [1, 1]))
    assert same(r[Yb], data(Yb, [1, 1]))
    assert same(r[void], data(void, []))

    r = Record()
    r[X] = 1
    assert same(r[X], data(X, [1, 1, 1, 1, 1, 1]))
    assert same(r[Xa], data(Xa, [1, 1]))
    assert same(r[Xb], data(Xb, [1, 1, 1]))
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record()
    r[Xa] = 1
    r[Ya] = 2
    with pytest.raises(ValueError):
        r[X]
    assert same(r[Xa], data(Xa, [1, 1]))
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    assert same(r[Ya], data(Ya, [2, 2]))
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record()
    with pytest.raises(ValueError):
        r[X]
    with pytest.raises(ValueError):
        r[Xa]
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record()
    r[...] = 1
    r[X] = 2
    r[Xa] = 3
    r[Xb] = 4
    assert same(r[X], data(X, [4, 4, 4, 3, 2, 2]))
    assert same(r[Xa], data(Xa, [4, 3]))
    assert same(r[Xb], data(Xb, [4, 4, 4]))
    assert same(r[Y], data(Y, [1, 1, 1, 1]))
    assert same(r[Ya], data(Ya, [1, 1]))
    assert same(r[Yb], data(Yb, [1, 1]))
    assert same(r[void], data(void, []))

    r = Record()
    r[...] = 1
    r[X] = 2
    r[Xa] = 3
    r[Xb] = 4
    r[...] = 0
    assert same(r[X], data(X, [0, 0, 0, 0, 0, 0]))
    assert same(r[Xa], data(Xa, [0, 0]))
    assert same(r[Xb], data(Xb, [0, 0, 0]))
    assert same(r[Y], data(Y, [0, 0, 0, 0]))
    assert same(r[Ya], data(Ya, [0, 0]))
    assert same(r[Yb], data(Yb, [0, 0]))
    assert same(r[void], data(void, []))

    r = Record()
    r[...] = 1
    r[...] = vx
    assert same(r[X], data(X, [0, 1, 2, 3, 4, 5]))
    assert same(r[Xa], data(Xa, [0, 3]))
    assert same(r[Xb], data(Xb, [0, 1, 2]))
    assert same(r[Y], data(Y, [1, 1, 1, 1]))
    assert same(r[Ya], data(Ya, [1, 1]))
    assert same(r[Yb], data(Yb, [1, 1]))
    assert same(r[void], data(void, []))
    foo = Data.from_zone(void)
    # error !
    # foo[...] = np.array([])


def test_record_state():
    data = lambda zone, values: (
        d := Data.from_zone(zone, demo.X),
        # todo: use d.__setitem__ instead of d.field.__setitem__
        d.field.__setitem__(..., values),
    )[0]
    same = lambda d1, d2: d1.zone == d2.zone == (d1 == d2)
    new_X = lambda a, b, c: (
        x := demo.X(),
        setattr(x, "x", a),
        x.y.__setitem__(..., (b, c)),
    )[0]
    v = lambda i: new_X(i * 100 + 1, i * 100 + 2, i * 100 + 3)

    v0 = new_X(0, 0, 0)
    v1 = new_X(10, 11, 12)
    v2 = new_X(20, 21, 22)
    v3 = new_X(30, 31, 32)
    v4 = new_X(40, 41, 42)
    v5 = new_X(50, 51, 52)

    void = Zone(icus.ISet())
    X = Zone.from_size(6)
    Xa = Zone(X.iset.extract(list(X.iset)[::3]))
    Xb = Zone(X.iset.extract(list(X.iset)[:3]))
    Y = Zone.from_size(4)
    Ya = Zone(Y.iset.extract(list(Y.iset)[::2]))
    Yb = Zone(Y.iset.extract(list(Y.iset)[:2]))
    vx = data(X, [new_X(i * 100 + 1, i * 100 + 2, i * 100 + 3) for i in X.iset])
    vy = data(Y, [new_X(i * 100 + 1, i * 100 + 2, i * 100 + 3) for i in Y.iset])

    r = Record(demo.X)
    r[...] = v1
    assert same(r[X], data(X, [v1, v1, v1, v1, v1, v1]))
    assert same(r[Xa], data(Xa, [v1, v1]))
    assert same(r[Xb], data(Xb, [v1, v1, v1]))
    assert same(r[Y], data(Y, [v1, v1, v1, v1]))
    assert same(r[Ya], data(Ya, [v1, v1]))
    assert same(r[Yb], data(Yb, [v1, v1]))
    assert same(r[void], data(void, []))

    r = Record(demo.X)
    r[X] = v1
    assert same(r[X], data(X, [v1, v1, v1, v1, v1, v1]))
    assert same(r[Xa], data(Xa, [v1, v1]))
    assert same(r[Xb], data(Xb, [v1, v1, v1]))
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record(demo.X)
    r[Xa] = v1
    r[Ya] = v2
    with pytest.raises(ValueError):
        r[X]
    assert same(r[Xa], data(Xa, [v1, v1]))
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    assert same(r[Ya], data(Ya, [v2, v2]))
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record(demo.X)
    with pytest.raises(ValueError):
        r[X]
    with pytest.raises(ValueError):
        r[Xa]
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record(demo.X)
    r[...] = v1
    r[X] = v2
    r[Xa] = v3
    r[Xb] = v4
    assert same(r[X], data(X, [v4, v4, v4, v3, v2, v2]))
    assert same(r[Xa], data(Xa, [v4, v3]))
    assert same(r[Xb], data(Xb, [v4, v4, v4]))
    assert same(r[Y], data(Y, [v1, v1, v1, v1]))
    assert same(r[Ya], data(Ya, [v1, v1]))
    assert same(r[Yb], data(Yb, [v1, v1]))
    assert same(r[void], data(void, []))

    r = Record(demo.X)
    r[...] = v1
    r[X] = v2
    r[Xa] = v3
    r[Xb] = v4
    r[...] = v0
    assert same(r[X], data(X, [v0, v0, v0, v0, v0, v0]))
    assert same(r[Xa], data(Xa, [v0, v0]))
    assert same(r[Xb], data(Xb, [v0, v0, v0]))
    assert same(r[Y], data(Y, [v0, v0, v0, v0]))
    assert same(r[Ya], data(Ya, [v0, v0]))
    assert same(r[Yb], data(Yb, [v0, v0]))
    assert same(r[void], data(void, []))

    r = Record(demo.X)
    r[...] = v1
    r[...] = vx
    assert same(r[X], data(X, [v(0), v(1), v(2), v(3), v(4), v(5)]))
    assert same(r[Xa], data(Xa, [v(0), v(3)]))
    assert same(r[Xb], data(Xb, [v(0), v(1), v(2)]))
    assert same(r[Y], data(Y, [v1, v1, v1, v1]))
    assert same(r[Ya], data(Ya, [v1, v1]))
    assert same(r[Yb], data(Yb, [v1, v1]))
    assert same(r[void], data(void, []))


def test_record_enum():
    data = lambda zone, values: (
        d := Data.from_zone(zone, demo.MyEnum),
        # todo: use d.__setitem__ instead of d.field.__setitem__
        d.field.__setitem__(..., values),
    )[0]
    same = lambda d1, d2: d1.zone == d2.zone == (d1 == d2)
    v = lambda i: [va, vb, vc][i % 3]

    va = demo.MyEnum(demo.MyEnum.A)
    vb = demo.MyEnum(demo.MyEnum.B)
    vc = demo.MyEnum(demo.MyEnum.C)

    void = Zone(icus.ISet())
    X = Zone(icus.ISet(6))
    Xa = Zone(X.iset.extract(list(X.iset)[::3]))
    Xb = Zone(X.iset.extract(list(X.iset)[:3]))
    Y = Zone.from_size(4)
    Ya = Zone(Y.iset.extract(list(Y.iset)[::2]))
    Yb = Zone(Y.iset.extract(list(Y.iset)[:2]))
    vx = data(X, [v(i) for i in X.iset])

    r = Record(demo.MyEnum)
    r[...] = va
    assert same(r[X], data(X, [va, va, va, va, va, va]))
    assert same(r[Xa], data(Xa, [va, va]))
    assert same(r[Xb], data(Xb, [va, va, va]))
    assert same(r[Y], data(Y, [va, va, va, va]))
    assert same(r[Ya], data(Ya, [va, va]))
    assert same(r[Yb], data(Yb, [va, va]))
    assert same(r[void], data(void, []))

    r = Record(demo.MyEnum)
    r[X] = va
    assert same(r[X], data(X, [va, va, va, va, va, va]))
    assert same(r[Xa], data(Xa, [va, va]))
    assert same(r[Xb], data(Xb, [va, va, va]))
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record(demo.MyEnum)
    r[Xa] = va
    r[Ya] = vb
    with pytest.raises(ValueError):
        r[X]
    assert same(r[Xa], data(Xa, [va, va]))
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    assert same(r[Ya], data(Ya, [vb, vb]))
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record(demo.MyEnum)
    with pytest.raises(ValueError):
        r[X]
    with pytest.raises(ValueError):
        r[Xa]
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], data(void, []))

    r = Record(demo.MyEnum)
    r[...] = va
    r[X] = vb
    r[Xa] = vc
    r[Xb] = vb
    assert same(r[X], data(X, [vb, vb, vb, vc, vb, vb]))
    assert same(r[Xa], data(Xa, [vb, vc]))
    assert same(r[Xb], data(Xb, [vb, vb, vb]))
    assert same(r[Y], data(Y, [va, va, va, va]))
    assert same(r[Ya], data(Ya, [va, va]))
    assert same(r[Yb], data(Yb, [va, va]))
    assert same(r[void], data(void, []))

    r = Record(demo.MyEnum)
    r[...] = va
    r[X] = vb
    r[Xa] = vc
    r[Xb] = vb
    r[...] = vc
    assert same(r[X], data(X, [vc, vc, vc, vc, vc, vc]))
    assert same(r[Xa], data(Xa, [vc, vc]))
    assert same(r[Xb], data(Xb, [vc, vc, vc]))
    assert same(r[Y], data(Y, [vc, vc, vc, vc]))
    assert same(r[Ya], data(Ya, [vc, vc]))
    assert same(r[Yb], data(Yb, [vc, vc]))
    assert same(r[void], data(void, []))

    r = Record(demo.MyEnum)
    r[...] = va
    r[...] = vx
    assert same(r[X], data(X, [v(0), v(1), v(2), v(3), v(4), v(5)]))
    assert same(r[Xa], data(Xa, [v(0), v(3)]))
    assert same(r[Xb], data(Xb, [v(0), v(1), v(2)]))
    assert same(r[Y], data(Y, [va, va, va, va]))
    assert same(r[Ya], data(Ya, [va, va]))
    assert same(r[Yb], data(Yb, [va, va]))
    assert same(r[void], data(void, []))


def test_record_Vector2():
    T = icus.tensor((2,), "d")
    empty_field = lambda iset: icus.field(iset, T)
    field = lambda iset, values: (
        f := empty_field(iset),
        f.__setitem__(..., np.asarray(values)),
    )[0]
    same = lambda d1, d2: d1.zone == d2.zone == (d1 == d2)

    void = Zone()
    X = Zone(icus.ISet(6))
    Xa = Zone(X.iset.extract(list(X.iset)[::3]))
    Xb = Zone(X.iset.extract(list(X.iset)[:3]))
    Y = Zone.from_size(4)
    Ya = Zone(Y.iset.extract(list(Y.iset)[::2]))
    Yb = Zone(Y.iset.extract(list(Y.iset)[:2]))
    vx = Data(field(X.iset, [[10 + i, 100 + i] for i in X.iset]))
    vy = Data(field(Y.iset, [[10 + i, 100 + i] for i in Y.iset]))

    r = Record(kind=Vector(2))
    r[:] = [1, 2]
    assert X == (r[X] == [1, 2])

    r = Record(kind=Vector(2))
    r[...] = [1, 2]
    assert same(
        r[X], Data(field(X.iset, [[1, 2], [1, 2], [1, 2], [1, 2], [1, 2], [1, 2]]))
    )
    assert same(r[Xa], Data(field(Xa.iset, [[1, 2], [1, 2]])))
    assert same(r[Xb], Data(field(Xb.iset, [[1, 2], [1, 2], [1, 2]])))
    assert same(r[Y], Data(field(Y.iset, [[1, 2], [1, 2], [1, 2], [1, 2]])))
    assert same(r[Ya], Data(field(Ya.iset, [[1, 2], [1, 2]])))
    assert same(r[Yb], Data(field(Yb.iset, [[1, 2], [1, 2]])))
    assert same(r[void], Data(empty_field(void.iset)))

    r = Record(kind=Vector(2))
    r[X] = [1, 2]
    assert same(
        r[X], Data(field(X.iset, [[1, 2], [1, 2], [1, 2], [1, 2], [1, 2], [1, 2]]))
    )
    assert same(r[Xa], Data(field(Xa.iset, [[1, 2], [1, 2]])))
    assert same(r[Xb], Data(field(Xb.iset, [[1, 2], [1, 2], [1, 2]])))
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], Data(empty_field(void.iset)))

    r = Record(kind=Vector(2))
    r[Xa] = [1, 2]
    r[Ya] = [11, 12]
    with pytest.raises(ValueError):
        r[X]
    assert same(r[Xa], Data(field(Xa.iset, [[1, 2], [1, 2]])))
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    assert same(r[Ya], Data(field(Ya.iset, [[11, 12], [11, 12]])))
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], Data(empty_field(void.iset)))

    r = Record(kind=Vector(2))
    with pytest.raises(ValueError):
        r[X]
    with pytest.raises(ValueError):
        r[Xa]
    with pytest.raises(ValueError):
        r[Xb]
    with pytest.raises(ValueError):
        r[Y]
    with pytest.raises(ValueError):
        r[Ya]
    with pytest.raises(ValueError):
        r[Yb]
    assert same(r[void], Data(empty_field(void.iset)))

    r = Record(kind=Vector(2))
    r[...] = [1, 2]
    r[X] = [11, 12]
    r[Xa] = [101, 102]
    r[Xb] = [1001, 1002]
    assert same(
        r[X],
        Data(
            field(
                X.iset,
                [
                    [1001, 1002],
                    [1001, 1002],
                    [1001, 1002],
                    [101, 102],
                    [11, 12],
                    [11, 12],
                ],
            )
        ),
    )
    assert same(r[Xa], Data(field(Xa.iset, [[1001, 1002], [101, 102]])))
    assert same(r[Xb], Data(field(Xb.iset, [[1001, 1002], [1001, 1002], [1001, 1002]])))
    assert same(r[Y], Data(field(Y.iset, [[1, 2], [1, 2], [1, 2], [1, 2]])))
    assert same(r[Ya], Data(field(Ya.iset, [[1, 2], [1, 2]])))
    assert same(r[Yb], Data(field(Yb.iset, [[1, 2], [1, 2]])))
    assert same(r[void], Data(empty_field(void.iset)))

    r = Record(kind=Vector(2))
    r[...] = [1, 2]
    r[X] = [11, 12]
    r[Xa] = [101, 102]
    r[Xb] = [1001, 1002]
    r[...] = [0, 0]
    assert same(
        r[X], Data(field(X.iset, [[0, 0], [0, 0], [0, 0], [0, 0], [0, 0], [0, 0]]))
    )
    assert same(r[Xa], Data(field(Xa.iset, [[0, 0], [0, 0]])))
    assert same(r[Xb], Data(field(Xb.iset, [[0, 0], [0, 0], [0, 0]])))
    assert same(r[Y], Data(field(Y.iset, [[0, 0], [0, 0], [0, 0], [0, 0]])))
    assert same(r[Ya], Data(field(Ya.iset, [[0, 0], [0, 0]])))
    assert same(r[Yb], Data(field(Yb.iset, [[0, 0], [0, 0]])))
    assert same(r[void], Data(empty_field(void.iset)))

    r = Record(kind=Vector(2))
    r[...] = [1, 2]
    r[...] = vx
    assert same(
        r[X],
        Data(
            field(
                X.iset,
                [
                    [10.0, 100.0],
                    [11.0, 101.0],
                    [12.0, 102.0],
                    [13.0, 103.0],
                    [14.0, 104.0],
                    [15.0, 105.0],
                ],
            )
        ),
    )
    assert same(r[Xa], Data(field(Xa.iset, [[10, 100], [13, 103]])))
    assert same(
        r[Xb],
        Data(
            field(
                Xb.iset,
                [
                    [10.0, 100.0],
                    [11.0, 101.0],
                    [12.0, 102.0],
                ],
            )
        ),
    )
    assert same(r[Y], Data(field(Y.iset, [[1, 2], [1, 2], [1, 2], [1, 2]])))
    assert same(r[Ya], Data(field(Ya.iset, [[1, 2], [1, 2]])))
    assert same(r[Yb], Data(field(Yb.iset, [[1, 2], [1, 2]])))
    assert same(r[void], Data(empty_field(void.iset)))


def test_record_Tensor2():
    T = icus.tensor((2, 2), "d")
    empty_field = lambda iset: icus.field(iset, T)
    field = lambda iset, values: (
        f := empty_field(iset),
        f.__setitem__(..., np.asarray(values)),
    )[0]
    same = lambda d1, d2: d1.zone == d2.zone == (d1 == d2)

    void = Zone(icus.ISet())
    X = Zone(icus.ISet(6))
    Xa = Zone(X.iset.extract(list(X.iset)[::3]))
    Xb = Zone(X.iset.extract(list(X.iset)[:3]))
    Y = Zone(icus.ISet(4))
    Ya = Zone(Y.iset.extract(list(Y.iset)[::2]))
    Yb = Zone(Y.iset.extract(list(Y.iset)[:2]))
    # vx = Data(field(X.iset, [[10 + i, 100 + i] for i in X.iset]))
    # vy = Data(field(Y.iset, [[10 + i, 100 + i] for i in Y.iset]))

    r = Record(kind=Tensor(2))
    r[:] = 1
    assert X == (r[X] == [[1, 0], [0, 1]])

    r = Record(kind=Tensor(2))
    r[:] = [1, 2]
    assert X == (r[X] == [[1, 0], [0, 2]])

    r = Record(kind=Tensor(2))
    r[:] = [[1, 0], [0, 2]]
    assert X == (r[X] == [[1, 0], [0, 2]])


def test_shelf():
    from icus.fig import Shelf

    s = Shelf({"a": {"b": 1}, "c": [2]})

    assert s.a.b == 1
    assert s.c == [2]

    d = s.to_dict()
    assert d == {"a": {"b": 1}, "c": [2]}
    d["d"] = 3
    assert s.to_dict() == {"a": {"b": 1}, "c": [2]}
    d["c"].append(4)
    assert s.to_dict() == {"a": {"b": 1}, "c": [2, 4]}

    assert s.c == [2, 4]


def test_record_shelf():
    data = RecordShelf(
        {
            "coefficients": {
                "diffusion": ("double", Scalar),
                "relaxation_time": ("double", Scalar),
            },
            "source_term": ("double", Scalar),
            "boundary_conditions": {
                "Dirichlet": ("double", Scalar),
                "Neumann": ("double", Scalar),
            },
            "initial_state": ("double", Scalar),
        }
    )

    assert isinstance(data.initial_state, Record)
    assert isinstance(data.source_term, Record)
    assert isinstance(data.coefficients, RecordShelf)
    assert isinstance(data.coefficients.diffusion, Record)
    assert isinstance(data.coefficients.relaxation_time, Record)
    assert isinstance(data.boundary_conditions, RecordShelf)
    assert isinstance(data.boundary_conditions.Dirichlet, Record)
    assert isinstance(data.boundary_conditions.Neumann, Record)

    data.normal_attribute = "anything"
    assert data.normal_attribute == "anything"
    with pytest.raises(AttributeError):
        data.doesnotexists

    with pytest.raises(AttributeError):
        data.initial_state = 1
    with pytest.raises(AttributeError):
        data.source_term = 1
    with pytest.raises(AttributeError):
        data.coefficients = 1
    with pytest.raises(AttributeError):
        data.coefficients.diffusion = 1
    with pytest.raises(AttributeError):
        data.coefficients.relaxation_time = 1
    with pytest.raises(AttributeError):
        data.boundary_conditions = 1
    with pytest.raises(AttributeError):
        data.boundary_conditions.Dirichlet = 1
    with pytest.raises(AttributeError):
        data.boundary_conditions.Neumann = 1

    X = Zone(icus.ISet(3))
    data.initial_state[...] = 1
    res = data.initial_state[X]
    assert np.allclose(res.field.as_array(), [1, 1, 1])
    data.coefficients.diffusion[...] = 2
    res = data.coefficients.diffusion[X]
    assert np.allclose(res.field.as_array(), [2, 2, 2])


def test_fig_data_access():
    I = icus.ISet(20)
    bar = Data(icus.field(I))
    # an index which is not in a tree has no meaning in icus
    with pytest.raises(TypeError):
        bar[6] = 3.1415
    bar[I.extract([6])] = 3.1415
    bar.field.as_array()[0] = 12.5
    assert np.array(bar)[6] == 3.1415
    assert bar[I.extract([0])] == 12.5


def test_fig_record_bool():
    foo = Record("bool", Scalar)
    I = icus.ISet(10)
    bar = Data(icus.field(I, dtype=bool))
    bar[...] = False
    bar[I.extract([0])] = True

    foo[...] = bar
    data_foo = foo[Zone(I)]
    a = data_foo.field.as_array()
    assert not a[4]
    data_foo[I.extract([4])] = True
    assert a[4]
