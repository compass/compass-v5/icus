#include <catch2/catch_test_macros.hpp>

#include "icus/Connectivity.h"

TEST_CASE("Connectivity preserve targets order") {
    using namespace icus;
    using indices = std::vector<index_type>;
    std::vector<indices> targets{{0, 1, 2}, {0, 2, 1}, {1, 0, 2},
                                 {1, 2, 0}, {2, 0, 1}, {2, 1, 0}};

    ISet I{targets.size()};
    ISet J{3};

    Connectivity C{I, J, targets, true};  // preserve targets order
    for (auto p = begin(targets); auto&& t : C.targets()) {
        CHECK(std::ranges::equal(*p, t));
        ++p;
    }

    auto C3 =
        make_connectivity<3>(I, J, targets, true);  // preserve targets order
    for (auto p = begin(targets); auto&& t : C3.targets()) {
        CHECK(std::ranges::equal(*p, t));
        ++p;
    }

    CHECK(!C.has_sorted_targets());
    C.sort_targets();
    CHECK(C.has_sorted_targets());
    for (auto&& r : C.targets()) {
        CHECK(std::ranges::equal(r, indices{0, 1, 2}));
    }

    Connectivity Cs{I, J, targets};  // default is to sort targets
    for (auto&& r : Cs.targets()) {
        CHECK(std::ranges::equal(r, indices{0, 1, 2}));
    }

    auto C3s =
        make_connectivity<3>(I, J, targets);  // default is to sort targets
    for (auto&& r : C3s.targets()) {
        CHECK(std::ranges::equal(r, indices{0, 1, 2}));
    }
}

TEST_CASE("Connectivity transposition is no longer involutive") {
    using namespace icus;
    using indices = std::vector<index_type>;
    std::vector<indices> targets{{0, 1}, {1, 0}};

    ISet I{2};
    Connectivity C{I, I, targets, true};  // preserve targets order
    CHECK_FALSE(C.has_sorted_targets());
    CHECK_FALSE(C.is_same(t(t(C))));
    CHECK(t(C).is_same(t(t(t(C)))));
    C.sort_targets();
    CHECK(C.has_sorted_targets());
    CHECK(C.is_same(t(t(C))));
    Connectivity Cs{I, I, targets};  // default is to sort targets
    CHECK(Cs.has_sorted_targets());
    CHECK(Cs.is_same(t(t(C))));
}
