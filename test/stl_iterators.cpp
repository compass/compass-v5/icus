#include <algorithm>
#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <vector>

#include "icus/ISet.h"

TEST_CASE("stl iterators") {
    auto I = icus::ISet(10);
    std::for_each(begin(I), end(I),
                  [](auto&& i) { std::cout << i << std::endl; });

    std::vector<icus::index_type> v;
    copy(begin(I), end(I), back_inserter(v));
}
