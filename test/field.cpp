#include <compass_cxx_utils/pretty_print.h>
#include <icus/Connectivity.h>
#include <icus/Field.h>
#include <icus/ISet.h>
#include <icus/field_operators.h>

#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>

using ivector = std::vector<icus::index_type>;

struct Pos {
    double x;
    double y;

    friend std::ostream& operator<<(std::ostream& os, const Pos& p) {
        os << "(" << p.x << ", " << p.y << ")";
        return os;
    }
    bool operator==(const Pos& other) const {
        return (x == other.x) && (y == other.y);
    }
};

void test_views() {
    using namespace icus;
    using std::views::iota;
    using std::views::zip;

    auto I = ISet(10);
    ivector Jmap{2, 7, 3};
    auto J = I.extract(Jmap);
    ivector Kmap{2, 0};
    auto K = J.extract(Kmap);
    auto PI = make_field(I).fill(iota(0, 10));
    CHECK(J.has_ancestor(I));
    auto PJ = view(PI, J);
    CHECK(PJ.size() == J.size());
    for (auto&& [j, i] : zip(J, Jmap)) {
        CHECK(PJ(j) == PI(i));
    }
    CHECK(K.has_ancestor(I));
    auto PK = view(PJ, K);
    for (auto&& [k, j] : zip(K, Kmap)) {
        CHECK(PK(k) == PI(Jmap[j]));
    }
    PJ.fill(3.14);
    auto inJ = make_field<bool>(I).fill(false);
    inJ.view(J).fill(true);
    for (auto&& i : I) {
        if (inJ(i)) {
            CHECK(PI(i) == 3.14);
        } else {
            CHECK(PI(i) == i);
        }
    }
}

void test_field_iterators() {
    using namespace icus;
    using std::views::iota;
    using std::views::zip;

    auto I = ISet(10);
    ivector Jmap{2, 7, 3};
    auto J = I.extract(Jmap);
    auto PI = make_field(I).fill(iota(0, 10));
    CHECK(J.has_ancestor(I));
    auto PJ = PI.view(J);
    std::cout << "PI:";
    for (auto&& x : PI) std::cout << " " << x;
    std::cout << std::endl;
    std::cout << "PJ:";
    for (auto&& x : PJ) std::cout << " " << x;
    std::cout << std::endl;

    STATIC_REQUIRE(std::forward_iterator<decltype(PI.begin())>);
    STATIC_REQUIRE(std::forward_iterator<decltype(PI.end())>);
    STATIC_REQUIRE(std::forward_iterator<decltype(PJ.begin())>);
    STATIC_REQUIRE(std::forward_iterator<decltype(PJ.end())>);
    STATIC_REQUIRE(std::ranges::range<decltype(PI)>);
    STATIC_REQUIRE(std::ranges::range<decltype(PJ)>);

    // test empty field
    PI.extract(ISet());
    auto empty_field = make_field(ISet());
    // Should not be possible to iterate over empty field
    CHECK(std::ranges::empty(empty_field));
}

void test_operators() {
    using namespace icus;
    using std::views::iota;
    using std::views::zip;
    using namespace icus::operators;
    compass::utils::Pretty_printer print;
    auto I = ISet(6);
    auto PI = make_field(I);
    auto reset = [&PI]() { PI.fill(iota(0, 6)); };
    reset();  // ~~~~~~~~~~~~~~~~~~~~~~~~
    PI *= 2;
    print(PI);
    for (auto&& [i, x] : zip(I, PI)) {
        CHECK(x == 2 * i);
    }
    // same loop as zip
    for (auto&& [i, x] : enumerate(PI)) {
        CHECK(x == 2 * i);
    }
    reset();  // ~~~~~~~~~~~~~~~~~~~~~~~~
    ivector Jmap{5, 1, 3};
    auto J = I.extract(Jmap);
    auto PJ = PI.view(J);
    PJ *= 2;  // will affect only values indexed by J
    print(PI);
    for (auto&& [i, x] : zip(I, PI)) {
        if (i % 2 == 0) {
            CHECK(x == i);
        } else {
            CHECK(x == 2 * i);
        }
    }
    reset();  // ~~~~~~~~~~~~~~~~~~~~~~~~
    for (auto&& [j, i] : zip(J, Jmap)) {
        CHECK(PI(i) == PJ(j));  // PJ remains a view of PI
    }
    PI *= PJ;  // will affect only values indexed by J
    for (auto&& i : I) {
        if (i % 2 == 0) {
            CHECK(PI(i) == i);
        } else {
            CHECK(PI(i) == i * i);
        }
    }
    reset();  // ~~~~~~~~~~~~~~~~~~~~~~~~
    PI += PI;
    print(PI);
    for (auto&& [i, x] : zip(I, PI)) {
        CHECK(x == 2 * i);
    }
    PI -= PI;
    CHECK(PI == 0);
    reset();  // ~~~~~~~~~~~~~~~~~~~~~~~~
    PI += 1;  // avoid 0
    PI /= PI;
    CHECK(PI == 1);
}

void test_field_of_tensors() {
    using namespace icus;
    using vertex = Tensor<double, 3>;

    auto I = ISet(3);
    std::vector<vertex> v{{{1., 2., 3.}}, {{2., 3., 1.}}, {{3., 1., 2.}}};

    auto vertices = make_field(I, std::move(v));

    vertex center = 0;  // calls operator=(Scalar)

    for (auto&& i : I) {
        center += vertices(i);
    }

    center /= I.size();

    CHECK(center(0) == 2.);
    CHECK(center(1) == 2.);
    CHECK(center(2) == 2.);
}

void test_field_of_tensors_other_syntax() {
    using namespace icus;
    using vertex = Tensor<double, 3>;

    auto center = [](const Field<vertex>& vertices) {
        vertex C = 0;  // calls operator=(Scalar)
        for (auto&& v : vertices) {
            C += v;
        }
        C /= vertices.size();
        return C;
    };

    auto I = ISet(3);
    auto vertices = Field<vertex>(I);
    vertices(0) = {1., 2., 3.};
    vertices(1) = {2., 3., 1.};
    vertices(2) = {3., 1., 2.};

    auto C = center(vertices);
    CHECK(C(0) == 2.);
    CHECK(C(1) == 2.);
    CHECK(C(2) == 2.);

    // recursively calls Tensor::operator=(Scalar) on all field elements
    vertices.fill(0);
    C = center(vertices);
    CHECK(C(0) == 0);
    CHECK(C(1) == 0);
    CHECK(C(2) == 0);
}

TEST_CASE("Field") {
    using namespace icus;
    using std::views::iota;
    // 0 - 1 - 2
    // |   |   |
    // 3 - 4 - 5
    // |   |   |
    // 6 - 7 - 8

    auto mesh = ISet(9);
    auto foo = Field(mesh);
    auto dummy = make_field(mesh);
    STATIC_REQUIRE(
        std::is_same_v<typename decltype(dummy)::value_type, double>);
    auto Pi = Field{mesh}.fill(3.14);
    STATIC_REQUIRE(std::is_same_v<typename decltype(Pi)::value_type, double>);
    CHECK(Pi == 3.14);
    Pos positions[] = {{0., 0.}, {0., 1.}, {0., 2.}, {1., 0.}, {1., 1.},
                       {1., 2.}, {2., 0.}, {2., 1.}, {2., 2.}};
    auto field = make_field<Pos>(mesh).fill(positions);
    CHECK(field == positions);

    std::cout << "2x2 mesh: vertices coordinates" << std::endl;
    std::cout << field << std::endl << std::endl;

    auto P = Field<int>{ISet(3)}.fill(std::array<int, 3>{1, 2, 3});

    std::vector<std::vector<index_type>> neighbourhood{
        {0, 1, 3},        // vertices connected to vertex 0
        {0, 1, 2, 4},     // vertices connected to vertex 1
        {1, 2, 5},        // vertices connected to vertex 2
        {0, 3, 4, 6},     // vertices connected to vertex 3
        {1, 3, 4, 5, 7},  // vertices connected to vertex 4
        {2, 4, 5, 8},     // vertices connected to vertex 5
        {3, 6, 7},        // vertices connected to vertex 6
        {4, 6, 7, 8},     // vertices connected to vertex 7
        {5, 7, 8}         // vertices connected to vertex 8
    };
    auto stencil = Connectivity(mesh, mesh, neighbourhood);

    auto edges_length = make_field(stencil.connections()).fill(1);
    STATIC_REQUIRE(
        std::is_same_v<typename decltype(edges_length)::value_type, double>);
    CHECK(edges_length == 1);

    std::cout << "3x3 mesh: edges length" << std::endl;
    std::cout << edges_length << std::endl << std::endl;

    auto vag_like_field =
        make_field<std::vector<double>>(stencil.connections());

    for (auto&& [i, neighbours] : stencil.targets_by_source()) {
        for (auto&& cij : stencil.connections_by_source(i)) {
            vag_like_field(cij).resize(neighbours.size());
            for (index_type k = 0; k < neighbours.size(); ++k) {
                vag_like_field(cij)[k] = 1.0;
            }
        }
    }

    std::cout << "Test nullify edges length:" << std::endl;
    edges_length.fill(0);
    std::cout << edges_length << std::endl << std::endl;

    // Fields are lightweight
    auto P1 = make_field<int>(mesh);
    P1.fill(iota(0, 9));
    auto P2 = P1;
    // explicitely request a copy, P1.copy() is available too
    auto P3 = copy(P1);
    CHECK(have_same_support(P1, P3));
    CHECK(have_same_support(P1, P2, P3));
    P2.fill(42);
    // P1 and P2 are referencing the same buffer
    CHECK(P1 == 42);
    CHECK(P3 == iota(0, 9));

    test_views();

    test_field_iterators();

    test_operators();

    test_field_of_tensors();
    test_field_of_tensors_other_syntax();
}
