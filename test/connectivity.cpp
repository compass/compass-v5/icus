#include "icus/Connectivity.h"

#include <compass_cxx_utils/pretty_print.h>

#include <catch2/catch_test_macros.hpp>
#include <iostream>
#include <ranges>

#include "icus/ISet.h"
#include "tricks/unused-variable.h"

void dump(auto &&conn) {
    for (auto &&[i, neighbours] : conn.targets_by_source()) {
        std::cout << "( " << i << " ) : ";
        for (auto &&neighbour : neighbours) {
            std::cout << neighbour << " ";
        }
        std::cout << std::endl;
    }
}

bool range_of_empty_ranges(auto &&R) {
    for (auto &&r : R) {
        if (!std::ranges::empty(r)) return false;
    }
    return true;
}

TEST_CASE("Connectivity tests") {
    SECTION("Test conn constructors") {
        using namespace icus;

        std::cout << "3x3 Mesh: \n";
        auto vertices = ISet(9);
        std::vector<std::vector<index_type>> neighbourhood{
            {0, 1, 3},        // vertices connected to vertex 0
            {0, 1, 2, 4},     // vertices connected to vertex 1
            {1, 2, 5},        // vertices connected to vertex 2
            {0, 3, 4, 6},     // vertices connected to vertex 3
            {1, 3, 4, 5, 7},  // vertices connected to vertex 4
            {2, 4, 5, 8},     // vertices connected to vertex 5
            {3, 6, 7},        // vertices connected to vertex 6
            {4, 6, 7, 8},     // vertices connected to vertex 7
            {5, 7, 8}         // vertices connected to vertex 8
        };
        auto stencil = Connectivity(vertices, vertices, neighbourhood);
        CHECK(stencil.source() == vertices);
        CHECK(stencil.target() == vertices);
        CHECK(stencil.image() == stencil.target());

        for (auto &&[i, neighbours] : stencil.targets_by_source()) {
            std::cout << "Source " << i << ": ";
            for (auto &&neighbour : neighbours) {
                std::cout << neighbour << " ";
            }
            std::cout << std::endl;
        }

        auto edges = vertices.extract({0, 1, 2, 3, 5, 6, 7, 8});
        auto edge_stencil = stencil.extract(edges, vertices);
        CHECK(edge_stencil.is_consistent());

        using indices = ISet::mapping_t::vector;
        auto triplets = vertices.extract(indices{0, 2, 6, 8});
        auto triplets_con = stencil.extract<3>(triplets, vertices);
        CHECK(triplets_con.is_consistent());
        CHECK(triplets_con.image() == edges);

        // build a regular connectivity directly provinding targets
        std::vector<index_type> targets;
        index_type n = 0;
        for (auto &&i : vertices) {
            UNUSED_VARIABLE(i)
            targets.emplace_back(size_to_index(n % vertices.size()));
            ++n;
            targets.emplace_back(size_to_index(n % vertices.size()));
        }
        auto regcon = make_connectivity<2>(vertices, vertices, targets);
    }

    SECTION("Test target access") {
        using namespace icus;
        constexpr int n = 6;

        compass::utils::Pretty_printer print;

        auto I = ISet(n);
        std::vector<std::vector<index_type>> v{n};
        for (index_type i = 0; i < n; ++i) {
            v[i].insert(v[i].end(), {i, (i + 1) % n});
        }
        auto con =
            make_connectivity<2>(I, I, v);  // constant target size cf. <2>
        // direct unpacking
        for (auto &&[t1, t2] : con.targets()) {
            std::cout << t1 << ", " << t2 << std::endl;
        }
        // or two steps
        for (auto &&a : con.targets()) {
            auto &&[t1, t2] = a;
            std::cout << t1 << ", " << t2 << std::endl;
        }
        // or with copy
        for (std::array<index_type, 2> a : con.targets()) {
            auto &&[t1, t2] = a;
            std::cout << t1 << ", " << t2 << std::endl;
        }
        print("Regular connectivity, targets by source:");
        CHECK(con.connections().size() == 2 * std::ranges::size(con.targets()));
        for (auto &&[source, targets] : con.targets_by_source()) {
            print(source, ":", targets);
        }
        for (auto &&i : con.source()) {
            // as con2 is a regular connectivity (constant targets size)
            // con.targets_by_source(i) is a const reference to a std::array
            // so we can unpack it
            auto &&[a, b] = con.targets_by_source(i);
            std::cout << "source " << i << " with targets: " << a << " and "
                      << b << std::endl;
        }

        auto con2 = make_connectivity(I, I, v);  // dynamic target size
        for (auto &&a : con2.targets()) {        // hence double loop
            for (auto &&t : a) std::cout << t << " ";
            std::cout << std::endl;
        }
        CHECK(con2.source() == I);
        CHECK(con2.target() == I);
        for (auto &&i : con2.source()) {
            // as con2 is a dynamic connectivity (variable targets size)
            // the following will NOT compile
            // auto&& [a, b] = con2.targets_by_source(i);
            auto &&row = con2.targets_by_source(i);
            CHECK(std::ranges::size(row) == 2);
        }
    }

    SECTION("Test transpose") {
        /*
        conn =
        . x .
        x . .
        . . x
        . x x
        x . x

        connT =
        . x . . x
        x . . x .
        . . x x x
        */

        using namespace icus;

        auto A = ISet(5);
        auto B = ISet(3);

        std::vector<std::vector<index_type>> neighbourhood{
            {1}, {0}, {2}, {1, 2}, {0, 2}};
        std::vector<std::vector<index_type>> neighbourhoodT{
            {1, 4}, {0, 3}, {2, 3, 4}};

        auto conn = Connectivity(A, B, neighbourhood);
        auto connT = Connectivity(B, A, neighbourhoodT);
        CHECK(conn.transpose().is_same(connT));
        /*
        conn is not structured but connT is structured
        conn =
        . x .
        x . .
        . . .
        . x x
        x . x

        connT =
        . x . . x
        x . . x .
        . . . x x
        */
        std::vector<std::vector<index_type>> neigh{
            {1}, {0}, {}, {1, 2}, {0, 2}};
        std::vector<std::vector<index_type>> neighT{{1, 4}, {0, 3}, {3, 4}};

        auto conn1 = Connectivity(A, B, neigh);
        auto connT1 = Connectivity<ISet, ISet, 2>(B, A, neighT);
        CHECK(conn1.transpose<2>().is_same(connT1));
    }

    SECTION("Test compose") {
        /*
           C1 =
           . x .
           x . .
           . x x
           x . x

           C2 =
           . x
           x .
           x x

           C3 = C1 o C2 =
           x .
           . x
           x x
           x x
        */

        using namespace icus;

        auto A = ISet(4);
        auto B = ISet(3);
        auto C = ISet(2);

        std::vector<std::vector<index_type>> N1{{1}, {0}, {1, 2}, {0, 2}};
        std::vector<std::vector<index_type>> N2{{1}, {0}, {0, 1}};
        std::vector<std::vector<index_type>> N3{{0}, {1}, {0, 1}, {1, 0}};

        auto C1 = Connectivity(A, B, N1);
        CHECK(C1.has_sorted_targets());
        auto C2 = Connectivity(B, C, N2);
        CHECK(C2.has_sorted_targets());
        auto C1oC2 = C1.compose(C2);
        CHECK(C1oC2.has_sorted_targets());
        auto C3 = Connectivity(A, C, N3);
        CHECK(C3.has_sorted_targets());  // default is to sort targets
        CHECK(C1oC2.is_same(C3));
        auto C3o = Connectivity(A, C, N3, true);  // preserve targets order
        CHECK_FALSE(C3o.has_sorted_targets());
        CHECK_FALSE(C1oC2.is_same(C3o));

        // Test composition between structured connectivities
        // (same number of targets for each source)
        /*
               D1 =
               x . x
               . x x
               x x .
               . x x

               D2 =
               . x
               x .
               . x

               D3 = D1 o D2 =
               . x
               x x
               x x
               x x
            */
        std::vector<std::vector<index_type>> M1{{0, 2}, {1, 2}, {0, 1}, {1, 2}};
        std::vector<std::vector<index_type>> M2{{1}, {0}, {1}};
        std::vector<std::vector<index_type>> M3{{1}, {0, 1}, {0, 1}, {0, 1}};

        auto D1 = Connectivity<ISet, ISet, 2>(A, B, M1);
        auto D2 = Connectivity<ISet, ISet, 1>(B, C, M2);
        auto D3 = Connectivity(A, C, M3);
        CHECK(D1.compose(D2).is_same(D3));

        // Test composition between structured connectivities
        // which returns a structured connectivity
        // (same number of targets for each source)
        /*
               D1 =
               . x x
               x x .
               . x x

               D2 =
               . x
               x .
               . x

               D3 = D1 o D2 =
               x x
               x x
               x x
            */
        auto D = ISet(3);
        auto E = ISet(3);
        auto F = ISet(2);
        std::vector<std::vector<index_type>> M4{{1, 2}, {0, 1}, {1, 2}};
        std::vector<std::vector<index_type>> M5{{1}, {0}, {1}};
        std::vector<std::vector<index_type>> M6{{0, 1}, {0, 1}, {0, 1}};

        auto D4 = Connectivity<ISet, ISet, 2>(D, E, M4);
        auto D5 = Connectivity<ISet, ISet, 1>(E, F, M5);
        auto D6 = Connectivity<ISet, ISet, 2>(D, F, M6);
        auto D7 = D4.compose<ISet, D5.targets_size_v, 2>(D5);
        static_assert(is_regular_connectivity<decltype(D7)>);
        CHECK(D7.is_same(D6));
    }

    SECTION("Test symmetrize") {
        /*
        conn =
        . x .
        x . .
        . . x
        . x x
        x . x

        connT =
        . x . . x
        x . . x .
        . . x x x

        conn o connT =
        x . . x .
        . x . . x
        . . x x x
        x . x x x
        . x x x x

        connT o conn =
        x . x
        . x x
        x x x

        */

        using namespace icus;

        auto A = ISet(5);
        auto B = ISet(3);

        std::vector<std::vector<index_type>> neighbourhood{
            {1}, {0}, {2}, {1, 2}, {0, 2}};
        std::vector<std::vector<index_type>> neighbourhoodCCT{
            {0, 3}, {1, 4}, {2, 3, 4}, {0, 2, 3, 4}, {1, 2, 3, 4}};
        std::vector<std::vector<index_type>> neighbourhoodCTC{
            {0, 2}, {1, 2}, {0, 1, 2}};

        auto conn = Connectivity(A, B, neighbourhood);
        auto CCT = Connectivity(A, A, neighbourhoodCCT);
        auto CTC = Connectivity(B, B, neighbourhoodCTC);

        auto connT = conn.transpose();
        std::cout << "CCT\n";
        dump(conn.compose(connT));
        std::cout << "CTC\n";
        dump(connT.compose(conn));
        CHECK(conn.compose(connT).is_same(CCT));
        CHECK(connT.compose(conn).is_same(CTC));
    }

    SECTION("Test intersect") {
        /*
        conn0 =
        . x .
        x . .
        . . .
        . . x
        . x x
        x . x

        conn1 =
        x . .
        x . .
        . . .
        . . x
        x x .
        . . x

        intersection =
        . . .
        x . .
        . . .
        . . x
        . x .
        . . x

        */

        using namespace icus;

        auto A = ISet(6);
        auto B = ISet(3);

        std::vector<std::vector<index_type>> neigh0{{1}, {0},    {},
                                                    {2}, {1, 2}, {0, 2}};
        std::vector<std::vector<index_type>> neigh1{{0}, {0},    {},
                                                    {2}, {0, 1}, {2}};
        std::vector<std::vector<index_type>> neighInter{{},  {0}, {},
                                                        {2}, {1}, {2}};

        auto conn0 = Connectivity(A, B, neigh0);
        auto conn1 = Connectivity(A, B, neigh1);
        auto connInter = Connectivity(A, B, neighInter);

        CHECK(intersect(conn0, conn1).is_same(connInter));
        CHECK(intersect(conn1, conn0).is_same(connInter));
        // intersect itself
        CHECK(intersect(conn0, conn0).is_same(conn0));
        CHECK(intersect(conn1, conn1).is_same(conn1));

        /*
        conn2 =
        . x .
        x . .
        . . .
        . . x
        . x x
        x . .

        conn3 =
        x . .
        . . x
        x x x
        . x .
        x . .
        . . x

        intersection =
        . . .
        . . .
        . . .
        . . .
        . . .
        . . .

        */
        std::vector<std::vector<index_type>> neigh2{{1}, {0},    {},
                                                    {2}, {1, 2}, {0}};
        std::vector<std::vector<index_type>> neigh3{{0}, {2}, {0, 1, 2},
                                                    {1}, {0}, {2}};
        std::vector<std::vector<index_type>> neighInt{{}, {}, {}, {}, {}, {}};

        auto conn2 = Connectivity(A, B, neigh2);
        auto conn3 = Connectivity(A, B, neigh3);
        auto connInt = Connectivity(A, B, neighInt);

        CHECK(intersect(conn2, conn3).is_same(connInt));
        CHECK(intersect(conn3, conn2).is_same(connInt));
        // intersect itself
        CHECK(intersect(conn2, conn2).is_same(conn2));
        CHECK(intersect(conn3, conn3).is_same(conn3));
    }

    SECTION("Test restrict target") {
        using namespace icus;
        compass::utils::Pretty_printer print;

        auto cells = ISet(3);
        auto faces = ISet(10);
        std::vector<std::vector<index_type>> faces_cells_vect{
            {0}, {1}, {2}, {0}, {0, 1}, {1, 2}, {2}, {0}, {1}, {2}};
        auto full_faces_cells = Connectivity(faces, cells, faces_cells_vect);
        auto sub_cells = cells.extract(std::vector{1, 2});
        auto sub_faces_cells =
            full_faces_cells.restrict_target(sub_cells, true);
        for (auto &&[f, n_cells] : sub_faces_cells.targets_by_source()) {
            CHECK(n_cells.size() > 0);
        }
    }

    SECTION("Test empty connectivity") {
        using namespace icus;

        auto vertices = ISet(12);
        auto cells = ISet(9);
        std::vector<std::vector<index_type>> neighbourhood{
            {0, 1, 3},         // vertices connected to cell 0
            {0, 1, 4, 9},      // vertices connected to cell 1
            {1, 2, 5},         // vertices connected to cell 2
            {0, 3, 4, 6},      // vertices connected to cell 3
            {1, 3, 4, 5, 11},  // vertices connected to cell 4
            {2, 4, 8, 10},     // vertices connected to cell 5
            {3, 6, 7},         // vertices connected to cell 6
            {4, 6, 7, 8},      // vertices connected to cell 7
            {5, 7, 8}          // vertices connected to cell 8
        };
        auto stencil = Connectivity(cells, vertices, neighbourhood);

        // extract connectivity with empty source iset
        auto empty_iset = ISet();
        // empty source
        auto empty_source = stencil.extract(empty_iset, vertices);
        std::cout << empty_source << std::endl;
        CHECK(empty_source.source().size() == 0);
        CHECK(empty_source.source().empty());
        CHECK(empty_source.connections().empty());
        CHECK(empty_source.targets().size() == 0);
        CHECK(empty_source.targets().empty());
        CHECK(empty_source.is_consistent());
        // Should not be possible to iterate over empty connectivity
        CHECK(std::ranges::empty(empty_source.targets()));
        // enumerate is not possible
        // for (auto &&[i, t] :
        // compass::utils::enumerate(empty_source.targets()))
        //     throw std::runtime_error(
        //         "Should not be possible to iterate over empty connectivity");
        auto other_empty_source = stencil.extract(ISet(), vertices);
        CHECK(empty_source.is_same(other_empty_source));

        // test empty connectivity when empty target
        // using transpose
        auto empty_target = empty_source.transpose();
        CHECK(empty_target.source() == vertices);
        CHECK(empty_target.target().empty());
        CHECK(empty_target.is_consistent());
        CHECK(empty_target.connections().empty());
        for (auto &&[s, targets] : empty_target.targets_by_source()) {
            UNUSED_VARIABLE(s);
            CHECK(std::ranges::empty(targets));
        }
        CHECK_FALSE(empty_source.is_same(empty_target));
        // targets is not empty whereas _targets is empty !
        CHECK(empty_target.target().empty());
        // because it has as many elements as the source ISet
        CHECK(empty_target.targets().size() == empty_target.source().size());
        CHECK(range_of_empty_ranges(empty_target.targets()));
        // test empty connectivity when empty target
        // using restrict target
        auto empty_restrict = stencil.restrict_target(ISet());
        CHECK(empty_restrict.source() == cells);
        CHECK(empty_restrict.target().empty());
        CHECK(empty_restrict.is_consistent());
        CHECK(empty_restrict.connections().empty());
        CHECK(range_of_empty_ranges(empty_restrict.targets()));
        auto empty_restrict_t = empty_restrict.transpose();
        CHECK(empty_restrict_t.source() == ISet());
        CHECK(empty_restrict_t.target() == cells);
        CHECK(empty_restrict_t.connections().empty());

        // and with simplify_source = true
        auto empty_restrict_simpl = stencil.restrict_target(ISet(), true);
        CHECK(empty_restrict_simpl.source().empty());
        CHECK(empty_restrict_simpl.target().empty());
        CHECK(empty_restrict_simpl.is_consistent());
        CHECK(empty_restrict_simpl.connections().empty());
        CHECK(std::ranges::empty(empty_restrict_simpl.targets_by_source()));
        auto empty_restrict_simpl_t = empty_restrict_simpl.transpose();
        CHECK(empty_restrict_simpl_t.source() == ISet());
        CHECK(empty_restrict_simpl_t.target() == ISet());
        CHECK(empty_restrict_simpl_t.connections().empty());

        // test empty connectivity when source and target are non empty
        // and no connection between them
        std::vector<std::vector<index_type>> no_connections{{}, {}, {}, {}, {},
                                                            {}, {}, {}, {}};
        auto no_conn = Connectivity(cells, vertices, no_connections);
        CHECK(no_conn.source().size() == cells.size());
        CHECK(no_conn.target().size() == vertices.size());
        CHECK(no_conn.is_consistent());
        CHECK(no_conn.connections().empty());
        CHECK(range_of_empty_ranges(no_conn.targets()));
        auto no_conn_t = no_conn.transpose();
        CHECK(no_conn_t.source() == vertices);
        CHECK(no_conn_t.target() == cells);
        CHECK(no_conn_t.connections().empty());
        auto empty_inter0 = intersect(stencil, no_conn);
        CHECK(empty_inter0.source() == cells);
        CHECK(empty_inter0.target() == vertices);
        CHECK(empty_inter0.connections().empty());
        // intersect is symetric
        auto empty_inter1 = intersect(no_conn, stencil);
        CHECK(empty_inter1.is_same(empty_inter0));

        // test composition with empty connectivity
        // (0xvertices)*(cellsx0) = (0*0)
        CHECK(empty_source.compose(empty_target).is_same(empty_restrict_simpl));
        // (cellsx0)*(0*vertices) = (cells*vertices)
        CHECK(empty_restrict.compose(empty_source).is_same(no_conn));
    }
}
