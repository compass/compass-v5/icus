#include <icus/dl-pack.h>

#include <catch2/catch_test_macros.hpp>
#include <iostream>
TEST_CASE("dl pack") {
    using namespace icus;
    auto pack = dl::pack<double>(std::initializer_list<std::size_t>{1, 2, 3});
    CHECK(pack.ndim() == 3);
    CHECK_FALSE(pack.shape.empty());
    std::cerr << "shape:";
    for (auto&& i : pack.shape) std::cerr << " " << i;
    std::cerr << std::endl;
    CHECK(pack.strides.empty());
}
