#include <icus/field-binding.h>
#include <icus/types.h>
#include <nanobind_utils/as-bytes.h>
#include <nanobind_utils/enum.h>
#include <nanobind_utils/numpy-dtype.h>

#include <iostream>
#include <type_traits>

struct X {
    double x, y[2];
};

std::ostream& operator<<(std::ostream& os, const X& x) {
    os << x.x << " (" << x.y[0] << ", " << x.y[1] << ")" << std::endl;
    return os;
}

enum struct MyEnum : char { A, B, C };

NB_MAKE_OPAQUE(MyEnum);

std::ostream& operator<<(std::ostream& os, const MyEnum&) {
    os << "nope";
    return os;
}

enum struct BC : char { dirichlet, neumann, interior };

NB_MAKE_OPAQUE(BC);

struct Site {
    BC pressure;
    BC temperature;
};

namespace nbu = compass::nanobind;
// using namespace icus::binding;

NB_MODULE(demo, m) {
    // FIXME: generate the field name

    nb::class_<X> pyX{m, "X"};
    pyX.def(nb::init<>());
    pyX.def_rw("x", &X::x);
    pyX.def_prop_ro(
        "y",
        [](nb::handle h) {
            return nb::ndarray<double, nb::numpy>(&(nb::cast<X&>(h).y), {2}, h);
        },
        nb::keep_alive<0, 1>());
    nbu::add_numpy_dtype(pyX, {{"x", "double"}, {"y", "double", {2}}});
    nbu::add_as_bytes(pyX);
    icus::bind_field(m, pyX, "Field_of_X");

    using Vertex = std::array<double, 3>;
    nb::class_<Vertex> pyVertex{m, "Vertex"};
    pyVertex.def(nb::init<>());
    pyVertex.def(
        "as_array",  // could be any name that you find explicit
                     // this binding is NOT mandatory for field exposition
                     // as packs to work (cf. below)
        [](nb::handle self) {
            return nb::ndarray<double, nb::numpy>{
                nb::cast<Vertex&>(self).data(), {3}, self};
        },
        nb::keep_alive<0, 1>());
    icus::bind_field(m, pyVertex, "Vertices", icus::Pack<Vertex>::info());

    // enum (cf. icus #104) - and nanobind-utils test/test_enum.py
    auto enum_cls = nbu::bind_enum<MyEnum>(m, "MyEnum",
                                           {
                                               {MyEnum::A, "A"},
                                               {MyEnum::B, "B"},
                                               {MyEnum::C, "C"},
                                           });
    // then bind the corresponding field
    icus::bind_field(m, enum_cls, "Field_of_MyEnum",
                     icus::pack<std::underlying_type_t<MyEnum>>());

    // another example
    // bind subtype first
    nbu::bind_enum<BC>(m, "BC",
                       {{BC::dirichlet, "dirichlet"},
                        {BC::neumann, "neumann"},
                        {BC::interior, "interior"}});
    // then bind the enclosing type...
    nb::class_<Site> pySite{m, "Site"};
    pySite.def(nb::init<>());
    pySite.def_rw("pressure", &Site::pressure);
    pySite.def_rw("temperature", &Site::temperature);
    // FIXME: add converter C++ type -> numpy string
    // the fact to add a numpy dtype will enable
    // the conversion of the associated field as a numpy recarray
    nbu::add_numpy_dtype(pySite,
                         {{"pressure", "int8"}, {"temperature", "int8"}});
    // ... and the associated field
    icus::bind_field(m, pySite, "Sites");
}
