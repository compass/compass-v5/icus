import pytest
import numpy as np
import icus

import icus_field_demo as demo


def setX(X, x, y):
    X.x = x
    X.y[:] = y


def test_indexing_from_mapping_values():
    I = icus.ISet(10)
    J = I.extract([1, 2])
    a = icus.field(I)
    for i in I:
        a[i]
    for j in J.mapping():
        a[j]


def test_more_structured_tests():
    I = icus.ISet(3)
    p = icus.field(I, demo.X)
    p[0].x = 1

    a = p.as_bytes().view(int)
    a[:] = 42
    assert np.all(p.as_bytes().view(int) == 42)

    a = p.as_bytes().view(demo.X.numpy_dtype, np.recarray)
    a.y[:] = 3.14, 0
    assert np.all(a.y[:, 0] == 3.14)
    assert p[2].y[0] == 3.14

    a = p.as_bytes().view(demo.X.numpy_dtype, np.recarray)
    a.y[:, 1] = 3.14
    assert np.all(a.y == 3.14)

    # directly returns a structured array
    b = p.as_array()
    assert np.all(b.y == a.y)

    X = demo.X()
    a = X.as_bytes().view(int)
    a[:] = 42
    assert np.all(X.as_bytes().view(int) == 42)


def test_state_binding():
    x = demo.X()
    assert x.numpy_dtype == np.dtype(
        [
            ("x", "double"),
            (
                "y",
                "double",
                [
                    2,
                ],
            ),
        ]
    )
    dt = x.numpy_dtype
    x.as_bytes()[:] = [
        0,
        0,
        0,
        0,
        0,
        0,
        240,
        63,
        0,
        0,
        0,
        0,
        0,
        0,
        0,
        64,
        0,
        0,
        0,
        0,
        0,
        0,
        8,
        64,
    ]
    y = x.as_bytes().view(dt, np.recarray).reshape(())
    assert y.x == 1
    assert y.y[0] == 2
    assert y.y[1] == 3


def test_factory():
    I = icus.ISet(10)
    # native type
    p = icus.field(I)
    assert isinstance(p, icus.FieldDouble)
    p = icus.field(I, np.int64)
    assert isinstance(p, icus.FieldInt64)
    # FIXME: we assume python float are C++ double
    p = icus.field(I, float)
    assert isinstance(p, icus.FieldDouble)
    p = icus.field(I, bool)
    assert isinstance(p, icus.FieldBool)
    # custom type : state
    p = icus.field(I, demo.X)
    assert isinstance(p, demo.Field_of_X)
    p = icus.field(I, demo.MyEnum)
    assert isinstance(p, demo.Field_of_MyEnum)
    p = icus.field(I, "byte")
    assert isinstance(p, icus.FieldByte)


def test_field_state_X():
    I = icus.ISet(2)
    # instantiate
    p = icus.field(I, demo.X)
    # get size
    assert p.size() == 2
    # setitem
    setX(p[0], 1, (2, 3))
    setX(p[1], 11, (22, 33))
    # getitem
    assert p[0].x == 1
    assert np.allclose(p[0].y, [2, 3])
    assert p[1].x == 11
    assert np.allclose(p[1].y, [22, 33])
    arr = p.as_array()
    assert arr.shape == (2,)
    # read view
    assert np.allclose(arr.x, [1, 11])
    assert np.allclose(arr.y, [[2, 3], [22, 33]])
    # write view
    arr.view(float)[:] *= -1
    # array view and field share memory
    assert np.allclose(arr.x, [-1, -11])
    assert np.allclose(arr.y, [[-2, -3], [-22, -33]])


def test_field_native_float():
    I = icus.ISet(10)
    # instantiate
    p = icus.field(I)  # default type is np.float64
    # get size
    assert p.size() == 10
    # setitem
    for i in range(10):
        p[i] = i**2
    # getitem
    for i in range(10):
        assert p[i] == i**2
    # array view
    arr = p.as_array()
    # read view
    assert np.allclose(arr, np.arange(10) ** 2)
    # write view
    arr[:] = -np.arange(10) ** 2
    # array view and field share memory
    for i in range(10):
        assert p[i] == -(i**2)


def test_fieldX_bis():
    I = icus.ISet(10)
    p = icus.field(I, demo.X)

    def play_with_view(itype):
        arr = p.as_bytes().view(itype)
        arr[:] = 42
        assert np.all(p.as_bytes().view(itype) == 42)
        for i in I:
            assert np.all(p[i].as_bytes().view(itype) == 42)

    play_with_view(int)
    play_with_view(np.int32)
    arr = p.as_bytes().view(float)
    arr[:] = np.arange(30)
    for i in range(I.size()):
        pp = p[i].as_bytes().view(float)
        assert np.allclose(pp, 3 * i + np.arange(3))
    p[0].as_bytes().view(float)[:] = np.array([11, 22, 33])
    assert np.allclose(p.as_bytes().view(float)[:3], [11, 22, 33])


# def test_fieldX():
#     I = icus.ISet(2)
#     # instantiate
#     p = icus.field(I, demo.X)
#     # get size
#     assert p.size() == 2
#     as_float = lambda obj: icus.reinterpret(obj, float)
#     # setitem
#     as_float(p[0])[:] = np.array([1, 2, 3])
#     as_float(p[1])[:] = np.array([11, 22, 33])
#     # getitem
#     assert np.allclose(as_float(p[0]), np.array([1, 2, 3]))
#     assert np.allclose(as_float(p[1]), np.array([11, 22, 33]))
#     # array view
#     arr = as_float(p)
#     assert arr.shape == (6,)
#     # read view
#     assert np.allclose(arr, [1, 2, 3, 11, 22, 33])
#     # write view
#     arr[:] *= -1
#     # array view and field share memory
#     assert np.allclose(arr, [-1, -2, -3, -11, -22, -33])
#     assert np.allclose(as_float(p), [-1, -2, -3, -11, -22, -33])


def test_fieldDouble():
    I = icus.ISet(10)
    # instantiate
    p = icus.FieldDouble(I)
    # get size
    assert p.size() == 10
    # setitem
    for i in range(10):
        p[i] = i**2
    # getitem
    for i in range(10):
        assert p[i] == i**2
    # array view
    arr = p.as_array()
    # read view
    assert np.allclose(arr, np.arange(10) ** 2)
    # write view
    arr[:] = -np.arange(10) ** 2
    # array view and field share memory
    for i in range(10):
        assert p[i] == -(i**2)
    # printable
    assert (
        str(p)
        == "Field carried by an iset whose size is 10\nValues : { 0, -1, -4, -9, -16, -25, -36, -49, -64, -81}"
    )


def test_field_view_state_X():
    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, demo.X)
    q = p.view(J)
    assert q.size() == 3
    setX(q[0], 1, (2, 3))
    setX(q[1], 11, (22, 33))
    setX(q[2], 111, (222, 333))
    assert p[2].x == 1
    assert np.allclose(p[2].y, [2, 3])
    assert p[7].x == 11
    assert np.allclose(p[7].y, [22, 33])
    assert p[3].x == 111
    assert np.allclose(p[3].y, [222, 333])

    with pytest.raises(AttributeError):
        q.as_array()


def test_field_view_native_float():
    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, float)
    q = p.view(J)
    assert q.size() == 3
    q[0] = 1
    q[1] = 2
    q[2] = 3
    assert p[2] == 1
    assert p[7] == 2
    assert p[3] == 3

    with pytest.raises(AttributeError):
        q.as_array()


def test_field_extract_state_X():
    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, demo.X)
    # use as_array view to use the numpy constructor from tuples
    a = p.as_array()
    a[2] = (1, (2, 3))
    a[7] = (11, (22, 33))
    a[3] = (111, (222, 333))
    q = p.extract(J)
    assert q.size() == 3
    assert np.allclose(q.as_array().x, [1, 11, 111])
    assert np.allclose(q.as_array().y, [[2, 3], [22, 33], [222, 333]])


def test_field_extract_native_float():
    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, float)
    p[2] = 1
    p[7] = 2
    p[3] = 3
    q = p.extract(J)
    assert q.size() == 3
    assert q[0] == 1
    assert q[1] == 2
    assert q[2] == 3
    assert np.allclose(q.as_array(), [1, 2, 3])


def test_have_same_support():
    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    A = icus.ISet(10)
    assert I != A
    assert not icus.have_same_support(I, A)
    pi = icus.field(I)
    pibis = icus.field(I)
    pj = pi.view(J)
    pa = icus.field(A)
    #
    assert pi.support() == I
    assert pibis.support() == I
    assert icus.have_same_support(pibis.support(), I)
    assert pa.support() != I
    #
    assert pi.support() == pibis.support()
    assert pi.support() != pa.support()
    #
    assert pj.support() != pi.support()


def test_have_same_support_different_types():
    I = icus.ISet(10)
    A = icus.ISet(10)
    assert I != A
    assert not icus.have_same_support(I, A)
    pi = icus.field(I)
    pibis = icus.field(I, demo.X)
    pa = icus.field(A, demo.X)
    #
    assert I == pi.support()
    assert I == pibis.support()
    assert I != pa.support()
    #
    assert pi.support() == pibis.support()
    assert pi.support() != pa.support()


# @pytest.mark.skip()
def test_flexible_indexing():
    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, float)

    p[:] = range(p.size())
    assert np.allclose(p.as_array(), range(p.size()))
    p[:] = np.arange(p.size())
    # assert np.allclose(p.as_array(), range(p.size()))
    # p.as_array()[:] = range(p.size())
    # assert np.allclose(p.as_array(), range(p.size()))
    # q = icus.field(J, float)
    # q.as_array()[:] = [-2, -7, -3]

    # assert list(p[J]) == [2, 7, 3]

    # p[J] = -1
    # assert list(p[J]) == [-1, -1, -1]
    # assert [p[2], p[7], p[3]] == [-1, -1, -1]

    # p[J] = q
    # assert list(p[J]) == [-2, -7, -3]
    # assert [p[2], p[7], p[3]] == [-2, -7, -3]

    # p[J] = [20, 70, 30]
    # assert list(p[J]) == [20, 70, 30]
    # assert [p[2], p[7], p[3]] == [20, 70, 30]


def test_flexible_indexing_ellipsis():
    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, float)
    p[:] = range(p.size())
    q = icus.field(J, float)
    q[:] = [-2, -7, -3]

    assert list(p[J]) == [2, 7, 3]

    # deprecated: just use p
    # pp = p[...]
    assert all(v == vv for v, vv in zip(p, range(p.size())))
    p[0] = -1
    assert p[0] == -1

    p[...] = -1
    assert all(v == -1 for v in p)

    p[J] = q
    assert list(p[J]) == [-2, -7, -3]

    p[...] = list(range(10))
    assert all(v == i for i, v in enumerate(p))


def test_flexible_indexing_state():
    new_X = lambda a, b, c: (
        x := demo.X(),
        setattr(x, "x", a),
        x.y.__setitem__(..., (b, c)),
    )[0]
    v = lambda n: new_X(100 * n + 1, 100 * n + 2, 100 * n + 3)
    list_to_arr = lambda values: np.concatenate(
        [x.as_bytes().view(x.numpy_dtype) for x in values]
    )
    same_X = lambda a, b: (a.x == b.x) and np.all(a.y == b.y)
    same_Xs = lambda A, B: all(same_X(a, b) for a, b in zip(A, B))

    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, demo.X)

    list_value = [v(i) for i in range(p.size())]
    arr_value = list_to_arr(list_value)
    p[:] = list_value
    assert np.all(p.as_array() == arr_value)
    # p[:] = arr_value
    # assert np.all(p.as_array() == arr_value)
    p.as_array()[:] = arr_value
    assert np.all(p.as_array() == arr_value)

    q = icus.field(J, demo.X)
    q[:] = [v(-2), v(-7), v(-3)]

    assert same_Xs(p[J], [v(2), v(7), v(3)])

    p[J] = v(-1)
    assert same_Xs(p[J], [v(-1), v(-1), v(-1)])
    assert same_Xs([p[2], p[7], p[3]], [v(-1), v(-1), v(-1)])

    p[J] = q
    assert same_Xs(p[J], [v(-2), v(-7), v(-3)])
    assert same_Xs([p[2], p[7], p[3]], [v(-2), v(-7), v(-3)])

    p[J] = [v(20), v(70), v(30)]
    assert same_Xs(p[J], [v(20), v(70), v(30)])
    assert same_Xs([p[2], p[7], p[3]], [v(20), v(70), v(30)])


def test_deepcopy_native():
    import copy

    I = icus.ISet(10)
    J = I.extract([2, 7, 3])
    p = icus.field(I, float)
    p.as_array()[:] = range(p.size())
    pc = copy.deepcopy(p)
    pc[0] = 42
    assert p[0] == 0


def test_deepcopy_enum():
    import copy

    I = icus.ISet(10)
    p = icus.field(I, demo.MyEnum)
    p[I] = demo.MyEnum.A
    pc = copy.deepcopy(p)
    pc[0] = demo.MyEnum.B
    assert p[0] == demo.MyEnum.A


def test_deepcopy_struct():
    import copy

    I = icus.ISet(10)
    p = icus.field(I, demo.X)
    # use as_array view to use the numpy constructor from tuples
    a = p.as_array()
    a[0] = (1, (2, 3))
    pc = copy.deepcopy(p)
    a = pc.as_array()
    a[0] = (10, (20, 30))
    assert p[0].x == 1
    assert np.allclose(p[0].y, [2, 3])


def test_bind_structured_field():
    n = 10
    I = icus.ISet(n)
    # instantiate
    p = icus.field(I, demo.X)
    a = p.as_array()
    assert a.x.shape == (n,)
    assert a.y.shape == (n, 2)
    a.x[:] = 1
    a.y[:, 0] = 3.14
    a.y[:, 1] = np.arange(n)
    for i in range(n):
        assert np.all(p[i].y == np.array([3.14, i]))
    for i in range(n):
        assert np.all(p[i].y == np.array([3.14, i]))
    a = p.as_array()
    assert np.allclose(a.x, 1)
    assert np.allclose(p[0].y, [3.14, 0])


def test_empty_field():
    I = icus.ISet()
    p = icus.field(I)
    assert isinstance(p, icus.Field)
    assert isinstance(p, icus.FieldDouble)
    for v in p:
        assert False
    p[...] = 0.0
    # p[icus.ISet()] = 2.0 # not allowed
