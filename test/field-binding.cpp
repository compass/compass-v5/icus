#include <icus/field-binding.h>

int main() {
    using namespace icus::binding;
    auto a = Attribute_info<double>("x", {2});
    static_assert(is_attribute_info<decltype(a)>);
}
