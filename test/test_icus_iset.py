import pytest
import numpy as np
from icus import ISet, intersect, join, lowest_common_ancestor


def test_iset():
    I = ISet(10)
    assert all(x == y for x, y in zip(I, range(I.size())))
    with pytest.raises(RuntimeError):
        I.parent()  # no parent yet
    subset_indices = [0, 3, 7, 5]
    J = I.extract(subset_indices)
    assert I == J.parent()
    # FIXME: J.parent()==I will fail: is it important to us ?
    assert all(i == j for i, j in zip(J.mapping(), subset_indices))
    L = intersect(J, I, J)
    Lprime = intersect(J, I, I)
    assert L == J
    assert J == L.parent()
    assert Lprime == J
    assert I == Lprime.parent()
    M = I.extract(np.arange(4).tolist())
    N = intersect(M, J, I)
    assert np.all(list(N.mapping()) == [0, 3])
    K = ISet(20)
    I.set_base(K, list(range(0, 20, 2)))
    assert all(i == j for i, j in zip(I.mapping(), range(0, 20, 2)))
    with pytest.raises(RuntimeError):
        I.set_base(K, list(range(10)))  # base already set

    U = join(I, J)
    V = join(I, N)
    assert I == V

    P = I.substract(M)
    assert list(P.mapping()) == [4, 5, 6, 7, 8, 9]
    assert P.size() == len(P)
    assert P


def test_conversions():
    I = ISet(4)
    assert len(list(I)) == 4
    assert all([i == j for i, j in zip(I, list(I))])
    assert len(np.array(I)) == 4
    assert np.all(np.array(I) == np.arange(len(I)))
    I = ISet()
    assert len(list(I)) == 0
    assert len(np.array(I)) == 0


def test_inclusions():
    A = ISet(2)
    B = ISet(2)
    rA = A.rebase(A)
    assert A.is_included_in(rA)
    assert rA.is_included_in(A)
    assert not B.is_included_in(A)
    assert not A.is_included_in(B)
    assert lowest_common_ancestor(A, B) is None
    assert lowest_common_ancestor(A, rA) == A
    assert lowest_common_ancestor(rA, A) == A


def test_mapping_constness():
    I = ISet(30)
    original_mapping = list(range(0, 15))
    Pm = I.extract(original_mapping)
    mapping = Pm.mapping_as_array()
    assert np.all(mapping == original_mapping)
    with pytest.raises(ValueError):
        mapping[0] = 10


def test_empty_iset():
    I = ISet(50)
    assert I.mapping() is None
    J = I.extract(list(range(0)))
    assert J.empty()
    assert J.size() == 0
    assert not J.has_base()
    assert J.mapping() is None
    with pytest.raises(RuntimeError):
        J.set_base(I, list(range(10)))  # empty iset cannot have a base
    K = I.extract([])
    assert K.empty()

    L = I.extract([2, 3])
    M = I.extract([40, 20, 32])
    N = intersect(L, M, I)
    assert N.empty()
    assert not N
    # J is empty
    assert intersect(I, J, J).empty()

    O = join(J, K)
    assert O.empty()
    assert O == ISet()

    assert join(I, O) == I


def test_extractions():
    I = ISet(100)
    J = I.extract(np.arange(0, 100, 10))
    assert J.size() == 10
    assert all(i == j for i, j in zip(J.mapping(), np.arange(0, 100, 10)))
    K = J.extract(np.arange(10) % 2 == 0)
    assert K.size() == 5
    assert all(i == j for i, j in zip(K.mapping(), np.arange(0, 10, 2)))
    Kr = K.rebase(I)
    assert all(i == j for i, j in zip(Kr.mapping(), np.arange(0, 100, 20)))
    L = K.extract([False, True, False, False, True])
    assert L.size() == 2
    assert tuple(L.mapping()) == (1, 4)


if __name__ == "__main__":
    test_iset()
    test_empty_iset()
    test_extractions()
