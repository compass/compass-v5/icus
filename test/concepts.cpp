#include <catch2/catch_test_macros.hpp>

#include "icus/ISet.h"
#include "icus/Slice.h"

TEST_CASE("Concepts") {
    using namespace icus;

    CHECK(std::forward_iterator<icus::Counter<index_type>::iterator>);
    CHECK(std::forward_iterator<icus::Slice<index_type>::iterator>);
    CHECK(std::ranges::range<icus::Slice<index_type>>);
    CHECK(std::forward_iterator<icus::ISet::iterator>);
    CHECK(std::ranges::range<icus::ISet>);
    CHECK(std::forward_iterator<icus::ISet::Chain::iterator>);
    CHECK(std::ranges::range<icus::ISet::Chain>);
}
