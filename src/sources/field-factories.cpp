#include <icus/field-factories.h>
#include <nanobind/nanobind.h>

#include <map>

namespace nb = nanobind;

namespace icus {

std::map<std::string, field_factory> string_factories;
// FIXME: try to merge with PyTypeObject* factories
struct ICUS_FIELD_BINDING_NO_EXPORT factory_from_np_dtype {
    nb::handle dtype;
    field_factory factory;
};
std::vector<factory_from_np_dtype> numpy_factories;
std::map<PyTypeObject*, field_factory> factories;

ICUS_FIELD_BINDING_EXPORT void register_factory_from_code(
    const std::string& code, field_factory&& f) {
#ifndef NDEBUG
    if (string_factories.contains(code)) {
        std::cerr << "WARNING: Overwritting factory with code: " << code
                  << std::endl;
    }
#endif
    string_factories[code] = std::move(f);
};

ICUS_FIELD_BINDING_EXPORT void register_factory_from_np_dtype(
    const std::string& np_dtype, field_factory&& f) {
    nb::module_ np = nb::module_::import_("numpy");
    auto h = np.attr(np_dtype.c_str());
#ifndef NDEBUG
    for (auto&& info : numpy_factories) {
        if (info.dtype.equal(h)) {
            std::cerr << "WARNING: Overwritting factory with numpy dtype: "
                      << np_dtype.c_str() << std::endl;
            break;
        }
    }
#endif
    numpy_factories.emplace_back(h, std::move(f));
};

ICUS_FIELD_BINDING_EXPORT void register_factory(PyTypeObject* tp,
                                                field_factory&& f) {
    factories[tp] = std::move(f);
};

template <typename Map, typename Key>
inline nb::object _build_field(const Map& map, const Key& key, const ISet& I,
                               const char* error_message) {
    auto it = map.find(key);
    if (it == map.end()) {
        throw nb::type_error(error_message);
    }
    return it->second(I);
}

ICUS_FIELD_BINDING_EXPORT nb::object build_field(const ISet& I,
                                                 nb::handle dtype) {
    for (auto&& info : numpy_factories) {
        if (dtype.equal(info.dtype)) {
            return nb::cast(info.factory(I));
        }
    }
    try {
        auto type_object = nb::cast<nb::type_object>(dtype);
        return _build_field(factories, (PyTypeObject*)dtype.ptr(), I,
                            "Field factory: unregistered type");
    } catch (nb::cast_error&) {
    }
    try {
        auto code = nb::cast<nb::str>(dtype);
        return _build_field(string_factories, std::string{code.c_str()}, I,
                            "Field factory: unregistered code");
    } catch (nb::cast_error&) {
    }
    std::string error_message{"Could not make a field out of provided type: "};
    error_message.append(nb::repr(dtype).c_str());
    throw nb::type_error(error_message.c_str());
}

}  // namespace icus
