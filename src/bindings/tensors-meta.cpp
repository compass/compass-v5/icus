// binding functions relying on meta-programming
// this works and binds a lot of tensor types

#include <compass_cxx_utils/meta.h>

#include <iostream>

template <typename IS, typename OS>
inline OS& dump(OS& os) {
    using namespace compass::meta::index_sequence;
    if constexpr (len_v<IS> != 0) {
        if constexpr (len_v<IS> == 1) {
            os << last_v<IS>;
        } else {
            dump<drop_t<1, IS>>(os) << " " << last_v<IS>;
        }
    }
    return os;
}

template <std::size_t i, typename IS>
bool _cmp(nb::tuple& t) {
    using namespace compass::meta::index_sequence;
    assert(nb::len(t) == len_v<IS>);
    if constexpr (i >= len_v<IS>) {
        return true;
    } else {
        return index_v<i, IS> == nb::cast<std::size_t>(t[i]) &&
               _cmp<i + 1, IS>(t);
    }
}

template <typename IS>
bool cmp(nb::tuple& t) {
    using namespace compass::meta::index_sequence;
    if (nb::len(t) != len_v<IS>) return false;
    return _cmp<0, IS>(t);
}

template <typename shape_iterator, typename shape, typename T>
auto _select_tensor_factory(nb::tuple& shape_tuple) {
    if (cmp<shape>(shape_tuple)) {
        return nb::type<helper::tensor_t<shape, T>>();
    }
    if constexpr (shape_iterator::template stop<shape>) {
        throw nb::type_error("unbound tensor type");
    }
    return _select_tensor_factory<
        shape_iterator, typename shape_iterator::template next<shape>, T>(
        shape_tuple);
}

template <std::size_t i, typename IS>
void _append_values(std::string& name) {
    using namespace compass::meta::index_sequence;
    if constexpr (i >= len_v<IS>) {
        return;
    } else {
        if constexpr (0 < i && i < len_v<IS>) name += "x";
        name += std::to_string(index_v<i, IS>);
        _append_values<i + 1, IS>(name);
    }
}

template <typename IS>
void append_values(std::string& name) {
    _append_values<0, IS>(name);
}

template <typename IS, typename T>
std::string tensor_name() {
    std::string name{"Tensor_"};
    name += type_repr<T>;
    name += "_";
    append_values<IS>(name);
    return name;
}

template <typename shape_iterator, typename shape, typename T>
void _bind_tensors(nanobind::module_& m) {
    bind_tensor<shape, T>(m, tensor_name<shape, T>());
    if constexpr (shape_iterator::template stop<shape>) {
        return;
    }
    _bind_tensors<shape_iterator, typename shape_iterator::template next<shape>,
                  T>(m);
}

void bind_tensors(nanobind::module_& m) {
    namespace meta = compass::meta::index_sequence;

    using shape_iterator =
        meta::Iterator<std::index_sequence<1, 1>, std::index_sequence<4, 4>>;
    using start = std::index_sequence<1, 1>;
    _bind_tensors<shape_iterator, start, int>(m);
    _bind_tensors<shape_iterator, start, float>(m);
    _bind_tensors<shape_iterator, start, double>(m);

    m.def(
        "tensor",
        [](nb::tuple shape, nb::str dtype) -> nb::handle {
            // FIXME: add PyFloat_Type...
            // FIXME: make this more flexible
            char dtype_code = nb::len(dtype) > 0 ? dtype.c_str()[0] : 'd';
            switch (dtype_code) {
                case 'i':
                    return _select_tensor_factory<shape_iterator, start, int>(
                        shape);
                case 'f':
                    return _select_tensor_factory<shape_iterator, start, float>(
                        shape);
                case 'd':
                    return _select_tensor_factory<shape_iterator, start,
                                                  double>(shape);
                default:
                    throw nb::type_error("unknown tensor value type");
            }
        },
        nb::arg("shape").none(false), nb::arg("dtype") = "");
}
