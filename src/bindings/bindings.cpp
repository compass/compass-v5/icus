#include <nanobind/nanobind.h>

namespace nb = nanobind;

void bind_iset_wrappers(nb::module_&);
void bind_connectivity_wrappers(nb::module_&);
void bind_field_wrappers(nb::module_&);
void bind_debug_utilities(nb::module_&);

NB_MODULE(bindings, module) {
    bind_iset_wrappers(module);
    bind_connectivity_wrappers(module);
    bind_field_wrappers(module);
    bind_debug_utilities(module);
}
