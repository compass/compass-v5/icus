// binding functions relying on explicit declaration
// alternative to meta-programming declarations (cf. tensors.meta.cpp)
// WARING: we bind less tensor types

template <std::size_t... nk>
using IS = std::index_sequence<nk...>;

template <typename T>
auto _select_tensor_factory(nb::tuple& shape) -> nb::handle {
    if (nb::len(shape) == 1) {
        const int nr = nb::cast<int>(shape[0]);
        if (nr == 1) return nb::type<helper::tensor_t<IS<1>, T>>();
        if (nr == 2) return nb::type<helper::tensor_t<IS<2>, T>>();
        if (nr == 3) return nb::type<helper::tensor_t<IS<3>, T>>();
    }
    if (nb::len(shape) != 2) throw nb::type_error("unbound tensor type");
    const int nr = nb::cast<int>(shape[0]);
    const int nc = nb::cast<int>(shape[1]);
    if (nr == 1) {
        if (nc == 1) return nb::type<helper::tensor_t<IS<1, 1>, T>>();
        // if(nc==2) return nb::type<helper::tensor_t<IS<1,2>, T>>();
        // if(nc==3) return nb::type<helper::tensor_t<IS<1,3>, T>>();
    }
    if (nr == 2) {
        // if(nc==1) return nb::type<helper::tensor_t<IS<2,1>, T>>();
        if (nc == 2) return nb::type<helper::tensor_t<IS<2, 2>, T>>();
        // if(nc==3) return nb::type<helper::tensor_t<IS<2,3>, T>>();
    }
    if (nr == 3) {
        if (nc == 1) return nb::type<helper::tensor_t<IS<3, 1>, T>>();
        if (nc == 2) return nb::type<helper::tensor_t<IS<3, 2>, T>>();
        if (nc == 3) return nb::type<helper::tensor_t<IS<3, 3>, T>>();
    }
    throw nb::type_error("unbound tensor type");
}

template <typename T>
void _bind_tensors(nanobind::module_& m) {
    std::string basename{"Tensor_"};
    basename += type_repr<T>;
    bind_tensor<IS<1>, T>(m, basename + "_1");
    bind_tensor<IS<2>, T>(m, basename + "_2");
    bind_tensor<IS<3>, T>(m, basename + "_3");
    bind_tensor<IS<1, 1>, T>(m, basename + "_1x1");
    // bind_tensor<IS<1,2>, T>(m, basename + "_1x2");
    // bind_tensor<IS<1,3>, T>(m, basename + "_1x3");
    // bind_tensor<IS<2,1>, T>(m, basename + "_2x1");
    bind_tensor<IS<2, 2>, T>(m, basename + "_2x2");
    // bind_tensor<IS<2,3>, T>(m, basename + "_2x3");
    bind_tensor<IS<3, 1>, T>(m, basename + "_3x1");
    bind_tensor<IS<3, 2>, T>(m, basename + "_3x2");
    bind_tensor<IS<3, 3>, T>(m, basename + "_3x3");
}

void bind_tensors(nanobind::module_& m) {
    _bind_tensors<int>(m);
    _bind_tensors<float>(m);
    _bind_tensors<double>(m);

    m.def(
        "tensor",
        [](nb::tuple shape, nb::str dtype) -> nb::handle {
            // FIXME: add PyFloat_Type...
            // FIXME: make this more flexible
            // FIXME: should we restrict ourselves to dlpack code?
            char dtype_code = nb::len(dtype) > 0 ? dtype.c_str()[0] : 'd';
            switch (dtype_code) {
                case 'i':
                    return _select_tensor_factory<int>(shape);
                case 'f':
                    return _select_tensor_factory<float>(shape);
                case 'd':
                    return _select_tensor_factory<double>(shape);
                default:
                    throw nb::type_error("unknown tensor value type");
            }
        },
        nb::arg("shape").none(false), nb::arg("dtype") = "");
}
