#include <icus/field-binding.h>
#include <nanobind/nanobind.h>

namespace nb = nanobind;

namespace helper {

template <typename IS, typename T>
struct tensor;

template <typename T, std::size_t... nk>
struct tensor<std::index_sequence<nk...>, T> {
    using type = icus::Tensor<T, nk...>;
};

template <typename IS, typename T>
using tensor_t = typename tensor<IS, T>::type;

template <typename T, std::size_t... nk>
inline nb::tuple shape_tuple(const icus::Tensor<T, nk...>&) {
    return nb::make_tuple(nk...);
}

}  // namespace helper

template <typename T>
inline constexpr const char* type_repr = nullptr;

template <>
inline constexpr const char* type_repr<int> = "int";
template <>
inline constexpr const char* type_repr<float> = "float";
template <>
inline constexpr const char* type_repr<double> = "double";

template <typename IS, typename value_type>
auto bind_tensor(nanobind::module_& m, std::string name) {
    using T = helper::tensor_t<IS, value_type>;
    nb::class_<T> pycls{m, name.c_str()};
    pycls.def(nb::init());
    // expose a Tensor as a numpy array
    pycls.def(
        "as_array",
        [](nb::handle self) {
            const auto shape = T::shape();
            return nb::ndarray<value_type, nb::numpy>{
                nb::cast<T&>(self).data(), T::ndim(), shape.data(), self};
        },
        nb::keep_alive<0, 1>());
    pycls.def("shape", [](const T& self) { return helper::shape_tuple(self); });

    name = "Field_of_" + name;
    icus::bind_field(m, pycls, name.c_str(), icus::pack<T>());
}

#ifdef ICUS_WITH_METAPROGRAMING
#include "tensors-meta.cpp"
#else  // explicit tensor binding
#include "tensors-explicit.cpp"
#endif
