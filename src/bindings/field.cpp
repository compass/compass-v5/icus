#include <icus/field-binding.h>
#include <nanobind/nanobind.h>

namespace nb = nanobind;

void bind_tensors(nanobind::module_&);

void bind_field_wrappers(nanobind::module_& m) {
    // the base class of all fields
    nb::class_<icus::Field_base>(m, "Field");

    // FIXME: try to use PyType_Object pointers for numpy types

    // a few words about managing the fact that
    // python int are C long which is a platform dependent type
    //
    // TLDR: if you want to label things with integers, think about using
    //       icus::ISet objects, it is their actual purpose
    //       if you still want to use a field of labels
    //       use icus::Labels on the C++ side and icus.Labels on the python side
    //       (you can also use the icus.labels factory function)
    //
    // python int is C long (with underlying PyLong_Type in C-python)
    // Numpy inherits the default size of an integer from the C compiler's long
    // which, for windows, is 32 bits even on a 64 bit system.
    // This means that np.dtype("int"), np.dtype(int), np.int_, np.dtype("l") or
    // np.dtype("long") design the same type (underlying PyLong_Type) but this
    // type varies accross platforms (it can be either np.int32 or np.int64 or
    // even long) whereas we always have np.dtype("i")==np.int32
    //
    // (NB: there is no np.long dtype!)
    //
    // In the following I is supposed to be an icus.ISet instance.
    // We follow numpy choices with some caveats concerning
    // the C++ counterpart of fields:
    // python icus.field(I, "int"), icus.field(I, int), icus.field(I, np.int_)
    // and icus.field(I, "i") will map to icus::Field<std::int32_t> when
    // sizeof(long) (i.e. python integers) is 4 and to icus::Field<std::int64_t>
    // when sizeof(long) is 8.
    // The consequence is that you cannot rely on this types when passing
    // arguments to C++ functions unless you handle type casting yourself
    // (but everything should work well if you stay on the python side).
    // You cannot even rely on C long type because on windows platforms
    // long is not the same as std::int32_t though sizeof(long) is 4. and
    // On *nix platforms things run smoorther and icus::Field<long>
    // is the same as icus::Field<std::int64_t>.
    //
    // When creating fields you can be explicit (which is always better than
    // implicit...) using on the python side icus.field(I, np.int32),
    // icus.field(I, "i") or icus.FieldInt32 which will map to a C++
    // icus::Field<int> (we check it to be the same as
    // icus::Field<std::int32_t> upon compilation) or using on the python side
    // icus.field(I, np.int64), icus.field(I, "int64") or icus.FieldInt64 which
    // will map to a C++ icus::Field<std::int64_t>.
    //
    // Though we expose a a python class.FieldLong which can always be mapped
    // to a C++ icus::Field<long> (but which is not always a
    // icus::Field<std::int32_t> or a icus::Field<std::int64_t>)
    // it can be safer NOT to use it.
    //
    // To disambiguate between python int and C int we opted for the
    // explicit naming of classes (i.e. icus.FieldInt32 or icus.FieldInt64).
    // For conveniency we also introduced python icus.Labels correponding
    // to the C++ icus::Labels class.
    // A python icus.Labels can always be mapped to a icus::Labels C++ type
    // (both are icus Fields of integers and behind the hood they are just
    // icus::Field<int> but you'd better not rely on this asumption and just use
    // them as they are).
    //
    // cf. examples in test/test_icus_field_int.py
    //
    // we rely on the following assertion (that C int are 32 bytes) to alias
    // Labels to FieldInt32 upon import in icus.__init__.py
    static_assert(std::is_same_v<int, std::int32_t>);
    icus::bind_native_field<std::int16_t>(m, "FieldInt16", {"int16"},
                                          {"int16"});

    nb::module_ np = nb::module_::import_("numpy");

    if constexpr (sizeof(long) == 4) {
        assert(!(std::is_same_v<long, std::int32_t>));
        assert(!(std::is_same_v<long, std::int64_t>));
        m.attr("long_is_int64") = false;
        if (np.attr("int_").is(np.attr("int64"))) {
            icus::bind_native_field<int>(m, "FieldInt32",
                                         {"i", "int32", "int", "l", "long"},
                                         {"int32"}, PyLong_Type);
            icus::bind_native_field<std::int64_t>(m, "FieldInt64", {"int64"},
                                                  {"int64", "int_"});
        } else {
            assert(np.attr("int_").is(np.attr("int32")));
            icus::bind_native_field<int>(m, "FieldInt32",
                                         {"i", "int32", "int", "l", "long"},
                                         {"int32", "int_"}, PyLong_Type);
            icus::bind_native_field<std::int64_t>(m, "FieldInt64", {"int64"},
                                                  {"int64"});
        }
        icus::bind_native_field<long>(m, "FieldLong");
    } else if constexpr (sizeof(long) == 8) {
        // FieldLong will be aliased to FieldInt64 in icus.__init__.py
        assert((std::is_same_v<long, std::int64_t>));
        m.attr("long_is_int64") = true;
        if (np.attr("int_").is(np.attr("int64"))) {
            icus::bind_native_field<int>(m, "FieldInt32", {"i", "int32"},
                                         {"int32"});
            icus::bind_native_field<std::int64_t>(
                m, "FieldInt64", {"int64", "int", "l", "long"},
                {"int64", "int_"}, PyLong_Type);
        } else {
            assert(np.attr("int_").is(np.attr("int32")));
            icus::bind_native_field<int>(m, "FieldInt32", {"i", "int32"},
                                         {"int32", "int_"});
            icus::bind_native_field<std::int64_t>(m, "FieldInt64",
                                                  {"int64", "int", "l", "long"},
                                                  {"int64"}, PyLong_Type);
        }
    } else {
        throw std::runtime_error(
            "could not manage sizeof(long) in icus Fields binding");
    }

    icus::bind_native_field<float>(m, "FieldFloat", {"f", "float"},
                                   {"float32"});
    icus::bind_native_field<double>(m, "FieldDouble", {"d", "double"},
                                    {"float64"}, PyFloat_Type);
    icus::bind_native_field<icus::byte_type>(m, "FieldByte", {"byte"});
    icus::bind_native_field<bool>(m, "FieldBool", {"b", "bool", "boolean"},
                                  {"bool_"}, PyBool_Type);

    bind_tensors(m);

    // FIXME: pass PyFloat_type not "d" as default dtype
    m.def("field", &icus::build_field, nb::arg("support").none(false),
          nb::arg("dtype") = nb::str{"d"});

    m.def("labels", [](const icus::ISet& I) { return icus::Labels{I}; });
}
