#include <icus/ISet.h>
#include <nanobind/make_iterator.h>
#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>
#include <nanobind/stl/vector.h>

#include <concepts>

namespace nb = nanobind;

template <std::integral Index>
auto iset_extraction(const icus::ISet& self,
                     nb::ndarray<Index, nb::ndim<1>, nb::c_contig> indices) {
    return self.extract(
        std::span<Index>{indices.data(), indices.data() + indices.size()});
}

namespace helper {

// we don't use nb::rv_policy::reference_internal to bind this function
// because in the first case (slice) it would lead to
// override an existing owner which raises an error
// keeping a reference to the ISet, to keep the mapping allocated,
// is enough in the vector case
nb::object mapping_as_array(const icus::ISet& self) {
    if (!self.has_base()) return nb::none();
    auto a = self.mapping().visit([&](auto&& indices) {
        using namespace icus;
        assert(indices.size() == self.size());
        if constexpr (std::is_same_v<std::decay_t<decltype(indices)>,
                                     ISet::mapping_t::slice>) {
            index_type* data = new index_type[indices.size()];
            std::ranges::copy(indices, data);
            nb::capsule owner(
                data, [](void* p) noexcept { delete[] (index_type*)p; });
            return nb::ndarray<nb::numpy, const index_type>(data, {self.size()},
                                                            owner);
        } else {
            static_assert(std::is_same_v<std::decay_t<decltype(indices)>,
                                         icus::ISet::mapping_t::vector>);
            ISet* data = new ISet{self};
            nb::capsule owner{data, [](void* p) noexcept { delete (ISet*)p; }};
            return nb::ndarray<nb::numpy, const index_type>(
                indices.data(), {self.size()}, owner);
        }
    });
    return nb::cast(a);
}

}  // namespace helper

namespace {
auto as_vector_of_ids(nb::list l) {
    std::vector<icus::index_type> ids;
    ids.reserve(nb::len(l));
    for (auto&& h : l) {
        ids.push_back(nb::cast<icus::index_type>(h));
    }
    return ids;
}
}  // namespace

void bind_iset_wrappers(nb::module_& module) {
    static_assert(std::forward_iterator<nb::detail::fast_iterator>);
    static_assert(std::ranges::range<nb::list>);

    using namespace icus;

    auto cls = nb::class_<ISet>(module, "ISet", "An Indexed Set");

    auto mapcls =
        nb::class_<ISet::mapping_t>(cls, "Mapping", "An ISet mapping");
    mapcls.def(
        "__iter__",
        [](const ISet::mapping_t& self) {
            return nb::make_iterator(nb::type<ISet::mapping_t>(),
                                     "mapping_iterator", self);
        },
        nb::keep_alive<0, 1>());
    mapcls.def("__len__", &ISet::mapping_t::size);
    mapcls.def("__getitem__", &ISet::mapping_t::operator[]);

    cls.def(nb::init<>());
    cls.def(nb::init<int>());
    cls.def(nb::init<const ISet&>());
    cls.def(
        "__iter__",
        [](const ISet& self) {
            return nb::make_iterator(nb::type<ISet>(), "index_iterator", self);
        },
        nb::keep_alive<0, 1>());
    // a convencience method to automatically fill np.array from ISet
    cls.def("__getitem__",
            [](const ISet&, const index_type i) -> index_type { return i; });
    // FIXME: use nanobind operators declaration
    cls.def("__eq__",
            [](const ISet& self, const ISet& other) { return self == other; });
    module.def("are_same_indexed_set",
               [](const ISet& I1, const ISet& I2) { return I1 == I2; });
    cls.def("__ne__",
            [](const ISet& self, const ISet& other) { return self != other; });
    cls.def("size", &ISet::size);
    cls.def("__len__", &ISet::size);
    cls.def("empty", &ISet::empty);
    cls.def("__bool__", [](const ISet& self) { return !self.empty(); });
    cls.def("extract", [](const ISet& self, index_type i, index_type j) {
        return self.extract(i, j);
    });
    cls.def("slice", [](const ISet& self, index_type i, index_type j) {
        return self.extract(i, j);
    });
    cls.def("extract", [](const ISet& self, nb::list l) {
        if (nb::len(l) == 0) return ISet{};
        if ((PyTypeObject*)l[0].type().ptr() == &PyBool_Type) {
            // consider the list as a mask
            if (self.size() != len(l)) {
                throw nb::value_error("mask and ISet must have the same size");
            }
            std::vector<index_type> indices;
            index_type i = 0;
            for (auto&& h : l) {
#ifndef NDEBUG
                if ((PyTypeObject*)h.type().ptr() != &PyBool_Type) {
                    std::cerr << "WARNING: casting " << nb::repr(h).c_str()
                              << " to boolean type" << std::endl;
                }
#endif
                if (nb::cast<bool>(h)) indices.push_back(i);
                ++i;
            }
            return self.extract(std::move(indices));
        }
        return self.extract(as_vector_of_ids(l));
    });
    cls.def("extract", &iset_extraction<index_type>);
    cls.def("extract", &iset_extraction<int>);
    cls.def("extract", [](const ISet& self,
                          nb::ndarray<bool, nb::ndim<1>, nb::c_contig> mask) {
        if (self.size() != mask.size()) {
            throw nb::value_error("mask and ISet must have the same size");
        }
        auto take = mask.view();
        std::size_t n = 0;
        for (auto&& i : self) {
            if (take(i)) ++n;
        }
        std::vector<index_type> indices;
        indices.reserve(n);
        for (auto&& i : self) {
            if (take(i)) indices.push_back(i);
        }
        return self.extract(std::move(indices));
    });
    // cls.def("extract", nb::overload_cast<const index_type&, const
    // index_type&>(
    //                        &ISet::extract, nb::const_));
    cls.def(
        "mapping",
        [](const ISet& self) -> nb::object {
            if (!self.has_base()) return nb::none();
            return nb::cast(self.mapping());
        },
        nb::keep_alive<0, 1>());
    // FIXME: deprecated prefer to use the result of ISet::mapping() as an array
    // we use neither nb::rv_policy::reference_internal nor
    // nb::keep_alive here, cf. implementation above
    cls.def("mapping_as_array", &helper::mapping_as_array);
    cls.def("has_base", &ISet::has_base);
    cls.def("parent", &ISet::parent);
    // nanobind cast from list or numpy array to std vector
    cls.def("set_base", [](ISet& self, const ISet& parent,
                           std::vector<icus::index_type>& indices) {
        self.set_base(parent, indices);
    });
    cls.def("has_ancestor", &ISet::has_ancestor);
    cls.def("is_included_in", &ISet::is_included_in);
    cls.def("rebase", &ISet::rebase);
    cls.def("substract", &ISet::substract);
    module.def("lowest_common_ancestor",
               [](const ISet& I, const ISet& J) -> nb::object {
                   auto lca = lowest_common_ancestor(I, J);
                   if (lca) return nb::cast(*lca);
                   return nb::none();
               });
    module.def("are_disjoint_sets", &are_disjoint_sets);
    module.def("join_no_base", [](nb::list isets) {
        return join_no_base(isets | std::views::transform([](nb::handle h) {
                                return nb::cast<ISet>(h);
                            }));
    });
    module.def("intersect", &intersect);
    module.def("join", &join);
}
