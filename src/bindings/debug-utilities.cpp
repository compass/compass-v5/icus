#include <icus/Field.h>
#include <nanobind/nanobind.h>

#include <iostream>

// for debug purpose only
// cf. the comments in src/bindings/fields.cpp
void bind_debug_utilities(nanobind::module_& m) {
    m.def("_check_labels", [](icus::Labels&) {});
    m.def("_check_field_C_int", [](icus::Field<int>&) {});
    m.def("_check_field_C_long", [](icus::Field<long>&) {});
    m.def("_check_field_int16", [](icus::Field<std::int16_t>&) {});
    m.def("_check_field_int32", [](icus::Field<std::int32_t>&) {});
    m.def("_check_field_int64", [](icus::Field<std::int64_t>&) {});
}
