#include <icus/binding/Connectivity.h>
#include <nanobind/make_iterator.h>
#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>

#include <algorithm>
#include <ranges>
#include <string>
#include <utility>
#include <variant>

namespace nb = nanobind;

using namespace icus::binding;

void warning_targets_size_too_big(const int ts) {
    std::string message{
        "You're trying to create a regular connectivity with targets of size "};
    message += std::to_string(ts);
    message += " whereas maximum target size set at compile time is ";
    message += std::to_string(maximum_target_size);
    nb::print(message.c_str());
}

template <int ts>
Connectivity extract_connectivity(const Connectivity& connectivity,
                                  const icus::ISet& source, nb::handle target) {
    static_assert(ts <= maximum_target_size);
    return std::visit(
        [&](auto&& C) -> Connectivity {
            if (target.is_none()) {
                return C.template extract<ts>(source, C.target());
            } else {
                return C.template extract<ts>(
                    source, nb::cast<const icus::ISet&>(target));
            }
        },
        connectivity);
}

template <int ts = maximum_target_size>
Connectivity extract_connectivity(const Connectivity& C, const int targets_size,
                                  const icus::ISet& source, nb::handle target) {
    static_assert(ts >= 0);
    static_assert(ts <= maximum_target_size);
    if constexpr (ts == 0) {
        warning_targets_size_too_big(targets_size);
        return extract_connectivity<-1>(C, source, target);
    }
    if (targets_size == ts) {
        return extract_connectivity<ts>(C, source, target);
    }
    if constexpr (ts > 0) {
        return extract_connectivity<ts - 1>(C, targets_size, source, target);
    }
}

template <int ts>
Connectivity do_compose_connectivities(const Connectivity& connectivity,
                                       const Connectivity& other) {
    return std::visit(
        [](auto&& C1, auto&& C2) -> Connectivity {
            return C1.template compose<
                icus::ISet, std::decay_t<decltype(C2)>::targets_size_v, ts>(C2);
        },
        connectivity, other);
}

template <int ts = maximum_target_size>
Connectivity compose_connectivities(const Connectivity& connectivity,
                                    const Connectivity& other,
                                    const int targets_size) {
    static_assert(ts >= 0);
    static_assert(ts <= maximum_target_size);
    if constexpr (ts == 0) {
        warning_targets_size_too_big(targets_size);
        return do_compose_connectivities<-1>(connectivity, other);
    }
    if (targets_size == ts) {
        return do_compose_connectivities<ts>(connectivity, other);
    }
    if constexpr (ts > 0) {
        return compose_connectivities<ts - 1>(connectivity, other,
                                              targets_size);
    }
}

template <int ts = maximum_target_size>
    requires(ts >= 0)
Connectivity connectivity_from_array(
    icus::ISet& source, icus::ISet& target,
    nb::ndarray<icus::index_type, nb::c_contig>& targets,
    const bool preserve_targets_order, const bool sorted_input_targets) {
    static_assert(ts >= 0);
    static_assert(ts <= maximum_target_size);
    if (targets.ndim() != 2) {
        throw std::runtime_error("Array must have two dimensions!");
    }
    if (targets.shape(0) != source.size()) {
        throw std::runtime_error(
            "The number of targets must match source size!");
    }
    if constexpr (ts == 0) {
        using std::views::chunk;
        warning_targets_size_too_big(targets.shape(1));
        auto span = std::span{targets.data(), targets.size()};
        return Random_connectivity{
            source, target, span | chunk(targets.shape(1)),
            preserve_targets_order, sorted_input_targets};
    }
    if constexpr (ts > 0) {
        if (targets.shape(1) == ts) {
            return Regular_connectivity<ts>{
                source, target, std::span{targets.data(), targets.size()},
                preserve_targets_order, sorted_input_targets};
        }
        return connectivity_from_array<ts - 1>(source, target, targets,
                                               preserve_targets_order,
                                               sorted_input_targets);
    }
}

template <int ts = maximum_target_size>
    requires(ts >= 0)
Connectivity regular_connectivity_from_list(icus::ISet& source,
                                            icus::ISet& target,
                                            const std::size_t targets_size,
                                            nb::list& targets,
                                            const bool preserve_targets_order,
                                            const bool sorted_input_targets) {
    static_assert(ts >= 0);
    static_assert(ts <= maximum_target_size);
    if constexpr (ts == 0) {
        warning_targets_size_too_big(targets_size);
        return _connectivity_from_list<-1>(source, target, targets,
                                           preserve_targets_order,
                                           sorted_input_targets);
    } else {
        if (targets_size == ts) {
            return _connectivity_from_list<ts>(source, target, targets,
                                               preserve_targets_order,
                                               sorted_input_targets);
        }
        return regular_connectivity_from_list<ts - 1>(
            source, target, targets_size, targets, preserve_targets_order,
            sorted_input_targets);
    }
}

Connectivity connectivity_from_list(icus::ISet& source, icus::ISet& target,
                                    nb::list& neighbours,
                                    const bool preserve_targets_order,
                                    const bool sorted_input_targets) {
    using namespace icus;
    const auto nn = nb::len(neighbours);
    if (nn == 0) return {};
    if (nn != source.size()) {
        throw std::runtime_error(
            "The number of targets must match source size!");
    }
    const auto l0 = nb::len(nb::cast<nb::handle>(neighbours[0]));
    const auto regular = std::all_of(
        neighbours.begin(), neighbours.end(),
        [l0](auto&& h) { return nb::len(nb::cast<nb::handle>(h)) == l0; });
    if (regular) {
        return regular_connectivity_from_list(source, target, l0, neighbours,
                                              preserve_targets_order,
                                              sorted_input_targets);
    }
    return _connectivity_from_list<-1>(source, target, neighbours,
                                       preserve_targets_order,
                                       sorted_input_targets);
}

namespace helper {

struct Targets {
    std::pair<const std::size_t*, const icus::index_type*> pointers = {nullptr,
                                                                       nullptr};
    int ts = 0;  // keep int here as target size may be negative
    std::size_t nb = 0;
    Targets(const Connectivity& c) {
        pointers = std::visit([](auto&& C) { return C._data(); }, c);
        ts = std::visit([](auto&& C) { return C.targets_size_v; }, c);
        assert(ts != 0);
        nb = std::visit([](auto&& C) { return C.source().size(); }, c);
    }
    nb::ndarray<nb::numpy, const icus::index_type> source_targets(
        const std::size_t k) const {
        assert(k < nb);
        if (ts > 0) {
            const auto ncols = static_cast<std::size_t>(ts);
            return {pointers.second + k * ncols, {ncols}, nb::handle()};
        } else {
            auto&& [o, p] = pointers;
            assert(*(o + k) <= *(o + k + 1));
            return {p + *(o + k), {*(o + k + 1) - *(o + k)}, nb::handle()};
        }
    }
    struct iterator {
        const Targets* owner;  // WARNING: use keep_alive in python bindings to
                               // avoid dangling pointers
        std::size_t i = 0;     // current source / row
        iterator& operator++() {
            ++i;
            return *this;
        }
        bool operator==(const std::size_t n) const { return i == n; }
        auto operator*() const {
            assert(owner);
            return owner->source_targets(i);
        }
    };
    iterator begin() const { return {this, 0}; }
    std::size_t end() const { return nb; }
    std::size_t size() const { return nb; }
};

}  // namespace helper

auto bind_connectivities(nb::module_& module) {
    using helper::Targets;

    auto cls = nb::class_<Connectivity>(module, "Connectivity",
                                        "Connectivity between Indexed Sets");

    auto tcls = nb::class_<Targets>(cls, "Targets", "Connectivity targets");
    tcls.def(
        "__iter__",
        [](const Targets& self) {
            return nb::make_iterator(nb::type<Targets>(), "iterator", self);
        },
        nb::keep_alive<0, 1>());
    tcls.def("__getitem__", &Targets::source_targets, nb::keep_alive<0, 1>());

    cls.def(nb::init<const Connectivity&>());
    cls.def(
        "__init__",
        [](Connectivity* self, icus::ISet& source, icus::ISet& target,
           nb::ndarray<icus::index_type, nb::c_contig>& targets,
           const bool preserve_targets_order, const bool sorted_input_targets) {
            new (self) Connectivity{connectivity_from_array(
                source, target, targets, preserve_targets_order,
                sorted_input_targets)};
        },
        nb::arg("source"), nb::arg("target"), nb::arg("targets"),
        nb::arg("preserve_targets_order") = false,
        nb::arg("sorted_input_targets") = false);
    cls.def(
        "__init__",
        [](Connectivity* self, icus::ISet& source, icus::ISet& target,
           nb::list& targets, const bool preserve_targets_order,
           const bool sorted_input_targets) {
            new (self) Connectivity{connectivity_from_list(
                source, target, targets, preserve_targets_order,
                sorted_input_targets)};
        },
        nb::arg("source"), nb::arg("target"), nb::arg("targets"),
        nb::arg("preserve_targets_order") = false,
        nb::arg("sorted_input_targets") = false);
    cls.def("source", [](const Connectivity& self) {
        return std::visit([](auto&& C) { return C.source(); }, self);
    });
    cls.def("target", [](const Connectivity& self) {
        return std::visit([](auto&& C) { return C.target(); }, self);
    });
    cls.def(
        "targets", [](const Connectivity& self) { return Targets{self}; },
        nb::keep_alive<0, 1>());  // keep_alive to avoid dangling pointers
    cls.def(
        "targets_by_source",
        [](const Connectivity& self, const std::size_t i) {
            return Targets{self}.source_targets(i);
        },
        nb::keep_alive<0, 1>());  // keep_alive to avoid dangling pointers
    cls.def("is_consistent", [](const Connectivity& self) {
        return std::visit([](auto&& C) { return C.is_consistent(); }, self);
    });
    cls.def_prop_ro("has_sorted_targets", [](const Connectivity& self) {
        return std::visit([](auto&& C) { return C.has_sorted_targets(); },
                          self);
    });
    cls.def("sort_targets", [](Connectivity& self) {
        return std::visit([](auto&& C) { return C.sort_targets(); }, self);
    });
    cls.def("connections", [](const Connectivity& self) {
        return std::visit([](auto&& C) { return C.connections(); }, self);
    });
    cls.def("image", [](const Connectivity& self) {
        return std::visit([](auto&& C) { return C.image(); }, self);
    });
    cls.def("targets_size", [](const Connectivity& self) -> nb::object {
        return std::visit(
            [](auto&& C) -> nb::object {
                if constexpr (icus::is_regular_connectivity<decltype(C)>) {
                    return nb::cast(std::decay_t<decltype(C)>::targets_size_v);
                }
                return nb::none();
            },
            self);
    });
    cls.def_prop_ro("is_regular", [](const Connectivity& self) -> bool {
        return std::visit(
            [](auto&& C) -> bool {
                return icus::is_regular_connectivity<decltype(C)>;
            },
            self);
    });
    cls.def(
        "extract",
        [](const Connectivity& self, const icus::ISet& source,
           nb::handle target) {
            return std::visit(
                [&](auto&& C) -> Connectivity {
                    if (target.is_none()) {
                        return C.extract(source, C.target());
                    } else {
                        return C.extract(source,
                                         nb::cast<const icus::ISet&>(target));
                    }
                },
                self);
        },
        nb::arg("source"), nb::arg("target") = nb::none());
    cls.def(
        "extract",
        [](const Connectivity& self, const int targets_size,
           const icus::ISet& source, nb::handle target) {
            return extract_connectivity(self, targets_size, source, target);
        },
        nb::arg("targets_size"), nb::arg("source"),
        nb::arg("target") = nb::none());
    // DEPRECATION
    cls.def("extract1", [](const Connectivity& self, const icus::ISet& source,
                           const icus::ISet& target) {
        nb::print("!!! DEPRECATION WARNING !!!");
        nb::print(
            "Please use: Connectivity.extract(1, source) or "
            "Connectivity.extract(1, source, target)");
        return std::visit(
            [&](auto&& C) -> Connectivity {
                return C.template extract<1>(source, target);
            },
            self);
    });
    // FIXME: on the python side we could add a flag to check if the transpose
    // is regular
    auto transpose = [](const Connectivity& self) {
        return std::visit(
            [](auto&& C) -> Connectivity { return C.template transpose<-1>(); },
            self);
    };
    cls.def("transpose", transpose);
    cls.def_prop_ro("T", transpose);
    cls.def("compose", &do_compose_connectivities<-1>);
    cls.def("compose", &compose_connectivities<maximum_target_size>);
    cls.def(
        "as_array",
        [](const Connectivity& self) {
            return std::visit(
                [](auto&& C) -> nb::object {
                    constexpr auto ts =
                        std::decay_t<decltype(C)>::targets_size_v;
                    if constexpr (ts <= 0) {
                        throw std::runtime_error(
                            "Only regular connectivities can be converted to "
                            "numpy arrays!");
                    } else {
                        assert(C.source().size() * ((std::size_t)ts) ==
                               C.connections().size());
                        return nb::cast(nb::ndarray<const icus::index_type,
                                                    nb::numpy, nb::c_contig>(
                            C.targets().data()->data(),
                            {C.source().size(), (std::size_t)ts},
                            nb::handle()));
                    }
                },
                self);
        },
        nb::keep_alive<0, 1>());
}

void bind_connectivity_wrappers(nb::module_& module) {
    bind_connectivities(module);
    module.def("make_connectivity", &connectivity_from_array<>,
               nb::arg("source"), nb::arg("target"), nb::arg("targets"),
               nb::arg("preserve_targets_order") = false,
               nb::arg("sorted_input_targets") = false);
    module.def("make_connectivity", &connectivity_from_list, nb::arg("source"),
               nb::arg("target"), nb::arg("targets"),
               nb::arg("preserve_targets_order") = false,
               nb::arg("sorted_input_targets") = false);
}
