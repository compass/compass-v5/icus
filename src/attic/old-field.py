import copy
from importlib.metadata import entry_points
import numpy as np

# from .bindings import FieldInt, FieldFloat, FieldDouble, FieldByte, FieldBool


def build_field(iset, valuetype=None):
    # try native types first
    if valuetype in [None, "d", "double", np.float64, np.double]:
        return FieldDouble(iset)
    if valuetype in [float, "f", "float", np.float32]:
        return FieldFloat(iset)
    if valuetype in [int, "i", "int", np.int32, np.int64]:
        return FieldInt(iset)
    if valuetype in ["c", "int8", "byte", np.int8]:
        return FieldByte(iset)
    if valuetype in [bool, "b", "bool", np.bool_]:
        return FieldBool(iset)
    # use entry points
    for ep in entry_points(group="icus.build.field"):
        builder = ep.load()
        try:
            return builder(iset, valuetype)
        except TypeError:
            pass
    # FIXME: to handle deprecated cases
    if valuetype is not None and not isinstance(valuetype, type):
        return build_field(iset, type(valuetype))
    raise TypeError(f"no field avalaible for type {valuetype}")


def Field(iset, type=np.float64):
    _check_type(type)
    return build_field(iset, type)


def _check_type(type):
    if hasattr(type, "dtype_spec") and hasattr(type, "memory_footprint"):
        assert np.dtype(type.dtype_spec).itemsize == type.memory_footprint


# def _please_give_me_a_value(type):
#     # default value
#     try:
#         return type()
#     except Exception:
#         pass
#     # build from zero
#     try:
#         return type(0)
#     except Exception:
#         pass
#     # test possible values
#     try:
#         for obj in type._entries:
#             return obj
#     except Exception:
#         pass
#     # nothing worked -> FAILURE
#     raise ValueError(f"unable to instanciate the type: {type}")


def reinterpret(obj, new_type):
    return obj.as_bytes().view(new_type)
