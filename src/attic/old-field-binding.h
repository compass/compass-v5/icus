/** helpers for binding field with nanobind
 *
 * Adding a native type for field value
 *  ```cpp
 *  #include <icus/binding-field.h>
 *  #include <nanobind/nanobind.h>
 *
 *  using namespace icus::binding;
 *
 *  NB_MODULE(demo, module) {
 *    bind_field_native<int>(module, "FieldInt", "build_field_number");
 *    bind_field_native<double>(module, "FieldDouble",
 *      "build_field_number");
 *  }
 *  ```
 *
 * Adding a structured type for field value
 *  ```cpp
 *  #include <icus/binding-field.h>
 *  #include <nanobind/nanobind.h>
 *
 *  struct X { double x, y[2]; };
 *
 *  using namespace icus::binding;
 *
 *  NB_MODULE(demo, module) {
 *    bind_struct<X>(module, "MyStateX",
 *      dtype(field("x", "double"), field("y", "double", 2)));
 *    bind_field_struct<X>(module, "FieldX", "build_FieldX");
 *  }
 *  ```
 *
 *  Plugging the new types the entry points "icus.build.field"
 *  with a pyproject.toml file
 *  ``` pyproject.toml
 *  [project.entry-points."icus.build.field"]
 *  FieldX = "my_module:build_FieldX"
 *  ```
 *  or file a setup.py file
 *  ``` setup.py
 *  setup(
 *     # ...,
 *     entry_points = {
 *         'icus.build.field': [
 *             'FieldX = my_module:build_FieldX'
 *         ]
 *     }
 * )
 *  ```
 *
 */

#pragma once

#include <icus/Field.h>
#include <icus/ISet.h>
#include <nanobind/make_iterator.h>
#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>
#include <nanobind/operators.h>
#include <nanobind/stl/string.h>
#include <nanobind/stl/tuple.h>

#include <functional>
#include <span>
#include <sstream>
#include <type_traits>

#include "binding/attributes.h"
#include "binding/utils.h"

#define PY_SSIZE_T_CLEAN
#include <Python.h>

namespace nb = nanobind;

namespace icus {
namespace binding {

using BYTE = char;

template <typename pyClass>
void bind_field_factory(nb::module_& module, const std::string& factory_name,
                        pyClass& value_type) {
    // FIXME: this rely on the structure name
    using T = typename pyClass::Type;
    auto value_type_name =
        std::string{nb::cast<const char*>(value_type.attr("__name__"))};
    module.def(
        factory_name.c_str(),
        [value_type_name](const icus::ISet& I, nb::handle candidate_type) {
            if (value_type_name !=
                nb::cast<const char*>(candidate_type.attr("__name__"))) {
                throw nb::type_error();
            }
            return icus::Field<T>(I);
        });
}

template <typename Other, typename pyClass>
void wrap_set_field_from_other(pyClass& cls) {
    // using Field = typename pyClass::Type;
    // cls.def("__setitem__", [](Field& self, nb::slice s, const Other& other) {
    //     if (s.attr("start").is_none() && s.attr("stop").is_none() &&
    //         s.attr("step").is_none()) {
    //         // we don't want to invalidate reference
    //         // so we copy the field content
    //         if (self.size() != other.size())
    //             throw std::runtime_error("Inconsistent sizes!");
    //         std::copy(other.begin(), other.end(), self.begin());
    //     } else {
    //         throw std::runtime_error("Not all slices are handled...");
    //     }
    // });
    // cls.def("__setitem__", [](Field& self, nb::ellipsis, const Other& other)
    // {
    //     // we don't want to invalidate reference
    //     // so we copy the field content
    //     if (self.size() != other.size())
    //         throw std::runtime_error("Inconsistent sizes!");
    //     std::copy(other.begin(), other.end(), self.begin());
    // });
    // cls.def("__setitem__", [](Field& self, const ISet& I, const Other& other)
    // {
    //     // we don't want to invalidate reference
    //     // so we copy the field content
    //     auto self_view = self.view(I);
    //     if (self_view.size() != other.size())
    //         throw std::runtime_error("Inconsistent sizes!");
    //     std::copy(other.begin(), other.end(), self_view.begin());
    // });
}

template <typename pyClass>
void add_common_field_bindings(pyClass& cls) {
    //     using Field = typename pyClass::Type;
    //     using T = typename Field::value_type;

    //     auto np = nb::module_::import_("numpy");

    //     cls.def(nb::init<>());
    //     cls.def(nb::init<const ISet&>());
    //     cls.def_prop_ro("size", &Field::size);
    //     cls.def_ro("support", &Field::support);
    //     cls.def(
    //         "__iter__",
    //         [](const Field& self) {
    //             // std::cerr << "C++ loop:";
    //             // if constexpr (utils::is_native<T>) {
    //             //     for (auto&& x : self.span()) std::cerr << " " << x;
    //             // }
    //             // std::cerr << std::endl;
    //             return nb::make_iterator(nb::type<Field>(), "field_iterator",
    //             self);
    //         },
    //         nb::keep_alive<0, 1>());
    //     if constexpr (utils::is_native<T>) {
    //         cls.def(
    //         "__getitem__",
    //         [](Field& self, const ISet::index_type idx) {
    //             return self(idx);
    //         }/*,
    //         no nb::keep_alive<0, 1>() for native types*/);
    //     } else {
    //         cls.def(
    //             "__getitem__",
    //             [](Field& self, const ISet::index_type idx) {
    //                 return nb::cast(&self(idx), nb::rv_policy::reference);
    //             },
    //             nb::keep_alive<0, 1>());
    //     }
    //     cls
    //         .def("__getitem__", [](nb::object self, const ISet& I) ->
    //         nb::object {
    //             Field* p = nb::cast<Field*>(self);
    //             assert(p);
    //             if (I == p->support) return self;
    //             return nb::cast(p->view(I));
    //         } /*,
    // no nb::keep_alive<0, 1>() because field shared pointer will take care of
    // this*/);
    //     // FIXME: all of these __setitem__ methods could be factorized
    //     cls.def("__setitem__", [](Field& self, const ISet::index_type& idx,
    //                               const T& t) { self(idx) = t; });
    //     cls.def("__setitem__", [](Field& self, const ISet& I, const T& t) {
    //         if (I == self.support) {
    //             self = t;
    //         } else {
    //             self.view(I) = t;
    //         }
    //     });
    //     cls.def("__setitem__",
    //             [](Field& self, nb::ellipsis, const T& t) { self = t; });
    //     cls.def("__setitem__", [](Field& self, nb::slice s, const T& t) {
    //         if (s.attr("start").is_none() && s.attr("stop").is_none() &&
    //             s.attr("step").is_none()) {
    //             self = t;
    //         } else {
    //             throw std::runtime_error("Not all slices are handled...");
    //         }
    //     });
    //     if constexpr (std::is_enum_v<T>) {
    //         cls.def("__setitem__",
    //                 [](Field& self, const ISet::index_type& idx, const int
    //                 val) {
    //                     self(idx) = static_cast<T>(val);
    //                 });
    //         cls.def("__setitem__", [](Field& self, const ISet& I, const int
    //         val) {
    //             if (I == self.support) {
    //                 self = static_cast<T>(val);
    //             } else {
    //                 self.view(I) = static_cast<T>(val);
    //             }
    //         });
    //         cls.def("__setitem__", [](Field& self, nb::ellipsis, const int
    //         val) {
    //             self = static_cast<T>(val);
    //         });
    //         cls.def("__setitem__", [](Field& self, nb::slice s, const int
    //         val) {
    //             if (s.attr("start").is_none() && s.attr("stop").is_none() &&
    //                 s.attr("step").is_none()) {
    //                 self = static_cast<T>(val);
    //             } else {
    //                 throw std::runtime_error("Not all slices are
    //                 handled...");
    //             }
    //         });
    //     }
    //     if constexpr (utils::is_native<T> || std::is_enum_v<T>) {
    //         cls.def(
    //             "__setitem__",
    //             [](Field& self, nb::slice s,
    //                nb::ndarray<nb::numpy, T, nb::shape<nb::any>,
    //                nb::c_contig> a) {
    //                 if (s.attr("start").is_none() && s.attr("stop").is_none()
    //                 &&
    //                     s.attr("step").is_none()) {
    //                     utils::copy_array_into_field(a, self);
    //                 } else {
    //                     throw std::runtime_error("Not all slices are
    //                     handled...");
    //                 }
    //             });
    //         cls.def(
    //             "__setitem__",
    //             [](Field& self, nb::ellipsis,
    //                nb::ndarray<nb::numpy, T, nb::shape<nb::any>,
    //                nb::c_contig> a) {
    //                 utils::copy_array_into_field(a, self);
    //             });
    //         cls.def(
    //             "__setitem__",
    //             [](Field& self, const ISet& I,
    //                nb::ndarray<nb::numpy, T, nb::shape<nb::any>,
    //                nb::c_contig> a) {
    //                 utils::copy_array_into_field(a, self.view(I));
    //             });
    //     } else {
    //         cls.def("__setitem__", [np](nb::handle self, const
    //         ISet::index_type i,
    //                                     nb::tuple record) {
    //             // FIXME: real boilerplate...

    //             nb::object obj = np.attr("array")(
    //                 record, self.attr("value_type").attr("dtype_spec"));
    //             Py_buffer buffer;
    //             const auto status =
    //                 PyObject_GetBuffer(obj.ptr(), &buffer,
    //                 PyBUF_C_CONTIGUOUS);
    //             if (status != 0) throw std::runtime_error("Could not get
    //             buffer!"); Field& F = nb::cast<Field&>(self); F(i) =
    //             *reinterpret_cast<const T*>(buffer.buf);
    //         });
    //     }
    //     wrap_set_field_from_other<Field>(cls);
    //     wrap_set_field_from_other<toggle_field_view_t<Field>>(cls);
    //     cls.def("__setitem__", [](Field& self, nb::slice s, nb::object obj) {
    //         if (s.attr("start").is_none() && s.attr("stop").is_none() &&
    //             s.attr("step").is_none()) {
    //             utils::copy_pyobject_into_field(obj, self);
    //         } else {
    //             throw std::runtime_error("Not all slices are handled...");
    //         }
    //     });
    //     cls.def("__setitem__", [](Field& self, nb::ellipsis, nb::object obj)
    //     {
    //         utils::copy_pyobject_into_field(obj, self);
    //     });
    //     cls.def("__setitem__", [](Field& self, const ISet& I, nb::object obj)
    //     {
    //         utils::copy_pyobject_into_field(obj, self.view(I));
    //     });

    // cls.def("__setitem__",
    //         [](Field& self, const ISet::index_type& idx,
    //            nb::ndarray<nb::numpy, BYTE, nb::shape<sizeof(T)>> val) {
    //             self(idx) = as_struct<T>(val.data());
    //         });
    // cls.def("extract", [](const Field& self, const ISet& iset) {
    //     return self.extract(iset);
    // });
    // cls.def("view", [](const Field& self, const ISet& iset) {
    //     return self.view(iset);
    // });
    // auto __eq__ = [](const Field& f, const typename Field::value_type& t) {
    //     using T = typename Field::value_type;
    //     if constexpr (std::equality_comparable<T>) {
    //         using std::views::zip;
    //         const std::size_t n = f.size();
    //         bool* result = new bool[n];
    //         assert(result);
    //         nb::capsule owner(result, [](void* p) noexcept {
    //             delete[] reinterpret_cast<bool*>(p);
    //         });
    //         for (auto&& [x, cmp] : zip(f, std::span(result, result + n))) {
    //             cmp = (x == t);
    //         }
    //         return nb::ndarray<nb::numpy, bool>{result, {n}, owner};
    //     } else {
    //         throw nb::type_error("Cannot compare underlying field values!");
    //     }
    // };
    // // FIXME: the API differs from C++ where we return the equivalent of
    // // np.alltrue(...)
    // cls.def("__eq__", __eq__);
    // if constexpr (std::is_enum_v<T>) {
    //     cls.def("__eq__", [__eq__](const Field& self, const int val) {
    //         return __eq__(self, static_cast<T>(val));
    //     });
    // }
    // // CHECKME: do we add this?
    // // module.add("__eq__", [](const typename Field::value_type& t, const
    // Field&
    // // f){
    // //     return __eq__(f, t);
    // // });
    // //     # @property
    // // # def dtype(self):
    // // #     return np.dtype(getattr(self._value_type, "dtype_spec",
    // // self._value_type))

    // if constexpr (utils::is_native<T>) {
    //     cls.def("__repr__", [](Field& self) {
    //         std::ostringstream stream;
    //         stream << self;
    //         return stream.str();
    //     });
    // }
    // cls.def(
    //     "__deepcopy__",
    //     [](const Field& self, nb::handle memo) { return self.copy(); },
    //     nb::arg("memo") = nb::none());
    // // cls.def(nb::self+=nb::self);
    // // cls.def(nb::self-=nb::self);
    // // cls.def(nb::self*=nb::self);
    // // cls.def(nb::self/=nb::self);
}

template <typename T>
auto bind_plain_field(nb::module_& module, const std::string& name) {
    using ISet = icus::ISet;
    using Field = icus::Field<T>;

    nb::class_<Field> cls{module, name.c_str(),
                          "Tabular data indexed by an ISet"};

    add_common_field_bindings(cls);

    cls.def(
        "as_bytes",
        [](Field& self) {
            static_assert(sizeof(BYTE) == 1);
            return nb::ndarray<nb::numpy, BYTE, nb::shape<nb::any>,
                               nb::c_contig>{
                reinterpret_cast<BYTE*>(self.values.get()),
                {self.size() * sizeof(T)}};
        },
        nb::rv_policy::reference, nb::keep_alive<0, 1>());

    if constexpr (utils::is_native<T> || std::is_enum_v<T>) {
        cls.def(
            "as_array",
            [](Field& self) {
                if constexpr (std::is_enum_v<T>) {
                    return nb::ndarray<nb::numpy, std::underlying_type_t<T>>{
                        self.values.get(), {self.size()}};
                } else {
                    return nb::ndarray<nb::numpy, T>{self.values.get(),
                                                     {self.size()}};
                }
            },
            nb::rv_policy::reference, nb::keep_alive<0, 1>());
    } else {
        auto np = nb::module_::import_("numpy");

        cls.def("as_array", [np](nb::handle self) {
            return self.attr("as_bytes")().attr("view")(
                self.attr("value_type").attr("dtype_spec"),
                np.attr("recarray"));
        });
    }

    return cls;
}

template <typename fieldClass, typename valueClass>
inline void bind_value_type_information(fieldClass& cls,
                                        valueClass& value_type) {
#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
    cls.def_prop_ro_static("_instance",
                           [value_type](nb::handle) { return value_type; });
#endif
    cls.def_prop_ro_static("value_type",
                           [value_type](nb::handle) { return value_type; });
}

template <typename pyClass, typename Arg, typename... Args>
    requires(std::conjunction_v<is_attribute_info_t<Arg>,
                                is_attribute_info_t<Args>...>)
void _bind_attribute_accessors(pyClass& cls, const std::size_t offset,
                               Arg&& arg, Args&&... args) {
    using Field = typename pyClass::Type;
    cls.def_prop_ro(
        arg.name.c_str(),
        /* copy arg into closure to avoid dangling references */
        [offset, arg](Field& self) {
            using value_type = typename Field::value_type;
            std::vector<std::size_t> shape;
            shape.push_back(self.size());
            for (auto&& n : arg.shape) shape.push_back(n);
            std::vector<std::int64_t> strides;
            strides.resize(shape.size());
            using AT = typename Arg::type;
            auto s = sizeof(AT);
            static_assert(sizeof(value_type) % sizeof(AT) == 0);
            strides[0] = sizeof(value_type) / sizeof(AT);
            assert(strides.size() > 0);
            s = 1;
            for (std::size_t i = strides.size() - 1; i != 0; --i) {
                strides[i] = s;
                s *= shape[i];
            }
            // arithmetic on void* is illegal in both C and C++
            // https://stackoverflow.com/a/3524270
            if constexpr (std::is_enum_v<AT>) {
                using UT = std::underlying_type_t<AT>;
                auto p = (UT*)((char*)(self.values.get()) + offset);
                return nb::ndarray<nb::numpy, UT>{
                    p, shape.size(), shape.data(), {}, strides.data()};
            } else {
                auto p = (AT*)(((char*)self.values.get()) + offset);
                return nb::ndarray<nb::numpy, AT>{
                    p, shape.size(), shape.data(), {}, strides.data()};
            }
        },
        nb::keep_alive<0, 1>());
    if constexpr (sizeof...(args) != 0) {
        _bind_attribute_accessors(cls, offset + arg.memory_footprint(),
                                  std::forward<Args>(args)...);
    }
}

template <typename pyClass, typename... Args>
    requires(std::conjunction_v<is_attribute_info_t<Args>...>)
auto bind_plain_field(nb::module_& module, const std::string& name,
                      pyClass value_type, Args&&... args) {
    using T = typename pyClass::Type;

    auto cls = bind_plain_field<T>(module, name);

    bind_value_type_information(cls, value_type);

    if constexpr (sizeof...(Args) != 0) {
        assert(sizeof(T) == memory_footprint(std::forward<Args>(args)...));
        assert(unique_names(std::forward<Args>(args)...));
        _bind_attribute_accessors(cls, 0, std::forward<Args>(args)...);
    }

    return cls;
}

template <typename T>
auto bind_field_view(nb::module_& module, const std::string& name) {
    using ISet = icus::ISet;
    using FieldView = icus::Field<T, true>;

    nb::class_<FieldView> cls{module, name.c_str(),
                              "(view of) Tabular data indexed by an ISet"};

    add_common_field_bindings(cls);

    cls.def("as_bytes", [](FieldView& self) {
        throw nb::type_error(
            "as_bytes() not available for field views: work with "
            "copies");
    });
    cls.def("as_array", [](FieldView& self) {
        throw nb::type_error(
            "as_array() not available for field views: work with "
            "copies");
    });

    return cls;
}

template <typename pyClass>
auto bind_field_view(nb::module_& module, const std::string& name,
                     pyClass value_type) {
    auto cls = bind_field_view<typename pyClass::Type>(module, name);
    bind_value_type_information(cls, value_type);
    return cls;
}

template <typename T>
auto bind_field_and_view(nb::module_& module, const std::string& name) {
    auto field_cls = bind_plain_field<T>(module, name);
    auto view_cls = bind_field_view<T>(module, name + "View");
    return std::make_tuple(field_cls, view_cls);
}

template <typename pyClass>
inline auto bind_field(nb::module_& module, const std::string& name,
                       pyClass& value_type) {
    auto field_cls = bind_plain_field(module, name, value_type);
    auto view_cls = bind_field_view(module, name + "View", value_type);
    return std::make_tuple(field_cls, view_cls);
}

template <typename pyClass>
inline auto bind_field(nb::module_& module, const std::string& name,
                       pyClass& value_type, const std::string& factory_name) {
    auto classes = bind_field(module, name, value_type);
    bind_field_factory(module, factory_name, value_type);
    return classes;
}

template <typename T, typename... Args>
    requires(std::conjunction_v<is_attribute_info_t<Args>...>)
inline auto bind_field_and_struct(nb::module_& module,
                                  const std::string& field_name,
                                  const std::string& value_type_name,
                                  Args&&... args) {
    auto value_cls = bind_field_structure<T>(module, value_type_name,
                                             std::forward<Args>(args)...);
    auto field_cls = bind_plain_field(module, field_name, value_cls,
                                      std::forward<Args>(args)...);
    auto view_cls = bind_field_view(module, field_name + "View", value_cls);
    return std::make_tuple(field_cls, view_cls, value_cls);
}

template <typename T, typename... Args>
    requires(std::conjunction_v<is_attribute_info_t<Args>...>)
inline auto bind_field_and_struct(nb::module_& module,
                                  const std::string& field_name,
                                  const std::string& value_type_name,
                                  const std::string& factory_name,
                                  Args&&... args) {
    auto classes = bind_field_and_struct<T>(module, field_name, value_type_name,
                                            std::forward<Args>(args)...);
    bind_field_factory(module, factory_name, std::get<2>(classes));
    return classes;
}

template <typename T>
auto bind_field_native(nb::module_& module, const std::string& field_name) {
    return bind_field_and_view<T>(module, field_name);
}

template <typename pyClass, typename Arg, typename... Args>
    requires(is_attribute_info<Arg> &&
             std::conjunction_v<is_attribute_info_t<Args>...>)
void _bind_structure_attributes(pyClass& cls, const std::size_t offset,
                                Arg&& arg, Args&&... args) {
    using T = typename pyClass::Type;
    using AT = typename Arg::type;
    if (arg.shape.empty()) {  // plain type
        cls.def_prop_rw(
            arg.name.c_str(),
            [offset](const T* self) {
                return *((AT*)(((std::byte*)self) + offset));
            },
            [offset](T* self, nb::handle obj) {
                auto pattr = (AT*)(((std::byte*)self) + offset);
                if constexpr (std::is_enum_v<AT>) {
                    try {
                        *pattr = nb::cast<AT>(obj);
                        return;
                    } catch (nb::cast_error) {
                    }
                    *pattr = static_cast<AT>(nb::cast<int>(obj));
                } else {
                    *pattr = nb::cast<AT>(obj);
                }
            });
    } else {  // array like
        if constexpr (std::is_enum_v<AT>) {
            cls.def_prop_ro(
                arg.name.c_str(),
                /* copy arg into closure to avoid dangling references */
                [offset, arg](nb::object self) {
                    T* pself = nb::cast<T*>(self);
                    assert(pself);
                    using UT = std::underlying_type_t<AT>;
                    auto data = (UT*)(((std::byte*)pself) + offset);
                    return nb::ndarray<nb::numpy, UT>{data, arg.shape.size(),
                                                      arg.shape.data(), self};
                },
                nb::keep_alive<0, 1>());
        } else {
            cls.def_prop_ro(
                arg.name.c_str(),
                /* copy arg into closure to avoid dangling references */
                [offset, arg](nb::object self) {
                    T* pself = nb::cast<T*>(self);
                    assert(pself);
                    auto data = (AT*)(((std::byte*)pself) + offset);
                    return nb::ndarray<nb::numpy, AT>{data, arg.shape.size(),
                                                      arg.shape.data(), self};
                },
                nb::keep_alive<0, 1>());
        }
    }
    if constexpr (sizeof...(args) != 0) {
        _bind_structure_attributes(cls, offset + arg.memory_footprint(),
                                   std::forward<Args>(args)...);
    }
}

template <typename T, typename... Args>
    requires(std::conjunction_v<is_attribute_info_t<Args>...>)
auto bind_field_structure(nb::module_& module, const std::string& name,
                          Args&&... args) {
    if (sizeof(T) != memory_footprint(std::forward<Args>(args)...))
        throw std::runtime_error(
            "bind_structured_field: inconsistent memory footprints!");
    if (!unique_names(std::forward<Args>(args)...))
        throw std::runtime_error(
            "bind_structured_field: attributes names must be unique!");

    nb::class_<T> cls{module, name.c_str()};

    cls.def(nb::init<>());  // FIXME: this could be optional

    cls.def_prop_ro_static("memory_footprint",
                           [](nb::handle) { return sizeof(T); });
    auto dtype = numpy_dtype(std::forward<Args>(args)...);
    cls.def_prop_ro_static("dtype_spec", [dtype](nb::handle) { return dtype; });
    cls.def(
        "as_bytes",
        [](nb::object self) {
            static_assert(sizeof(BYTE) == 1);
            void* p = nb::cast<T*>(self);
            assert(p);
            return nb::ndarray<nb::numpy, BYTE>{static_cast<BYTE*>(p),
                                                {sizeof(T)}};
        },
        nb::rv_policy::reference, nb::keep_alive<0, 1>());

    _bind_structure_attributes(cls, 0, std::forward<Args>(args)...);

    return cls;
}

}  // namespace binding
}  // namespace icus
