#pragma once

#include <icus/Field.h>
#include <icus/ISet.h>
#include <nanobind/nanobind.h>

#include <functional>

#include "binding/array-view.h"
#include "binding/wrap.h"
#include "icus-field-binding_export.h"

namespace nb = nanobind;

namespace icus {

using field_factory = std::function<nb::object(const ISet&)>;

ICUS_FIELD_BINDING_EXPORT void register_factory_from_code(const std::string&,
                                                          field_factory&&);
ICUS_FIELD_BINDING_EXPORT void register_factory_from_np_dtype(
    const std::string&, field_factory&&);
ICUS_FIELD_BINDING_EXPORT void register_factory(PyTypeObject*, field_factory&&);
ICUS_FIELD_BINDING_EXPORT nb::object build_field(const ISet&, nb::handle);

namespace binding {

template <typename T>
inline nb::object factory(const ISet& I) {
    return nb::cast(Field<T>{I});
}
inline std::string view_name(const char* basename) {
    std::string name{basename};
    name.append("_view");
    return name;
}
template <typename T, typename Type_object>
void _register_native_factory(Type_object&& type_object) {
    register_factory(&type_object, &factory<T>);
}

template <typename T>
void register_native_factory() {}
template <typename T, typename Type_object, typename... Type_objects>
void register_native_factory(Type_object&& type_object,
                             Type_objects&&... type_objects) {
    _register_native_factory<T>(std::forward<Type_object>(type_object));
    if constexpr (sizeof...(type_objects) > 0) {
        register_native_factory<T>(std::forward<Type_objects>(type_objects)...);
    }
}

template <typename T, typename TT = void>
auto bind_field(nanobind::module_& m, const char* name,
                const std::vector<std::string>& codes = {},
                const std::vector<std::string>& numpy_dtypes = {},
                const dl::Pack<TT>& pack = {}) {
    nb::class_<Field<T>, Field_base> field{m, name};
    add_common_field_bindings(field, pack);
    nb::class_<Field<T, true>, Field_base> field_view{m,
                                                      view_name(name).c_str()};
    add_common_field_bindings(field_view);
    for (auto&& code : codes) {
        register_factory_from_code(code, &factory<T>);
    }
    for (auto&& np_dtype : numpy_dtypes) {
        register_factory_from_np_dtype(np_dtype, &factory<T>);
    }
    return std::make_tuple(field, field_view);
}
}  // namespace binding

template <typename T, typename TT = void>
auto bind_custom_field(nanobind::module_& m, const char* name, PyTypeObject* tp,
                       const std::vector<std::string>& codes = {},
                       const std::vector<std::string>& numpy_dtypes = {},
                       const dl::Pack<TT>& pack = {}) {
    auto result = binding::bind_field<T>(m, name, codes, numpy_dtypes, pack);
    register_factory(tp, &binding::factory<T>);
    return result;
}

template <typename T, typename... Type_objects>
auto bind_native_field(nanobind::module_& m, const char* name,
                       const std::vector<std::string>& codes = {},
                       const std::vector<std::string>& numpy_dtypes = {},
                       Type_objects&&... type_objects) {
    auto result =
        binding::bind_field<T>(m, name, codes, numpy_dtypes, pack<T>());
    binding::register_native_factory<T>(
        std::forward<Type_objects>(type_objects)...);
    return result;
}

}  // namespace icus
