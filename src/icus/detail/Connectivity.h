#pragma once

#include <algorithm>
#include <iterator>
#include <memory>
#include <numeric>
#include <ranges>
#include <span>
#include <type_traits>
#include <utility>

#include "../ISet.h"

namespace icus {
namespace detail {

namespace helpers {
template <std::ranges::range R>
bool has_unique_values(const R &r) {
    // copy because we sort the range
    std::vector<std::ranges::range_value_t<R>> tmp{std::begin(r), std::end(r)};
    return unique(begin(tmp), end(tmp)) == end(tmp);
}
}  // namespace helpers

template <class Connectivity_handle>
struct Connectivity
    : std::enable_shared_from_this<Connectivity<Connectivity_handle>> {
    using source_type = typename Connectivity_handle::source_type;
    using target_type = typename Connectivity_handle::target_type;
    using offset_type = size_type;
    using shared_pointer = std::shared_ptr<Connectivity<Connectivity_handle>>;
    static constexpr int targets_size_v = Connectivity_handle::targets_size_v;

    source_type source;
    target_type target;
    icus::ISet _connections;
    bool sorted_targets;
    std::vector<offset_type> _offsets;
    std::vector<index_type> _targets;

   private:
    friend Connectivity_handle;

    Connectivity() = delete;

    void _check_input_targets(const bool sorted_input) {
        if (!sorted_input) {
            check_if_sorted_targets();
        } else {
            sorted_targets = true;
            assert(check_if_sorted_targets());
        }
    }

    template <typename Neighborhood>
    void _build_targets_from_neighbours(Neighborhood &&neighbourhood,
                                        const bool sorted_input_targets) {
        assert(source.size() == std::ranges::size(neighbourhood));
        // reserve memory before push_backs to avoid
        // reallocation
        _offsets.reserve(source.size() + 1);
        _targets.reserve(std::ranges::fold_left(
            neighbourhood, 0,
            [](auto &&n, auto &&r) { return n + std::ranges::size(r); }));
        _offsets.push_back(0);
        for (auto &&neighbors : neighbourhood) {
            assert(helpers::has_unique_values(neighbors));
            assert(std::cmp_less_equal(targets_size_v, 0) ||
                   std::cmp_equal(neighbors.size(), targets_size_v));
            _offsets.push_back(_offsets.back() + neighbors.size());
            assert(std::ranges::all_of(neighbors, [&](auto &&i) {
                return std::cmp_greater_equal(i, 0) &&
                       std::cmp_less(i, target.size());
            }));
            _targets.insert(end(_targets), std::ranges::begin(neighbors),
                            std::ranges::end(neighbors));
        }
        assert(_offsets.capacity() == _offsets.size());
        _targets.shrink_to_fit();  // free unused memory
        _check_input_targets(sorted_input_targets);
    }

    template <typename Data>
    void _build_targets(Data &&data, const bool sorted_input_targets)
        requires(targets_size_v <= 0)
    {
        _build_targets_from_neighbours(std::forward<Data>(data),
                                       sorted_input_targets);
    }

    // FIXME: this is to be removed with a clearer separation between
    //        regular and random connectivities
    void _fill_regular_offsets()
        requires(targets_size_v > 0)
    {
        assert(_targets.size() == source.size() * targets_size_v);
        _offsets.resize(source.size() + 1);
        _offsets.shrink_to_fit();
        offset_type offset = 0;
        for (auto &&n : _offsets) {
            n = offset;
            offset += targets_size_v;
        }
        assert(_offsets.back() == source.size() * targets_size_v);
    }

    template <std::ranges::range Data>
    void _build_targets(Data &&data, const bool sorted_input_targets)
        requires(targets_size_v > 0)
    {
        using data_type = std::decay_t<Data>;
        if constexpr (std::ranges::range<typename data_type::value_type>) {
            // FIXME: to be removed, kept for backward compatibility
            _build_targets_from_neighbours(std::forward<Data>(data),
                                           sorted_input_targets);
        } else {
            if constexpr (std::is_same_v<data_type, decltype(_targets)>) {
                _targets = std::forward<Data>(data);
                _fill_regular_offsets();  // FIXME: to be removed
            } else {
                _targets.reserve(std::ranges::size(data));
                _targets.insert(_targets.end(), data.begin(), data.end());
                _fill_regular_offsets();  // FIXME: to be removed
            }
            _check_input_targets(sorted_input_targets);
        }
    }

    /** actually check and update the sorted_target flag */
    bool check_if_sorted_targets() {
        sorted_targets = true;
        for (auto &&r : targets()) {
            sorted_targets = std::ranges::is_sorted(r);
            if (!sorted_targets) break;
        }
        return sorted_targets;
    }

    void sort_targets() {
        if (!sorted_targets) {
            for (auto &&r : targets()) {
                std::sort(begin(r), end(r));
            }
            sorted_targets = true;
        }
#ifndef NDEBUG
        else {
            check_if_sorted_targets();
        }
#endif
        assert(sorted_targets);
    }

    template <typename Data>
    Connectivity(const source_type &source, const target_type &target,
                 Data &&data, const bool sorted_input_targets)
        : source{source}, target{target}, sorted_targets{false} {
        _build_targets(std::forward<Data>(data), sorted_input_targets);
        _connections = icus::ISet(_targets.size());
        assert(is_consistent());
    }

    operator bool() const {
        return (source && target && _offsets.size() != 0 &&
                _targets.size() != 0);
    }

    bool is_consistent() const {
        if (_connections.size() != _targets.size()) return false;
        // returns true if empty consistent connectivity
        // due to empty source
        // in this case _offset is of size 1 and contains 0
        if (_offsets.size() == 1) {
            if (_offsets[0] != 0) return false;
            return source.empty() && _targets.empty();
        }
        // if targets is not empty, test coherency
        if (!_targets.empty()) {
            // Return false if connections are not in target
            const auto N = target.size();
            if (!all_of(_targets.begin(), _targets.end(), [N](index_type i) {
                    return std::cmp_greater_equal(i, 0) && std::cmp_less(i, N);
                })) {
                return false;
            };
            std::set<index_type> unique_neighbours;
            for (index_type i = 1; i < _offsets.size(); ++i) {
                unique_neighbours.clear();
                unique_neighbours.insert(_targets.begin() + _offsets[i - 1],
                                         _targets.begin() + _offsets[i]);
                // Next line returns false if source element i has duplicate
                // neighbours
                if (unique_neighbours.size() !=
                    (_offsets[i] - _offsets[i - 1])) {
                    return false;
                }
            }
        }
        // offset is never suposed to be empty (even with empty connectivity)
        // it contains source.size() + 1 values
        if (_offsets.empty() || (_offsets.back() != _targets.size())) {
            return false;
        };
        if (sorted_targets) {
            for (auto &&r : targets()) {
                if (!std::ranges::is_sorted(r)) return false;
            }
        }
        return true;
    }

    bool is_same(const Connectivity &other) const {
        return (source == other.source) && (target == other.target) &&
               (_offsets == other._offsets) && (_targets == other._targets);
    }

    auto targets_by_source()
        requires(targets_size_v <= 0)
    {
        // FIXME: slide(2) should be replaced by C++23 adjacent_view<2>
        //        aka pairwise which is known at compile time
        //        moreover adjacent_view<2> | transform <=> pairwise_transform
        using std::views::iota;
        using std::views::transform;
        using std::views::zip;
        const auto o = _offsets.data();
        const auto p = _targets.data();
        return zip(source, iota((std::size_t)0, source.size()) |
                               transform([o, p](const auto i) {
                                   return std::ranges::subrange(
                                       p + *(o + i), p + *(o + i + 1));
                               }));
    }

    auto targets_by_source(const index_type s) const
        requires(targets_size_v <= 0)
    {
        assert(std::cmp_less(s + 1, _offsets.size()));
        const auto p = _targets.data();
        // return std::ranges::subrange(p + _offsets[s], p + _offsets[s+1]);
        return std::span(p + _offsets[s], p + _offsets[s + 1]);
    }

    auto connections_by_source(const index_type s) const
        requires(targets_size_v <= 0)
    {
        using std::views::iota;
        assert(std::cmp_less(s + 1, _offsets.size()));
        return iota(_offsets[s], _offsets[s + 1]);
    }

    // CHECKME: it might even be more efficient to write explicitely a transform
    // view here
    auto targets_by_source()
        requires(targets_size_v > 0)
    {
        using std::views::chunk;
        using std::views::zip;
        return zip(source, _targets | chunk(targets_size_v));
    }

    const std::array<index_type, targets_size_v> &targets_by_source(
        const index_type s) const
        requires(targets_size_v > 0)
    {
        return *reinterpret_cast<
            const std::array<index_type, targets_size_v> *>(_targets.data() +
                                                            s * targets_size_v);
    }

    auto connections_by_source(const index_type s) const
        requires(targets_size_v > 0)
    {
        using std::views::iota;
        assert(std::cmp_less(s + 1, _offsets.size()));
        return iota(s * targets_size_v, (s + 1) * targets_size_v);
    }

    // FIXME: the following trick relying on static member function could
    //        replaced by C++-23 Explicit object member functions
    //        https://en.cppreference.com/w/cpp/language/member_functions#Explicit_object_member_functions
    //        but this is not available with gcc 12.3.0

    template <typename Self>
    static auto _expose_targets(Self &self)
        requires(std::is_same_v<std::decay_t<Self>, Connectivity> &&
                 targets_size_v > 0)
    {
        assert(self._targets.size() == self.source.size() * targets_size_v);
        using array_type = std::array<index_type, targets_size_v>;
        using ptr = std::conditional_t<
            std::is_const_v<std::remove_reference_t<decltype(self)>>,
            const array_type *, array_type *>;
        return std::span{reinterpret_cast<ptr>(self._targets.data()),
                         self.source.size()};
    }

    template <typename Self>
    static auto _expose_targets(Self &self)
        requires(std::is_same_v<std::decay_t<Self>, Connectivity> &&
                 targets_size_v <= 0)
    {
        using std::views::iota;
        using std::views::transform;
        assert(self._offsets.size() > 0);
        auto poff = self._offsets.data();
        auto pcon = self._targets.data();
        auto targets_view = [poff, pcon](auto &&i) {
            return std::span{pcon + *(poff + i), pcon + *(poff + i + 1)};
        };
        return iota(static_cast<size_type>(0), self._offsets.size() - 1) |
               transform(targets_view);
    }

    auto targets() const { return _expose_targets(*this); }
    auto targets() { return _expose_targets(*this); }
};

}  // namespace detail

}  // namespace icus
