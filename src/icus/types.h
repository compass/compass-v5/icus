#pragma once

#include <compass_cxx_utils/tensor.h>

#include <cassert>
#include <cstdint>
#include <limits>

namespace icus {

using index_type = std::uint_fast32_t;
using size_type = std::size_t;

inline constexpr index_type size_to_index(const size_type s) {
    assert(s < std::numeric_limits<index_type>::max());
    return static_cast<index_type>(s);
}

// FIXME: the following is because nb::dtype<std::byte>() is not defined
using byte_type = unsigned char;
static_assert(sizeof(byte_type) == sizeof(std::byte));

using compass::utils::is_tensor;
using compass::utils::Tensor;

}  // namespace icus
