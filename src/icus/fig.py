""" fig: friendly icus glue
"""


import copy

import numpy as np

import icus

__all__ = [
    "Zone",
    "Data",
    "Record",
    "RecordShelf",
    "Shelf",
    "DataKind",
    "Scalar",
    "Vector",
    "Tensor",
]


def _vector(values, dtype="d"):
    values = np.asarray(values)
    v = icus.tensor(values.shape, dtype)()
    v.as_array()[:] = values
    return v


def as_zone(obj):
    if isinstance(obj, icus.ISet):
        return Zone(obj)
    return obj


def as_data(obj):
    if isinstance(obj, icus.Field):
        return Data(obj)
    return obj


class Zone:
    """A wrapper for icus.ISet that exposes set API."""

    def __init__(self, iset=None):
        if iset is None:
            iset = icus.ISet()
        assert isinstance(iset, icus.ISet)
        self._iset = iset

    @staticmethod
    def from_size(n):
        return Zone(icus.ISet(n))

    @property
    def iset(self):
        return self._iset

    def difference(self, other):
        other = as_zone(other)
        assert isinstance(other, Zone)
        if not self or not other:
            return self
        return type(self)(self._iset.substract(other._iset))

    def intersection(self, other):
        other = as_zone(other)
        assert isinstance(other, Zone)
        if icus.are_disjoint_sets(self._iset, other._iset):
            return type(self)(icus.ISet())
        return type(self)(
            icus.intersect(
                self._iset,
                other._iset,
                icus.lowest_common_ancestor(self._iset, other._iset),
            )
        )

    def isdisjoint(self, other):
        other = as_zone(other)
        assert isinstance(other, Zone)
        return icus.are_disjoint_sets(self._iset, other._iset)

    def issubset(self, other):
        other = as_zone(other)
        assert isinstance(other, Zone)
        if not self:
            return True
        if not other:
            return False
        return self._iset.is_included_in(other._iset)

    def issuperset(self, other):
        other = as_zone(other)
        assert isinstance(other, Zone)
        return other.issubset(self)

    def symmetric_difference(self, other):
        other = as_zone(other)
        assert isinstance(other, Zone)
        return self.union(other).difference(self.intersection(other))

    def union(self, other):
        other = as_zone(other)
        assert isinstance(other, Zone)
        return type(self)(icus.join(self._iset, other._iset))

    def __eq__(self, other):
        other = as_zone(other)
        if not isinstance(other, Zone):
            return False
        if not (self or other):
            return True  # empty == empty
        return self._iset == other._iset

    def __and__(self, other):
        return self.intersection(other)

    def __ge__(self, other):
        return self.issuperset(other)

    def __gt__(self, other):
        return (self >= other) and (self != other)

    def __le__(self, other):
        return self.issubset(other)

    def __len__(self):
        return self._iset.size()

    def __lt__(self, other):
        return (self <= other) and (self != other)

    def __ne__(self, other):
        return not (self == other)

    def __or__(self, other):
        return self.union(other)

    def __sub__(self, other):
        return self.difference(other)

    def __xor__(self, other):
        return self.symmetric_difference(other)


class DataKind:
    """Data kind description : Scalar, Vector(dim), Tensor(dim).
    convert a value into given kind : kind.convert(value)
    """

    def __init__(self, order, dim):
        order = int(order)
        dim = int(dim)
        assert order in (0, 1, 2)
        assert dim >= 0
        if order == 0:
            assert dim == 0
        self.__dict__["order"] = order
        self.__dict__["dim"] = dim

    def __eq__(self, other):
        assert isinstance(other, DataKind)
        return (self.order == other.order) and (self.dim == other.dim)

    def __repr__(self):
        if self.order == 0:
            return "Scalar"
        if self.order == 1:
            return f"Vector({self.dim})"
        if self.order == 2:
            return f"Tensor({self.dim})"
        return f"DataKind({self.order}, {self.dim})"

    @property
    def order(self):
        return self.__dict__["order"]

    @property
    def dim(self):
        return self.__dict__["dim"]

    @property
    def shape(self):
        return (self.dim,) * self.order

    @classmethod
    def Scalar(cls):
        return cls(0, 0)

    @classmethod
    def Vector(cls, dim):
        return cls(1, dim)

    @classmethod
    def Tensor(cls, dim):
        return cls(2, dim)

    @staticmethod
    def from_value(value, *, is_array=False):
        """Get a DataKind instance corresponding to value.

        Parameters
        ----------
        value: Any
            the value to guess kind from.
        is_array: bool
            indicates if value must be seen as one value or a 1d-array of values.

        """
        value = as_data(value)
        if isinstance(value, Data):
            return value.kind
        order = np.ndim(value)
        if is_array:
            order -= 1
        if order == 0:
            return DataKind(0, 0)
        shape = np.shape(value)
        if is_array:
            shape = shape[1:]
        dims = set(shape)
        if len(dims) != 1:
            raise TypeError(
                f"unable to get kind from {'array' if is_array else 'value'}: {value}"
            )
        dim = dims.pop()
        return DataKind(order, dim)

    def convert(self, value, *, is_array=None):
        """Convert a value into the given data kind.

        Parameters
        ----------
        value: Any
            the value to convert.
        is_array: Optional[bool]
            indicates if value must be seen as one value or a 1d-array of values.
            if None then a first conversion is attempted with as_array=False
            and a second with is_array=True in case of failure.

        """
        if is_array is None:
            try:
                return self.convert(value, is_array=False)
            except TypeError:
                return self.convert(value, is_array=True)
        kind = self.from_value(value, is_array=is_array)
        if kind == self:
            return value
        if self.order == self.dim == 1 and kind.order == 0:
            return [value]
        if self.order == 2:
            if kind.order == 1 and kind.dim == self.dim:
                if is_array:
                    value = np.array(value)
                    res = np.zeros([len(value), *([self.dim] * 2)], dtype=value.dtype)
                    for i in range(self.dim):
                        res[:, i, i] = value[:, i]
                    return res
                else:
                    return np.diag(value)
            if kind.order == 0:
                if is_array:
                    value = np.array(value)
                    res = np.zeros([len(value), *([self.dim] * 2)], dtype=value.dtype)
                    for i in range(self.dim):
                        res[:, i, i] = value[:]
                    return res
                else:
                    return np.diag([value] * self.dim)
        raise TypeError(
            f"{'array' if is_array else 'value'} cannot be converted into {self}: {value}"
        )


Scalar = DataKind.Scalar()
Vector = DataKind.Vector
Tensor = DataKind.Tensor


def resolve_field_type(dtype, kind):
    numpy_dtype = np.dtype(dtype)
    dtype = dtype if numpy_dtype.char == "O" else numpy_dtype
    if isinstance(dtype, np.dtype):
        return dtype if kind == Scalar else icus.tensor(kind.shape, dtype.char)
    if kind == Scalar:
        return dtype
    raise ValueError(f"not able to find adapted field type from {dtype=} and {kind=}")


def reduce_all(array):
    return np.all(array, tuple(i for i in range(1, array.ndim)))


def resolve_index(index, default):
    if index is Ellipsis:
        return default
    if isinstance(index, slice) and index == slice(None):
        return default
    return index


def is_valid_zone_index(index):
    index = as_zone(index)
    return isinstance(index, Zone) or (index != resolve_index(index, object()))


class Data:
    """A wrapper for icus.Field, compatible with numpy ufuncs and operators.

    When an operation would give a boolean array, get a Zone instead.

    Indexing operator, `data[zone]`, provides extraction to a sub-zone.
    """

    _field = None

    def __init__(self, field):
        assert isinstance(field, icus.Field)
        self._field = field

    @staticmethod
    def from_zone(zone, dtype=None):
        assert isinstance(zone, Zone)
        if dtype is None:
            field = icus.field(zone.iset)
        else:
            field = icus.field(zone.iset, dtype)
        return Data(field)

    @property
    def field(self):
        return self._field

    @property
    def zone(self):
        return Zone(self._field.support())

    @property
    def dtype(self):
        try:
            dtype = self._field.dtype
        except TypeError:
            dtype = None
        array_dtype = self._field.as_array().dtype
        return dtype or array_dtype

    def __getattr__(self, name):
        dtype = self.field.as_array().dtype
        if not (issubclass(dtype.type, np.record) and name in dtype.fields):
            raise AttributeError(f"'{type(self).__name__}' has no attribute '{name}'")
        return self[name]

    def __setattr__(self, name, value):
        if not self._field:
            return super().__setattr__(name, value)
        dtype = self.field.as_array().dtype
        if not (issubclass(dtype.type, np.record) and name in dtype.fields):
            return super().__setattr__(name, value)
        self[name] = value

    def __getitem__(self, zone):
        zone = resolve_index(zone, self.zone)
        if isinstance(zone, tuple):
            zone, *idx = zone
            if not is_valid_zone_index(zone):
                return self[(Ellipsis, zone, *idx)]
            zone = as_zone(zone)
            zone = resolve_index(zone, self.zone)
            sub_arr = self[zone].field.as_array().view(np.ndarray)
            if sub_arr.ndim == 1:
                assert len(idx) == 1
                arr = sub_arr[idx[0]]
            else:
                arr = sub_arr[(slice(None), *idx)]
            kind = DataKind.from_value(arr, is_array=True)
            field = icus.field(zone.iset, resolve_field_type(arr.dtype, kind))
            field.as_array()[:] = arr
            return Data(field)
        #
        if not is_valid_zone_index(zone):
            return self[Ellipsis, zone]
        zone = as_zone(zone)
        assert isinstance(zone, Zone)
        if not zone <= self.zone:
            raise KeyError("zone must be a subset of data definition zone")
        # Field getitem not enough bc as_array() not available for field views
        return Data(
            self._field.extract(zone.iset)
            if zone
            else icus.field(icus.ISet(), self.dtype)
        )

    def __setitem__(self, key, value):
        if isinstance(key, tuple):
            zone, *idx = key
            if not is_valid_zone_index(zone):
                self[(Ellipsis, *key)] = value
                return
        else:
            zone, idx = key, None
            if not is_valid_zone_index(zone):
                self[Ellipsis, key] = value
                return
        value = as_data(value)
        zone = resolve_index(zone, Ellipsis)
        zone = as_zone(zone)
        assert isinstance(zone, (Zone, type(Ellipsis)))
        #
        if idx is None:
            obj = self.field
            if isinstance(value, Data):
                assert zone is Ellipsis, "zone must be Ellipsis or slice"
                key = value.zone.iset
                value = value.field
            else:
                key = resolve_index(zone, self.zone).iset
                if isinstance(value, list):
                    value = _vector(value, self.field.as_array().dtype.char)
        else:
            arr = self.field.as_array().view(np.ndarray)
            if arr.ndim == 1:  # for case of struct
                arr = arr[idx[0]]
                idx = idx[1:]
            obj = arr
            if isinstance(value, Data):
                assert zone is Ellipsis, "zone must be Ellipsis or slice"
                zone = value.zone
                field = value.field
            else:
                zone = resolve_index(zone, self.zone)
                if isinstance(value, list):
                    value = _vector(value, arr.dtype.char)
                field = icus.field(zone.iset, type(value))
                field[zone.iset] = value
            key = (zone.iset.rebase(self.zone.iset).mapping_as_array(), *idx)
            value = field.as_array()
        #
        obj[key] = value

    def __array__(self, dtype=None):
        return np.asarray(self.field.as_array(), dtype)

    def __array_ufunc__(self, ufunc, method, *inputs, **kwds):
        assert method in ["__call__", "reduce"]
        #
        inputs = [as_data(v) for v in inputs]
        zone = self.zone
        zones = [v.zone for v in inputs if isinstance(v, Data)]
        if zones:
            zone, *zones = zones
            for next_zone in zones:
                zone = zone & next_zone
            inputs = [v[zone] if isinstance(v, Data) else v for v in inputs]
        #
        inputs = [v.field.as_array() if isinstance(v, Data) else v for v in inputs]
        arr = getattr(ufunc, method)(*inputs, **kwds)
        #
        if method == "reduce":
            return arr
        #
        if arr.dtype == bool:
            [idx] = np.where(arr)
            return Zone(zone.iset.extract(idx))
        #
        kind = DataKind.from_value(arr, is_array=True)
        field = icus.field(zone.iset, resolve_field_type(arr.dtype, kind))
        field[...] = arr
        return type(self)(field)

    def __eq__(self, other):
        return self.__array_ufunc__(
            lambda a, b: reduce_all(a == b), "__call__", self, other
        )

    def __ne__(self, other):
        return self.__array_ufunc__(
            lambda a, b: reduce_all(a != b), "__call__", self, other
        )

    def __gt__(self, other):
        return np.greater(self, other)

    def __ge__(self, other):
        return np.greater_equal(self, other)

    def __lt__(self, other):
        return np.less(self, other)

    def __le__(self, other):
        return np.less_equal(self, other)

    def __abs__(self):
        return np.abs(self)

    def __add__(self, other):
        return np.add(self, other)

    def __radd__(self, other):
        return np.add(other, self)

    def __sub__(self, other):
        return np.subtract(self, other)

    def __rsub__(self, other):
        return np.subtract(other, self)

    def __mul__(self, other):
        return np.multiply(self, other)

    def __rmul__(self, other):
        return np.multiply(other, self)

    def __truediv__(self, other):
        return np.true_divide(self, other)

    def __rtruediv__(self, other):
        return np.true_divide(other, self)

    def __neg__(self):
        return np.negative(self)

    def __pos__(self):
        return np.positive(self)

    def __pow__(self, other):
        return np.power(self, other)


class Record:
    """Data factory based on record.

    Add record with setitem : `record[key] = value`:
        - record[...] = const  -> constant default value (erases previous settings)
        - record[zone] = const -> set constant value to zone
        - record[...] = data   -> set Data object (variable value) to its definition zone

        Values are converted with respect to `record.kind`.

    Build Data with getitem : `record[zone] -> data`:
        Build a Data object with a given definition zone (`data.zone == zone`)
        and values extracted from record.

    """

    # TODO  access sub data for non float scalar data
    #   could be higher order -> access by axis
    #       idea: getitem with int/slice
    #   could be structured dtype (State) -> acces by fields
    #       idea: getattr matching fields

    def __init__(self, dtype="double", kind=Scalar):
        self._dtype = dtype
        self._kind = kind
        self._record = []

    @property
    def dtype(self):
        return self._dtype

    @property
    def kind(self):
        return self._kind

    def get_field_type(self):
        return resolve_field_type(self._dtype, self._kind)

    def definition_zone(self, zone):
        zones = [
            (v.zone & zone if isinstance(v, Data) else zone)
            if z is Ellipsis
            else z & zone
            for z, v in self._record
        ]
        if not zones:
            return Zone(icus.ISet())
        res, *zones = zones
        for z in zones:
            res = res | z
        return res

    def reduce_record(self, support, key, value):
        if isinstance(value, Data):
            key = value.zone
        key = resolve_index(key, support)
        if key is not support:
            key &= support
        value = as_data(value)
        value = value[key] if isinstance(value, Data) else value
        return (key, value)

    def __setitem__(self, key, value):
        key = resolve_index(key, Ellipsis)
        key = as_zone(key)
        assert key is Ellipsis or isinstance(key, Zone)
        value = as_data(value)
        if isinstance(value, Data):
            assert key is Ellipsis
            field = icus.field(value.zone.iset, value.dtype)
            arr = self._kind.convert(value.field.as_array(), is_array=True)
            field.as_array()[...] = arr
            value = Data(field)
        else:
            value = self._kind.convert(value, is_array=False)
            if key is Ellipsis:
                self._record.clear()
        self._record.append((key, value))

    def __getitem__(self, key):
        key = as_zone(key)
        assert isinstance(key, Zone)
        record = (self.reduce_record(key, k, v) for k, v in self._record)
        record = [(z, v) for z, v in record if z]
        field = icus.field(key.iset, self.get_field_type())
        result = Data(field)
        remains = key
        for zone, value in record:
            remains = remains - zone
            result[Ellipsis if isinstance(value, Data) else zone] = value
        if remains:
            raise ValueError(f"Some data are missing over requested zone.")
        return result


class Shelf:
    """Nested storage from template specification.

    Derive this class and override the class method `_instantiate_value()`
    to customize the translation of the specification.

    """

    def __init__(self, spec):
        assert all(isinstance(key, str) for key, _ in spec.items())
        self._data = self._instantiate_data(spec)

    @classmethod
    def _instantiate_data(cls, spec):
        if isinstance(spec, dict):
            return {key: cls._instantiate_data(value) for key, value in spec.items()}
        return cls._instantiate_value(spec)

    @classmethod
    def _instantiate_value(cls, value):
        return value

    def to_dict(self):
        return Shelf(self._data)._data

    def __getattr__(self, name):
        if name not in self.__dict__.get("_data", ()):
            raise AttributeError(
                f"'{type(self).__name__}' object has no attribute '{name}'"
            )
        data = self._data[name]
        if isinstance(data, dict):
            sub_shelf = type(self)({})
            sub_shelf._data = data
            return sub_shelf
        return data

    def __setattr__(self, name, value):
        if name in self.__dict__.get("_data", ()):
            raise AttributeError(f"can't set attribute '{name}'")
        self.__dict__[name] = value


class RecordShelf(Shelf):
    """Nested storage of Record objects based on template specification.

    >>> shelf = RecordShelf(
    ...     {
    ...         "A": {
    ...             "x": ("double", Scalar),
    ...         },
    ...         "B": {
    ...             "y": ("double", Scalar),
    ...         },
    ...         "z": ("double", Scalar),
    ...     }
    ... )
    >>> shelf.A.x  # -> Record
    >>> shelf.B.y  # -> Record
    >>> shelf.z    # -> Record

    """

    @classmethod
    def _instantiate_value(cls, spec):
        dtype, kind = spec
        return Record(dtype, kind)
