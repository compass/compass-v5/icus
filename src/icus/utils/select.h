#pragma once

#include <icus/Field.h>

namespace icus {

namespace helper {

struct Min {
    template <std::ranges::range R>
    inline auto operator()(R&& r) const {
        return std::ranges::min(std::forward<R>(r));
    }
};

}  // namespace helper

template <std::integral T, bool view, typename Connectivity,
          typename Selector = helper::Min>
Field<T, false> select(const Field<T, view>& color, Connectivity&& C,
                       const Selector& select = {}) {
    assert(C.target() == color.support);
    using std::views::transform;
    Field<T, false> result{C.source()};
    for (auto&& [i, targets] : C.targets_by_source()) {
        result(i) =
            select(targets | transform([color](auto k) { return color(k); }));
    }
    return result;
}

}  // namespace icus
