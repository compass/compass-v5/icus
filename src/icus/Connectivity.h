#pragma once

#include <compass_cxx_utils/range_utils.h>

#include <algorithm>
#include <cassert>

#include "ISet.h"
#include "icus/detail/Connectivity.h"

namespace icus {

template <typename Source, typename Target = Source, int targets_size = -1>
struct Connectivity {
    using source_type = Source;
    using target_type = Target;
    static constexpr int targets_size_v = targets_size;

   private:
    using implementation =
        detail::Connectivity<Connectivity<Source, Target, targets_size>>;
    using shared_pointer = typename implementation::shared_pointer;

    shared_pointer p;

    Connectivity(const shared_pointer &q) : p{q} {}
    Connectivity(shared_pointer &&q) : p{std::forward<shared_pointer>(q)} {}

   public:
    Connectivity() = default;
    Connectivity(const Connectivity &) = default;
    Connectivity(Connectivity &&) = default;
    Connectivity &operator=(const Connectivity &) = default;
    Connectivity &operator=(Connectivity &&) = default;
    template <int other_targets_size>
    Connectivity(const Connectivity<Source, Target, other_targets_size> &other)
        : p{new implementation{other.source(), other.target(), other.targets(),
                               other.has_sorted_targets()}} {
        assert(targets_size == -1);
    }
    template <int other_targets_size>
    Connectivity(Connectivity<Source, Target, other_targets_size> &&other)
        : p{new implementation{other.source(), other.target(), other.targets(),
                               other.has_sorted_targets()}} {
        assert(targets_size == -1);
    }

    /**
    the preserve_targets_order serves to determine if the targets must be sorted
    upon instantiation
    the sorted_input_targets flag serves to spare the check to dertermine
    if targets are sorted upon detail::Connectivity instantiation
    NB: consitency checks are made in DEBUG mode
        in detail::Connectivity to verify that targets are actually sorted
    */
    template <typename Data>
    Connectivity(const Source &S, const Target &T, Data &&data,
                 const bool preserve_targets_order = false,
                 const bool sorted_input_targets = false)
        : p{new implementation{S, T, std::forward<Data>(data),
                               sorted_input_targets}} {
        if (!(sorted_input_targets || preserve_targets_order)) {
            sort_targets();
        }
    }

    source_type source() const {
        assert(p);
        return p->source;
    }
    target_type target() const {
        assert(p);
        return p->target;
    }
    operator bool() const { return p && *p; }
    bool is_consistent() const {
        assert(p);
        return p->is_consistent();
    }
    const auto &connections() const {
        assert(p);
        return p->_connections;
    }
    bool has_sorted_targets() const {
        assert(p);
        return p->sorted_targets;
    }
    void sort_targets() {
        assert(p);
        p->sort_targets();
    }
    auto targets() const {
        assert(p);
        return p->targets();
    }
    auto targets_by_source() const {
        assert(p);
        return p->targets_by_source();
    }
    auto targets_by_source(const index_type s) const {
        assert(p);
        return p->targets_by_source(s);
    }
    auto connections_by_source(const index_type s) const {
        assert(p);
        return p->connections_by_source(s);
    }
    auto _data() const {
        assert(p);
        std::pair<const std::size_t *, const index_type *> result{
            nullptr, p->_targets.data()};
        if constexpr (targets_size_v <= 0) {
            result.first = p->_offsets.data();
        }
        return result;
    }
    bool operator==(const Connectivity &other) const { return p == other.p; }
    bool operator!=(const Connectivity &other) const { return p != other.p; }
    // this is useful to use Connectivity as map keys (the order is arbitrary)
    bool operator<(const Connectivity &other) const { return p < other.p; }
    bool is_same(const Connectivity &other) const {
        return p->is_same(*other.p);
    }
    template <int extract_targets_size = targets_size_v>
    auto extract(const source_type &new_source,
                 const target_type &new_target) const {
        // not necessary but otherwise not implemented yet
        if (new_target != target()) {
            throw std::runtime_error(
                "conn.extract with distinct targets not implemented yet");
        }
        // resolve_mapping makes sure new_source is
        // included into the source
        auto mapping = new_source.resolve_mapping(source());
        if constexpr (extract_targets_size > 0) {
            std::vector<index_type> new_targets;
            new_targets.reserve(extract_targets_size * new_source.size());
            visit_mapping(mapping, [&](auto i) {
                auto &&r = this->targets_by_source(i);
                assert(r.size() == extract_targets_size);
                new_targets.insert(new_targets.end(), r.begin(), r.end());
            });
            return Connectivity<Source, Target, extract_targets_size>{
                new_source, new_target, new_targets, true,
                has_sorted_targets()};
        } else {
            using targets_type = decltype(targets_by_source(index_type{}));
            std::vector<targets_type> new_targets;
            new_targets.reserve(new_source.size());
            visit_mapping(mapping, [&](auto i) {
                new_targets.emplace_back(this->targets_by_source(i));
            });
            return Connectivity<Source, Target, -1>{new_source, new_target,
                                                    new_targets, true,
                                                    has_sorted_targets()};
        }
    }
    template <int transpose_targets_size = -1>
    auto transpose() const {
        std::vector<std::vector<index_type>> neighbourhood_T(target().size());
        for (auto &&[k, neighbours] : targets_by_source()) {
            for (auto &&l : neighbours) neighbourhood_T[l].push_back(k);
        }
        return Connectivity<Target, Source, transpose_targets_size>(
            // the new connectivity is sorted by construction
            target(), source(), neighbourhood_T, true, true);
    }
    template <typename OtherTarget, int other_targets_size,
              int compose_targets_size = -1>
    auto compose(const Connectivity<Target, OtherTarget, other_targets_size>
                     &other) const {
        // basically the following is a matrix-product
        using compass::utils::sort_and_unique;
        assert(target() == other.source());
        std::vector<std::vector<index_type>> new_targets(source().size());
        for (auto &&i : source()) {
            auto &rowi = new_targets[i];
            for (auto &&j : targets_by_source(i)) {
                for (auto &&k : other.targets_by_source(j)) {
                    rowi.push_back(k);
                }
            }
            sort_and_unique(rowi);
            assert(std::cmp_less(compose_targets_size, 0) ||
                   std::cmp_equal(rowi.size(), compose_targets_size));
        }
        if constexpr (compose_targets_size > 0) {
            std::vector<index_type> all_targets;
            all_targets.reserve(source().size() * compose_targets_size);
            for (auto &&v : new_targets) {
                copy(v.begin(), v.end(), back_inserter(all_targets));
            }
            return Connectivity<Source, OtherTarget, compose_targets_size>(
                source(), other.target(), all_targets, true, true);
        } else {
            return Connectivity<Source, OtherTarget, -1>(
                source(), other.target(), new_targets, true, true);
        }
    }
    template <int other_targets_size, int intersect_targets_size = -1>
    auto intersect(
        Connectivity<Source, Target, other_targets_size> const &other) const {
        if (!has_sorted_targets() || !other.has_sorted_targets())
            throw std::runtime_error(
                "Intersect of non sorted connectivities not implemented yet");

        // compute the intersection of 2 connectivities:
        // term by term multiplication
        // this operation is symetric
        assert(source() == other.source());
        assert(target() == other.target());
        std::vector<std::vector<index_type>> new_targets(source().size());
        for (auto &&[s0, ts0] : targets_by_source()) {
            auto &row = new_targets[s0];
            auto &&ts1 = other.targets_by_source(s0);
            // find the intersection between ts0 and ts1, store it into row
            std::ranges::set_intersection(ts0, ts1, back_inserter(row));
        }

        return Connectivity<Source, Target, intersect_targets_size>(
            source(), target(), new_targets, true, true);
    }

    // get the iset of the non-empty colonnes of the connectivity
    // returns a sub-iset of target()
    auto image() const {
        std::vector<bool> hit_t(target().size(), false);
        // todo : could use directly the targets pointer, not accessible here
        for (auto &&ts : targets()) {
            for (auto &&t : ts) hit_t[t] = true;
        }
        return target().extract(hit_t);
    }
    template <int restrict_targets_size = -1>
    // build connectivity(source, new_target)
    // if simplify_source is false (default), source is the same source
    // if simplify_source is true, source is the extracted source where the
    // source indices not connected to new_target are removed.
    auto restrict_target(const target_type &new_target,
                         const bool simplify_source = false) const {
        // construct new connectivity using extract(new_source, new_target)
        auto restrict_transposed = transpose().extract(new_target, source());
        auto restrict_con =
            restrict_transposed.template transpose<restrict_targets_size>();
        if (!simplify_source) {
            // keep the same source iset : connectivity(source(), new_target)
            return restrict_con;
        } else {
            // construct other source iset where the indices not connected to
            // new_target are removed : connectivity(new_source, new_target)
            std::vector<index_type> new_source_ind;
            for (auto &&[s, targets] : restrict_con.targets_by_source()) {
                if (targets.size() > 0) new_source_ind.push_back(s);
            }
            return restrict_con.extract(source().extract(new_source_ind),
                                        new_target);
        }
    }

    friend std::ostream &operator<<(std::ostream &os,
                                    const Connectivity &conn_to_output) {
        if (targets_size > 0) {
            os << "The connectivity is regular, each source has "
               << targets_size << " targets" << std::endl;
        }
        os << "Connectivity source size: " << conn_to_output.source().size()
           << ", ";
        os << "target size: " << conn_to_output.target().size() << ", ";
        os << "number of connections: " << conn_to_output.connections().size()
           << std::endl;
        // compass::utils::pretty_print(conn_to_output.targets(), os, " ", "");
        return os;
    }
};

/** Build a connectivity instance.
 *  If the template int parameter is set to a strictly positive value,
 *  then targets will be accessible ranges of structured array through
 *  the target member function.
 */
template <int targets_size = -1, typename Source, typename Target,
          typename Data>
auto make_connectivity(const Source &source, const Target &target, Data &&data,
                       const bool preserve_targets_order = false,
                       const bool sorted_input_targets = false) {
    return Connectivity<Source, Target, targets_size>{
        source, target, std::forward<Data>(data), preserve_targets_order,
        sorted_input_targets};
}

inline bool have_same_support(const Connectivity<ISet, ISet> &con1,
                              const Connectivity<ISet, ISet> &con2) {
    assert(con1);
    assert(con2);
    // equality of the underlying unique pointer
    if (con1 == con2) {
        return true;
    }
    // otherwise checks that sources and targets are both the same
    return (con1.source() == con2.source()) && (con1.target() == con2.target());
}

/** \internal */
template <typename T>
struct Is_connectivity {
    static constexpr bool value = false;
};

/** \internal */
template <typename S, typename T, int v>
struct Is_connectivity<Connectivity<S, T, v>> {
    static constexpr bool value = true;
};

/** \internal */
template <typename T>
inline constexpr bool is_connectivity = Is_connectivity<std::decay_t<T>>::value;

/** \internal */
template <typename T>
struct Is_regular_connectivity {
    static constexpr bool value = false;
};

/** \internal */
template <typename S, typename T, int v>
    requires(v > 0)
struct Is_regular_connectivity<Connectivity<S, T, v>> {
    static constexpr bool value = true;
};

/** \internal */
template <typename T>
inline constexpr bool is_regular_connectivity =
    Is_regular_connectivity<std::decay_t<T>>::value;

/** Transpose a given connectivity. */
template <typename Con>
    requires(is_connectivity<Con>)
inline auto t(Con &&C) {
    return C.transpose();
}

template <typename Con0, typename Con1>
    requires(is_connectivity<Con0> && is_connectivity<Con1>)
inline auto intersect(Con0 &&C0, Con1 &&C1) {
    return C0.intersect(C1);
}

}  // namespace icus
