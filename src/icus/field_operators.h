#pragma once

#include "Field.h"

namespace icus {

namespace operators {

namespace detail {
struct iadd {
    template <typename A, typename B>
    static constexpr auto apply(A &a, const B &b) -> A & {
        return a += b;
    }
};
struct isub {
    template <typename A, typename B>
    static constexpr auto apply(A &a, const B &b) -> A & {
        return a -= b;
    }
};

struct imul {
    template <typename A, typename B>
    static constexpr auto apply(A &a, const B &b) -> A & {
        return a *= b;
    }
};
struct idiv {
    template <typename A, typename B>
    static constexpr auto apply(A &a, const B &b) -> A & {
        return a /= b;
    }
};
}  // namespace detail

template <typename T, bool iv, typename TT>
inline Field<T, iv> &operator+=(Field<T, iv> &P, const TT &a) {
    for (auto &&x : P) x += a;
    return P;
}

template <typename T, bool iv, typename TT>
inline Field<T, iv> &operator-=(Field<T, iv> &P, const TT &a) {
    for (auto &&x : P) x -= a;
    return P;
}

template <typename T, bool iv, typename TT>
inline Field<T, iv> &operator*=(Field<T, iv> &P, const TT &a) {
    for (auto &&x : P) x *= a;
    return P;
}

template <typename T, bool iv, typename TT>
inline Field<T, iv> &operator/=(Field<T, iv> &P, const TT &a) {
    for (auto &&x : P) x /= a;
    return P;
}

namespace detail {

template <typename S>
inline bool compliant_supports(const S &s1, const S &s2) {
    if constexpr (std::is_same_v<std::decay_t<S>, ISet>) {
        return s2.chain().contains(s1);
    } else {
        return s1 == s2;
    }
}

template <typename F, typename T1, typename T2, bool iv1, bool iv2>
inline Field<T1, iv1> &apply_iop(Field<T1, iv1> &P1, const Field<T2, iv2> &P2) {
    using std::views::all;
    using std::views::zip;
    assert(detail::compliant_supports(P1.support, P2.support));
    if (P1.support == P2.support) {
        for (auto &&[x1, x2] : zip(P1, P2)) {
            F::apply(x1, x2);
        }
    } else {
        assert(iv2);  // if supports are compliant
        assert((!iv1 && P1.support == P2.support.parent()) ||
               (iv1 && P1.support.parent() == P2.support.parent()));
        for (auto &&i2 : P2.support.mapping()) {
            F::apply(P1.values[i2], P2.values[i2]);
        }
    }
    return P1;
}

}  // namespace detail

template <typename T1, typename T2, bool iv1, bool iv2>
inline Field<T1, iv1> &operator+=(Field<T1, iv1> &P1,
                                  const Field<T2, iv2> &P2) {
    return detail::apply_iop<detail::iadd>(P1, P2);
}

template <typename T1, typename T2, bool iv1, bool iv2>
inline Field<T1, iv1> &operator-=(Field<T1, iv1> &P1,
                                  const Field<T2, iv2> &P2) {
    return detail::apply_iop<detail::isub>(P1, P2);
}

template <typename T1, typename T2, bool iv1, bool iv2>
inline Field<T1, iv1> &operator*=(Field<T1, iv1> &P1,
                                  const Field<T2, iv2> &P2) {
    return detail::apply_iop<detail::imul>(P1, P2);
}

template <typename T1, typename T2, bool iv1, bool iv2>
inline Field<T1, iv1> &operator/=(Field<T1, iv1> &P1,
                                  const Field<T2, iv2> &P2) {
    return detail::apply_iop<detail::idiv>(P1, P2);
}

}  // namespace operators

}  // namespace icus
