#pragma once

#include <icus/Slice.h>
#include <icus/types.h>

#include <algorithm>
#include <ranges>
#include <variant>
#include <vector>

namespace icus {

struct ISet;

struct Mapping {
    using slice = Slice<index_type>;
    using vector = std::vector<index_type>;

   private:
    using variant = std::variant<slice, vector>;
    variant idsv;

   public:
    using value_type = index_type;
    struct iterator {
       private:
        using variant = std::variant<typename slice::iterator,
                                     typename vector::const_iterator>;
        variant itv;

       public:
        using value_type = index_type;
        using pointer = const index_type*;
        using reference = const index_type&;
        // FIXME: must be defined to as signed type for ranges-v3
        //        even for forward iterators
        using difference_type = long;
        iterator() = default;
        iterator(const iterator&) = default;
        iterator(iterator&&) = default;
        iterator& operator=(const iterator&) = default;
        iterator& operator=(iterator&&) = default;
        template <typename T>
            requires(std::constructible_from<variant, T>)
        iterator(T&& t) : itv{t} {}
        // implementation
        reference operator*() const {
            return std::visit([](auto&& it) -> reference { return *it; }, itv);
        }
        iterator& operator++() {
            std::visit([](auto&& it) { ++it; }, itv);
            return *this;
        }
        iterator operator++(int) {
            iterator pre_increment{*this};
            std::visit([](auto&& it) { ++it; }, itv);
            return pre_increment;
        }
        bool operator==(const iterator& other) const {
            return itv == other.itv;
        }
        bool operator!=(const iterator& other) const {
            return itv != other.itv;
        }
        bool operator<(const iterator& other) const { return itv < other.itv; }
        // Not necessary as long as this class us used as a forward iterator
        // difference_type operator-(const Counter &other) const {
        //     return static_cast<difference_type>(value) -
        //     static_cast<difference_type>(other.value);
        // }
    };

    Mapping() = default;
    Mapping(const Mapping&) = default;
    Mapping(Mapping&&) = default;
    Mapping& operator=(const Mapping&) = default;
    Mapping& operator=(Mapping&&) = default;
    template <typename T>
        requires(std::constructible_from<variant, T>)
    Mapping(T&& t) : idsv{std::forward<T>(t)} {}
    template <typename T>
        requires(std::assignable_from<variant, T>)
    Mapping& operator=(T&& t) {
        assert(empty());
        idsv = std::forward<T>(t);
        return *this;
    }
    // Mapping& operator=(Mapping&&) = default;
    // template <std::integral... I>
    // Mapping(I&&... i)
    //     : idsv{std::vector<index_type>{std::forward<I>(i)...}} {}
    // template <std::ranges::range R>
    // bool operator==(R&& r) const {
    //     return std::ranges::equal(*this, std::forward<R>(r));
    // }
    // template <std::ranges::range R>
    // Mapping& operator=(R&& r) {
    //     assert(idsv.empty());
    //     idsv.reserve(
    //         std::ranges::size(r));  // CHECKME: useless ? cf. std::copy ?
    //     std::ranges::copy(std::forward<R>(r), back_inserter(idsv));
    //     return *this;
    // }
    // Mapping& operator=(vector&& v) {
    //     assert(empty());
    //     idsv = std::move(v);
    //     return *this;
    // }
    // const value_type* data() const { return idsv.data(); }
    // iterator begin() const noexcept { return idsv.cbegin(); }
    // iterator end() const noexcept { return idsv.cend(); }
    iterator begin() const noexcept {
        return std::visit([](auto&& ids) -> iterator { return ids.begin(); },
                          idsv);
    }
    iterator end() const noexcept {
        return std::visit([](auto&& ids) -> iterator { return ids.end(); },
                          idsv);
    }
    bool empty() const noexcept {
        return std::visit([](auto&& ids) -> bool { return ids.empty(); }, idsv);
    }
    std::size_t size() const noexcept {
        return std::visit([](auto&& ids) -> std::size_t { return ids.size(); },
                          idsv);
    }
    index_type operator[](const index_type k) const noexcept {
        return std::visit([k](auto&& ids) -> index_type { return ids[k]; },
                          idsv);
    }
    std::vector<index_type> copy_as_vector() const noexcept {
        return std::visit(
            [this](auto&& ids) -> std::vector<index_type> {
                if constexpr (std::is_same_v<std::decay_t<decltype(ids)>,
                                             vector>) {
                    return ids;
                } else {
                    static_assert(
                        std::is_same_v<std::decay_t<decltype(ids)>, slice>);
                    std::vector<index_type> result;
                    result.reserve(this->size());
                    std::ranges::copy(ids, back_inserter(result));
                    return result;
                }
            },
            idsv);
    }
    Mapping& sort() {
        std::visit(
            [this](auto&& ids) {
                if constexpr (std::is_same_v<std::decay_t<decltype(ids)>,
                                             vector>) {
                    std::ranges::sort(ids);
                }
            },
            idsv);
        return *this;
    }
    index_type min() const {
        assert(!empty());
        return std::visit(
            [this](auto&& ids) -> index_type {
                if constexpr (std::is_same_v<std::decay_t<decltype(ids)>,
                                             vector>) {
                    return *std::ranges::min_element(ids);
                } else {
                    static_assert(
                        std::is_same_v<std::decay_t<decltype(ids)>, slice>);
                    return ids.front();
                }
            },
            idsv);
    }
    index_type max() const {
        assert(!empty());
        return std::visit(
            [this](auto&& ids) -> index_type {
                if constexpr (std::is_same_v<std::decay_t<decltype(ids)>,
                                             vector>) {
                    return *std::ranges::max_element(ids);
                } else {
                    static_assert(
                        std::is_same_v<std::decay_t<decltype(ids)>, slice>);
                    return ids.back();
                }
            },
            idsv);
    }
    template <typename F>
    auto visit(F&& f) const {
        return std::visit(std::forward<F>(f), idsv);
    }
};

}  // namespace icus
