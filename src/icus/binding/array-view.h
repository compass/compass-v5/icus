#pragma once

#include <icus/dl-pack.h>

#include <utility>

namespace icus {

namespace helper {

template <typename T>
void check_array_shape(const dl::Pack<T>& pack,
                       nb::ndarray<T, nb::c_contig, nb::numpy> a,
                       const bool field = false) {
    if (field) {
        for (std::size_t i = 0; i < pack.ndim(); ++i) {
            if (std::cmp_not_equal(a.shape(i + 1), pack.shape[i]))
                throw nb::value_error("unconsistent array shape");
        }
    } else {
        for (std::size_t i = 0; i < pack.ndim(); ++i) {
            if (std::cmp_not_equal(a.shape(i), pack.shape[i]))
                throw nb::value_error("unconsistent array shape");
        }
    }
}

template <typename T>
void check_array_dimension(const dl::Pack<T>& pack,
                           nb::ndarray<T, nb::c_contig, nb::numpy> a) {
    if (a.ndim() != pack.ndim())
        throw nb::value_error("unconsistent array dimension");
}

}  // namespace helper

template <typename pyClass, typename TT>
void add_array_view(pyClass& cls, const dl::Pack<TT>& pack) {
    using Field = typename pyClass::Type;
    using T = typename Field::value_type;

    if constexpr (!std::is_same_v<TT, void>) {
        cls.def(
            "as_array",
            [pack](nb::handle h) {
                auto self = nb::cast<Field&>(h);
                const auto ndim = pack.ndim() + 1;
                std::vector<std::size_t> shape;
                shape.reserve(ndim);
                shape.push_back(self.support.size());
                for (auto&& s : pack.shape) {
                    shape.push_back(s);
                }
                std::vector<typename dl::Pack<TT>::size_type> strides;
                if (!pack.strides.empty()) {
                    strides.reserve(ndim);
                    strides.push_back(sizeof(T));
                    for (auto&& s : pack.strides) {
                        strides.push_back(s);
                    }
                }
                // FIXME: does std::vector<T>::data() return nullptr for an
                //        unallocated vector ?
                // nanobind will transform self.data() into an opaque pointer
                // under the hood constness is deduced from TT
                return nb::ndarray<TT, nb::numpy>(
                    self.data(), ndim, shape.data(), h,
                    strides.empty() ? nullptr : strides.data());
            },
            nb::keep_alive<0, 1>());
    }
}

template <typename pyClass, typename TT>
void _add_array_field_setters(pyClass& cls, const dl::Pack<TT>& pack) {
    using Field = typename pyClass::Type;

    if constexpr (!std::is_same_v<TT, void>) {
        assert(is_valid(pack));

        cls.def("__setitem__",
                [pack](Field& self, const index_type i,
                       nb::ndarray<TT, nb::c_contig, nb::numpy> a) {
                    helper::check_array_dimension(pack, a);
                    helper::check_array_shape(pack, a);
                    std::copy(a.data(), a.data() + pack.size(),
                              reinterpret_cast<TT*>(&self(i)));
                });
        cls.def("__setitem__", [pack](
                                   Field& self, const ISet& I,
                                   nb::ndarray<TT, nb::c_contig, nb::numpy> a) {
            if (a.ndim() == pack.ndim() + 1) {
                if (a.shape(0) != I.size())
                    throw nb::value_error("unconsistent dimensions");
                helper::check_array_shape(pack, a, true);
                auto p = a.data();
                for (auto&& x : self.view(I)) {
                    std::copy(p, p + pack.size(), reinterpret_cast<TT*>(&x));
                    p += pack.size();
                }
            } else {
                helper::check_array_dimension(pack, a);
                helper::check_array_shape(pack, a, true);
                for (auto&& x : self.view(I)) {
                    std::copy(a.data(), a.data() + pack.size(),
                              reinterpret_cast<TT*>(&x));
                }
            }
        });
        cls.def("__setitem__",
                [pack](Field& self, nb::ellipsis,
                       nb::ndarray<TT, nb::c_contig, nb::numpy> a) {
                    if (a.ndim() == pack.ndim() + 1) {
                        if (a.shape(0) != self.size())
                            throw nb::value_error("unconsistent dimensions");
                        helper::check_array_shape(pack, a, true);
                        auto p = a.data();
                        if (self.support.has_base()) {
                            for (auto&& x : self) {
                                std::copy(p, p + pack.size(),
                                          reinterpret_cast<TT*>(&x));
                                p += pack.size();
                            }
                        } else {
                            std::copy(p, p + pack.size() * self.support.size(),
                                      reinterpret_cast<TT*>(self.data()));
                        }
                    } else {
                        helper::check_array_dimension(pack, a);
                        helper::check_array_shape(pack, a, true);
                        for (auto&& x : self) {
                            std::copy(a.data(), a.data() + pack.size(),
                                      reinterpret_cast<TT*>(&x));
                        }
                    }
                });
    }
}

}  // namespace icus
