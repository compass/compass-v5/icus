#pragma once

#include <nanobind/nanobind.h>
#include <nanobind/ndarray.h>

#include <string>
#include <utility>

namespace icus {
namespace binding {

namespace nb = nanobind;

namespace utils {

#include <nanobind/nanobind.h>

namespace nb = nanobind;

// this is a mere copy/paste from nanobind/nb_internals.h
template <typename T>
struct scoped_pymalloc {
    scoped_pymalloc(size_t size = 1) {
        ptr = (T*)PyMem_Malloc(size * sizeof(T));
        if (!ptr)
            throw std::runtime_error(
                "icus::binding::utils::scoped_pymalloc failed");
    }
    ~scoped_pymalloc() { PyMem_Free(ptr); }
    T* release() {
        T* temp = ptr;
        ptr = nullptr;
        return temp;
    }
    T* get() const { return ptr; }
    T& operator[](size_t i) { return ptr[i]; }
    T* operator->() { return ptr; }

   private:
    T* ptr{nullptr};
};

template <typename T>
inline constexpr bool is_native =
    std::is_floating_point_v<T> || std::is_integral_v<T>;

template <typename Field,
          typename TT = typename std::decay_t<Field>::value_type>
void copy_array_into_field(nb::handle h, Field&& field) {
    using T = typename std::decay_t<Field>::value_type;
    auto a = nb::cast<nb::ndarray<TT, nb::numpy, nb::c_contig>>(h);
    if (a.ndim() != 1 || a.shape(0) != field.size())
        throw std::runtime_error("Inconsistent sizes!");
    if constexpr (std::is_enum_v<T>) {
        using std::views::transform;
        field.fill(std::span{(const TT*)a.data(), field.size()} |
                   transform([](auto&& i) { return static_cast<T>(i); }));
    } else {
        field.fill(std::span{a.data(), field.size()});
    }
}

// BVT = Buffer Value Type
template <typename Field,
          typename BVT = typename std::decay_t<Field>::value_type>
void copy_buffer_into_field(nb::handle h, Field&& field) {
    using T = typename std::decay_t<Field>::value_type;
    scoped_pymalloc<Py_buffer> view;
    auto status =
        PyObject_GetBuffer(steal(h).ptr(), view.get(), PyBUF_RECORDS_RO);
    if (status != 0) throw std::runtime_error("Could not get buffer!");
    const Py_buffer* buffer = view.get();
    const BVT* data = reinterpret_cast<const BVT*>(buffer->buf);
    assert(std::cmp_equal(buffer->len,
                          sizeof(T) * (buffer->ndim == 0 ? 1 : field.size())));
    switch (buffer->ndim) {
        case 0:
            field.fill(*data);
            break;
        case 1:
            field.fill(std::span{data, field.size()});
            break;
        default:
            throw nb::value_error("could not handle buffer dimension");
    }
}

template <typename Field>
void copy_pyobject_into_field(nb::handle h, Field&& field) {
    using std::views::zip;
    using T = typename std::decay_t<Field>::value_type;
    if (hasattr(h, "dtype")) {
        // FIXME: manage native types (PyLong_Type...)
        if constexpr (is_native<T>) {
            copy_array_into_field(h, std::forward<Field>(field));
        }
        if constexpr (std::is_enum_v<T>) {
            copy_array_into_field<Field, int>(h, std::forward<Field>(field));
        }
        if constexpr (!(is_native<T> || std::is_enum_v<T>)) {
            auto pyT = nb::borrow(nb::type<T>());
            if (hasattr(pyT, "numpy_dtype")) {
                if (h.attr("dtype").equal(pyT.attr("numpy_dtype"))) {
                    copy_buffer_into_field(h, std::forward<Field>(field));
                } else {
                    throw nb::value_error("incompatible dtypes");
                }
            } else {
                std::string message{"no numpy dtype provided for "};
                message += nb::repr(pyT).c_str();
                throw nb::value_error(message.c_str());
            }
        }
        return;
    }
    try {
        field.fill(nb::cast<T>(h));  // try implicit cast
        return;
    } catch (nb::cast_error&) {
    }
    if constexpr (std::is_enum_v<T>) {
        try {
            field.fill(static_cast<T>(nb::cast<int>(h)));
            return;
        } catch (nb::cast_error&) {
        }
    }
    if (!(nb::hasattr(h, "__len__") && nb::hasattr(h, "__iter__")))
        throw std::runtime_error("Cannot make something with object!");
    if (nb::len(h) != field.size())
        throw std::runtime_error("Inconsistent sizes!");
    for (auto&& [x, o] : zip(field, h)) {
        x = nb::cast<T>(o);
    }
}

template <typename T>
inline std::string numpy_typecode() {
    if constexpr (std::is_same_v<T, int>) return "int";
    if constexpr (std::is_same_v<T, char>) return "int8";
    if constexpr (std::is_same_v<T, float>) return "float";
    if constexpr (std::is_same_v<T, double>) return "double";
    if constexpr (std::is_enum_v<T>)
        return numpy_typecode<std::underlying_type_t<T>>();
    assert(false);
    return {};
}

}  // namespace utils
}  // namespace binding
}  // namespace icus
