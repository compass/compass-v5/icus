#pragma once

#include <icus/types.h>
#include <nanobind/make_iterator.h>
#include <nanobind/nanobind.h>

#include <sstream>

#include "utils.h"

namespace icus {

namespace nb = nanobind;
namespace ibu = icus::binding::utils;

template <typename pyClass>
void _add_getters(pyClass& cls) {
    using Field = typename pyClass::Type;
    using T = typename Field::value_type;
    if constexpr (ibu::is_native<T>) {
        cls.def(
            "__getitem__",
            [](Field& self, const icus::index_type idx) {
                return self(idx);
            }/*,
            no nb::keep_alive<0, 1>() for native types*/);
    } else {
        cls.def(
            "__getitem__",
            [](Field& self, const icus::index_type idx) {
                return nb::cast(&self(idx), nb::rv_policy::reference);
            },
            nb::keep_alive<0, 1>());
    }
    // extraction of a field view
    cls
        .def("__getitem__", [](nb::object self, const ISet& I) -> nb::object {
            Field* p = nb::cast<Field*>(self);
            assert(p);
            if (I == p->support) return self;
            return nb::cast(p->view(I));
        } /*,
no nb::keep_alive<0, 1>() because field shared pointer will take care of
this*/);
}

template <typename Other, typename pyClass>
void _wrap_set_field_from_other(pyClass& cls) {
    using Field = typename pyClass::Type;
    cls.def("__setitem__", [](Field& self, nb::slice s, const Other& other) {
        if (s.attr("start").is_none() && s.attr("stop").is_none() &&
            s.attr("step").is_none()) {
            // we don't want to invalidate reference
            // so we copy the field content
            if (self.size() != other.size())
                throw std::runtime_error("Inconsistent sizes!");
            std::copy(other.begin(), other.end(), self.begin());
        } else {
            throw std::runtime_error("Not all slices are handled...");
        }
    });
    cls.def("__setitem__", [](Field& self, nb::ellipsis, const Other& other) {
        // we don't want to invalidate reference
        // so we copy the field content
        if (self.size() != other.size())
            throw std::runtime_error("Inconsistent sizes!");
        std::copy(other.begin(), other.end(), self.begin());
    });
    cls.def("__setitem__", [](Field& self, const ISet& I, const Other& other) {
        // we don't want to invalidate reference
        // so we copy the field content
        auto self_view = self.view(I);
        if (self_view.size() != other.size())
            throw std::runtime_error("Inconsistent sizes!");
        std::copy(other.begin(), other.end(), self_view.begin());
    });
}

// FIXME: all of these __setitem__ methods could be factorized
template <typename pyClass>
void _add_setters(pyClass& cls) {
    using Field = typename pyClass::Type;
    using T = typename Field::value_type;

    // FIXME: real boilerplate, we have to keep a reference to numpy
    auto np = nb::module_::import_("numpy");

    cls.def("__setitem__", [](Field& self, const icus::index_type& idx,
                              const T& t) { self(idx) = t; });
    cls.def("__setitem__", [](Field& self, const ISet& I, const T& t) {
        if (I == self.support) {
            self.fill(t);
        } else {
            self.view(I).fill(t);
        }
    });
    cls.def("__setitem__",
            [](Field& self, nb::ellipsis, const T& t) { self.fill(t); });
    cls.def("__setitem__", [](Field& self, nb::slice s, const T& t) {
        if (s.attr("start").is_none() && s.attr("stop").is_none() &&
            s.attr("step").is_none()) {
            self.fill(t);
        } else {
            throw std::runtime_error("Not all slices are handled...");
        }
    });
    if constexpr (std::is_enum_v<T>) {
        cls.def("__setitem__",
                [](Field& self, const icus::index_type& idx, const int val) {
                    self(idx) = static_cast<T>(val);
                });
        cls.def("__setitem__", [](Field& self, const ISet& I, const int val) {
            if (I == self.support) {
                self.fill(static_cast<T>(val));
            } else {
                self.view(I).fill(static_cast<T>(val));
            }
        });
        cls.def("__setitem__", [](Field& self, nb::ellipsis, const int val) {
            self.fill(static_cast<T>(val));
        });
        cls.def("__setitem__", [](Field& self, nb::slice s, const int val) {
            if (s.attr("start").is_none() && s.attr("stop").is_none() &&
                s.attr("step").is_none()) {
                self.fill(static_cast<T>(val));
            } else {
                throw std::runtime_error("Not all slices are handled...");
            }
        });
    }
    if constexpr (ibu::is_native<T> || std::is_enum_v<T>) {
        // cls.def(
        //     "__setitem__",
        //     [](Field& self, nb::slice s,
        //        nb::ndarray<nb::numpy, T, nb::shape<nb::any>, nb::c_contig> a)
        //        {
        //         if (s.attr("start").is_none() && s.attr("stop").is_none() &&
        //             s.attr("step").is_none()) {
        //             ibu::copy_array_into_field(a, self);
        //         } else {
        //             throw std::runtime_error("Not all slices are
        //             handled...");
        //         }
        //     });
        // cls.def(
        //     "__setitem__",
        //     [](Field& self, nb::ellipsis,
        //        nb::ndarray<nb::numpy, T, nb::shape<nb::any>, nb::c_contig> a)
        //        {
        //         ibu::copy_array_into_field(a, self);
        //     });
        // cls.def(
        //     "__setitem__",
        //     [](Field& self, const ISet& I,
        //        nb::ndarray<nb::numpy, T, nb::shape<nb::any>, nb::c_contig> a)
        //        {
        //         ibu::copy_array_into_field(a, self.view(I));
        //     });
    }
    // else {
    //     cls.def("__setitem__", [np](nb::handle self, const icus::index_type
    //     i,
    //                                 nb::tuple record) {
    //         // FIXME: real boilerplate...

    //         nb::object obj = np.attr("array")(
    //             record, self.attr("dtype").attr("numpy_dtype"));

    //         Py_buffer buffer;
    //         const auto status =
    //             PyObject_GetBuffer(obj.ptr(), &buffer, PyBUF_C_CONTIGUOUS);
    //         if (status != 0) throw std::runtime_error("Could not get
    //         buffer!"); Field& F = nb::cast<Field&>(self); F(i) =
    //         *reinterpret_cast<const T*>(buffer.buf);
    //     });
    // }
    _wrap_set_field_from_other<Field>(cls);
    _wrap_set_field_from_other<toggle_field_view_t<Field>>(cls);
    cls.def("__setitem__", [](Field& self, nb::slice s, nb::object obj) {
        if (s.attr("start").is_none() && s.attr("stop").is_none() &&
            s.attr("step").is_none()) {
            ibu::copy_pyobject_into_field(obj, self);
        } else {
            throw std::runtime_error("Not all slices are handled...");
        }
    });
}

template <typename pyClass>
void _add_setters_from_pyobject(pyClass& cls) {
    using Field = typename pyClass::Type;

    cls.def("__setitem__", [](Field& self, nb::ellipsis, nb::object obj) {
        ibu::copy_pyobject_into_field(obj, self);
    });
    cls.def("__setitem__", [](Field& self, const ISet& I, nb::object obj) {
        ibu::copy_pyobject_into_field(obj, self.view(I));
    });
    // cls.def("__setitem__",
    //         [](Field& self, const icus::index_type& idx,
    //            nb::ndarray<nb::numpy, BYTE, nb::shape<sizeof(T)>> val) {
    //             self(idx) = as_struct<T>(val.data());
    //         });
}

template <typename pyClass>
void _add__eq__(pyClass& cls) {
    using Field = typename pyClass::Type;
    using T = typename Field::value_type;

    auto __eq__ = [](const Field& f, const typename Field::value_type& t) {
        using T = typename Field::value_type;
        if constexpr (std::equality_comparable<T>) {
            using std::views::zip;
            const std::size_t n = f.size();
            bool* result = new bool[n];
            assert(result);
            nb::capsule owner(result, [](void* p) noexcept {
                delete[] reinterpret_cast<bool*>(p);
            });
            for (auto&& [x, cmp] : zip(f, std::span(result, result + n))) {
                cmp = (x == t);
            }
            return nb::ndarray<nb::numpy, bool>{result, {n}, owner};
        } else {
            throw nb::type_error("Cannot compare underlying field values!");
        }
    };
    // FIXME: the API differs from C++ where we return the equivalent of
    // np.alltrue(...)
    cls.def("__eq__", __eq__);
    if constexpr (std::is_enum_v<T>) {
        cls.def("__eq__", [__eq__](const Field& self, const int val) {
            return __eq__(self, static_cast<T>(val));
        });
    }
    // CHECKME: do we add this?
    // module.add("__eq__", [](const typename Field::value_type& t, const Field&
    // f){
    //     return __eq__(f, t);
    // });
    //     # @property
    // # def dtype(self):
    // #     return np.dtype(getattr(self._value_type, "numpy_dtype",
    // self._value_type))
}

template <typename pyClass>
void _add__repr__(pyClass& cls) {
    using Field = typename pyClass::Type;
    using T = typename Field::value_type;

    if constexpr (ibu::is_native<T>) {
        cls.def("__repr__", [](Field& self) {
            std::ostringstream stream;
            stream << self;
            return nb::str{stream.str().c_str()};
        });
    }
}

template <typename pyClass>
void _add_copy_operators(pyClass& cls) {
    using Field = typename pyClass::Type;

    cls.def(
        "__deepcopy__",
        [](const Field& self, nb::handle) { return self.copy(); },
        nb::arg("memo") = nb::none());
}

template <typename Field>
nb::object field_as_byte_array(nb::handle h) {
    using T = typename Field::value_type;
    Field& self = nb::cast<Field&>(h);
    return nb::cast(nb::ndarray<byte_type, nb::numpy>(
        reinterpret_cast<byte_type*>(self.data()),
        {sizeof(T) * self.support.size()}, h));
}

template <typename pyClass, typename TT = void>
void add_common_field_bindings(pyClass& cls, const dl::Pack<TT>& pack = {}) {
    using Field = typename pyClass::Type;
    using T = typename Field::value_type;

    cls.def(nb::init<const ISet&>());
    cls.def_prop_ro("is_field", [](const Field&) { return true; });
    cls.def("size", &Field::size);
    cls.def("support", [](const Field& self) { return self.support; });
    cls.def_prop_ro("dtype", [](const Field&) { return nb::type<T>(); });
    // FIXME: deprecated
    cls.def_prop_ro("value_type", [](const Field&) { return nb::type<T>(); });
    cls.def(
        "__iter__",
        [](const Field& self) {
            // std::cerr << "C++ loop:";
            // if constexpr (ibu::is_native<T>) {
            //     for (auto&& x : self.span()) std::cerr << " " << x;
            // }
            // std::cerr << std::endl;
            return nb::make_iterator(nb::type<Field>(), "field_iterator", self);
        },
        nb::keep_alive<0, 1>());
    cls.def("extract", &Field::extract);
    // direct binding fails because of overloads
    cls.def("view", [](const Field& self, const ISet& iset) {
        return self.view(iset);
    });
    cls.def("copy", nb::overload_cast<>(&Field::copy, nb::const_));
    cls.def("copy", nb::overload_cast<const ISet&>(&Field::copy, nb::const_));
    cls.def("share", &Field::share);
    if constexpr (!is_view<Field>) {
        // we cannot expose Field views as bytes because
        // we cannot ensure data contiguity
        if (is_valid(pack)) {
#ifndef NDEBUG
            if (hasattr(cls, "numpy_dtype")) {
                std::cerr
                    << "pack declaration is overriding field item numpy_dtype"
                    << std::endl;
            }
#endif
            // field views cannot expose themselves as arrays
            // because we cannot ensure data contiguity
            add_array_view(cls, pack);
        } else {
            auto item_type = nb::type<T>();
            if (hasattr(item_type, "numpy_dtype")) {
                cls.def(
                    "as_array",
                    [item_type](nb::handle h) {
                        nb::module_ np = nb::module_::import_("numpy");
                        nb::handle dtype = item_type.attr("numpy_dtype");
                        auto bytes = field_as_byte_array<Field>(h);
                        return bytes.attr("view")(dtype).attr("view")(
                            np.attr("recarray"));
                    },
                    nb::keep_alive<0, 1>());
            }
        }
        cls.def("as_bytes", &field_as_byte_array<Field>,
                nb::keep_alive<0, 1>());
    }
    _add_getters(cls);
    _add_setters(cls);
    if (is_valid(pack)) {
        _add_array_field_setters(cls, pack);
    }
    if constexpr (!is_tensor<T>) {
        _add_setters_from_pyobject(cls);
    }
    _add__eq__(cls);
    _add__repr__(cls);
    _add_copy_operators(cls);

    // FIXME: do we add this ?
    // // cls.def(nb::self+=nb::self);
    // // cls.def(nb::self-=nb::self);
    // // cls.def(nb::self*=nb::self);
    // // cls.def(nb::self/=nb::self);
}

}  // namespace icus
