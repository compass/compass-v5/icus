#pragma once

#include <nanobind/nanobind.h>

#include <string>

#include "utils.h"

namespace icus {
namespace binding {

namespace nb = nanobind;

template <typename T>
struct Attribute_info {
    using type = T;
    std::string name;
    std::vector<std::size_t> shape;
    Attribute_info(const std::string& s) : name{s} {}
    Attribute_info(const std::string& s,
                   const std::initializer_list<std::size_t>& n)
        : name{s} {
        shape.reserve(n.size());
        std::copy(n.begin(), n.end(), back_inserter(shape));
    }
    std::size_t memory_footprint() const {
        std::size_t nb_bytes = sizeof(T);
        for (auto&& n : shape) nb_bytes *= n;
        return nb_bytes;
    }
    nb::tuple numpy_dtype() const {
        if (shape.empty())
            return nb::make_tuple(name, utils::numpy_typecode<T>());
        nb::list shape_list;
        for (auto&& n : shape) shape_list.append(n);
        return nb::make_tuple(name, utils::numpy_typecode<T>(), shape_list);
    }
};

template <typename T>
struct Is_attribute_info {
    using type = std::false_type;
};

template <typename T>
struct Is_attribute_info<Attribute_info<T>> {
    using type = std::true_type;
};

template <typename T>
using is_attribute_info_t = typename Is_attribute_info<std::decay_t<T>>::type;

template <typename T>
inline constexpr bool is_attribute_info = is_attribute_info_t<T>::value;

template <typename... Args>
    requires(std::conjunction_v<is_attribute_info_t<Args>...>)
auto numpy_dtype(Args&&... args) {
    nb::list result;
    (result.append(args.numpy_dtype()), ...);
    return result;
}

template <typename Arg, typename... Args>
    requires(is_attribute_info<Arg> &&
             std::conjunction_v<is_attribute_info_t<Args>...>)
std::size_t memory_footprint(Arg&& arg, Args&&... args) {
    if constexpr (sizeof...(Args) == 0) {
        return arg.memory_footprint();
    } else {
        return arg.memory_footprint() +
               memory_footprint(std::forward<Args>(args)...);
    }
}

template <typename Arg, typename... Args>
    requires(is_attribute_info<Arg> &&
             std::conjunction_v<is_attribute_info_t<Args>...>)
bool _unique_names(std::vector<std::string>& names, Arg&& arg, Args&&... args) {
    if (std::find(names.begin(), names.end(), arg.name) != names.end())
        return false;
    if constexpr (sizeof...(Args) == 0) {
        return true;
    } else {
        names.push_back(arg.name);
        return _unique_names(names, std::forward<Args>(args)...);
    }
}

template <typename... Args>
    requires(std::conjunction_v<is_attribute_info_t<Args>...>)
std::size_t unique_names(Args&&... args) {
    std::vector<std::string> names;
    return _unique_names(names, std::forward<Args>(args)...);
}

}  // namespace binding
}  // namespace icus
