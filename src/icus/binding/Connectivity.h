#pragma once

#include <icus/Connectivity.h>
#include <icus/ISet.h>
#include <nanobind/make_iterator.h>
#include <nanobind/nanobind.h>

#include <utility>
#include <variant>

namespace icus {
namespace binding {

namespace nb = nanobind;

constexpr int maximum_target_size = 12;

using Random_connectivity = icus::Connectivity<icus::ISet, icus::ISet>;
template <int ts>
using Regular_connectivity = icus::Connectivity<icus::ISet, icus::ISet, ts>;

template <typename T>
struct Connectivity_binding;

template <std::size_t... n>
struct Connectivity_binding<std::index_sequence<n...>> {
    using type =
        std::variant<Random_connectivity, Regular_connectivity<n + 1>...>;
};

// the C++ underlying type for all connectivities
using Connectivity = typename Connectivity_binding<
    std::make_index_sequence<maximum_target_size>>::type;

// build random connectivity from python list, return classical connectivity
// object
Random_connectivity _rand_connectivity_from_list(
    icus::ISet& source, icus::ISet& target, nb::list& targets,
    const bool preserve_targets_order, const bool sorted_input_targets) {
    std::vector<std::vector<index_type>> collected_targets;
    collected_targets.reserve(nb::len(targets));
    for (nb::handle h : targets) {
        collected_targets.emplace_back();
        auto& row = collected_targets.back();
        auto l = nb::cast<nb::iterable>(h);
        row.reserve(nb::len(l));
        for (nb::handle h : l) {
            row.push_back(nb::cast<index_type>(h));
        }
    }
    return Random_connectivity{source, target, collected_targets,
                               preserve_targets_order, sorted_input_targets};
}

// build regular connectivity from python list, return classical connectivity
// object
template <int ts>
    requires(ts > 0)
Regular_connectivity<ts> _reg_connectivity_from_list(
    icus::ISet& source, icus::ISet& target, nb::list& targets,
    const bool preserve_targets_order, const bool sorted_input_targets) {
    assert(std::all_of(targets.begin(), targets.end(), [](auto&& h) {
        return nb::len(nb::cast<nb::handle>(h)) == ts;
    }));
    std::vector<icus::index_type> all_targets;
    all_targets.reserve(ts * nb::len(targets));
    for (auto&& l : targets) {
        for (auto&& i : l) {
            all_targets.emplace_back(nb::cast<icus::index_type>(i));
        }
    }
    return Regular_connectivity<ts>{source, target, std::move(all_targets),
                                    preserve_targets_order,
                                    sorted_input_targets};
}

// build whatever connectivity from python list, return the C++ underlying type
// for all connectivities
template <int ts>
Connectivity _connectivity_from_list(icus::ISet& source, icus::ISet& target,
                                     nb::list& targets,
                                     const bool preserve_targets_order,
                                     const bool sorted_input_targets) {
    static_assert(ts > 0 || ts == -1);
    if constexpr (ts == -1) {
        return _rand_connectivity_from_list(source, target, targets,
                                            preserve_targets_order,
                                            sorted_input_targets);
    } else {
        return _reg_connectivity_from_list<ts>(source, target, targets,
                                               preserve_targets_order,
                                               sorted_input_targets);
    }
}

}  // namespace binding
}  // namespace icus
