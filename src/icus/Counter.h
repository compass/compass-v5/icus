#pragma once

#include <iterator>
#include <limits>
#include <utility>

namespace icus {

template <std::integral T>
struct Counter {
    struct iterator {
        // iterator traits
        using value_type = T;
        using pointer = const T *;
        using reference = const T &;
        // FIXME: must be defined to as signed type for ranges-v3
        //        even for forward iterators
        using difference_type = long;
        using iterator_category = std::forward_iterator_tag;
        // implementation
        T value;
        reference operator*() const { return value; }
        iterator &operator++() {
            ++value;
            return *this;
        }
        iterator operator++(int) {
            Counter pre_increment{*this};
            ++value;
            return pre_increment;
        }
        bool operator==(const iterator &other) const {
            return value == other.value;
        }
        bool operator!=(const iterator &other) const {
            return value != other.value;
        }
        bool operator<(const iterator &other) const {
            return value < other.value;
        }
        // Not necessary as long as this class us used as a forward iterator
        // difference_type operator-(const Counter &other) const {
        //     return static_cast<difference_type>(value) -
        //     static_cast<difference_type>(other.value);
        // }
    };
    static iterator begin() noexcept { return {0}; }
    static iterator begin(std::integral auto &&i) noexcept {
        assert(std::cmp_less_equal(i, std::numeric_limits<T>::max()));
        return {static_cast<T>(i)};
    }
    static iterator end() noexcept { return {std::numeric_limits<T>::max()}; }
    static iterator end(std::integral auto i) noexcept {
        assert(std::cmp_less_equal(i, std::numeric_limits<T>::max()));
        return {static_cast<T>(i)};
    }
};

}  // namespace icus
