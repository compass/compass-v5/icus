#pragma once

#include <icus/field-factories.h>

namespace icus {

// FIXME: use tempalte parameter pack and specific structures to bind arguments
//        in a generic way
// FIXME: bind_custom_field could be a more explicit name
// FIXME: maybe the pack info should be exposed directly by the field type
// binding if PY_VERSION_HEX >= 0x03090000 PyHeapTypeObject has ht_module
// attribute that can be use to retrieve scope from wrapped class
template <typename T, typename TT = void>
inline auto bind_field(nanobind::module_& scope, nb::class_<T>& value_type,
                       const char* name,
                       const std::vector<std::string>& codes = {},
                       const std::vector<std::string>& numpy_dtypes = {},
                       const dl::Pack<TT>& pack = {}) {
    auto to = nb::borrow<nb::type_object>(value_type.ptr());
    // actually binds Field<T> in the same scope as T
    return bind_custom_field<T>(scope, name, (PyTypeObject*)to.ptr(), codes,
                                numpy_dtypes, pack);
}

template <typename T, typename TT = void>
inline auto bind_field(nanobind::module_& scope, nb::class_<T>& value_type,
                       const char* name, const dl::Pack<TT>& pack) {
    auto to = nb::borrow<nb::type_object>(value_type.ptr());
    // actually binds Field<T> in the same scope as T
    return bind_custom_field<T>(scope, name, (PyTypeObject*)to.ptr(), {}, {},
                                pack);
}

}  // namespace icus
