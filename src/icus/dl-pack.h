#pragma once

#include <icus/types.h>
#include <nanobind/ndarray.h>

#include <iostream>
#include <vector>

namespace nb = nanobind;

namespace icus {

namespace dl {

// our own restricted implementation of dlpack
// TODO: improve this... make it standard / FAIR?
template <typename T>
struct Pack {
    using value_type = T;
    using size_type = int64_t;
    std::vector<size_type> shape = {};
    std::vector<size_type> strides = {};
    auto ndim() const { return shape.size(); }
    auto nbytes() const { return size() * sizeof(T); }
    auto memory_footprint() const { return nbytes(); }
    auto size() const {
        size_type res = 1;
        for (auto&& n : shape) {
            res *= n;
        }
        return res;
    }
};

template <>
struct Pack<void> {};

template <typename T, typename Shape,
          typename Strides = std::initializer_list<typename Pack<T>::size_type>>
auto pack(const Shape& shape, const Strides& strides = {}) {
    Pack<T> result;
    result.shape.reserve(shape.size());
    for (auto&& s : shape) {
        assert(s > 0);
        result.shape.push_back(s);
    }
    result.strides.reserve(strides.size());
    for (auto&& s : strides) {
        result.strides.push_back(s);
    }
    return result;
}

// relies on domain look-up
template <typename T>
inline constexpr bool is_valid(const dl::Pack<T>&) {
    return true;
}

// relies on  domain look-up
inline constexpr bool is_valid(const dl::Pack<void>&) { return false; }

}  // namespace dl

template <typename T>
struct Pack {
    static constexpr auto info() { return dl::Pack<void>{}; }
};

template <typename T>
    requires(std::is_arithmetic_v<T> || std::is_same_v<T, byte_type>)
struct Pack<T> {
    static constexpr auto info() { return dl::Pack<T>{}; }
};

template <typename T, std::size_t... nk>
struct Pack<Tensor<T, nk...>> {
    static constexpr dl::Pack<T> info() {
        return dl::pack<T>(Tensor<T, nk...>::shape());
    }
};

template <typename T>
inline constexpr auto pack() {
    return Pack<T>::info();
}

}  // namespace icus
