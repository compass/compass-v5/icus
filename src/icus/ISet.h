#pragma once

#include <compass_cxx_utils/pretty_print.h>
#include <compass_cxx_utils/range_utils.h>
#include <compass_cxx_utils/self-managed.h>
#include <icus/Counter.h>
#include <icus/Mapping.h>
#include <icus/types.h>

#include <algorithm>
#include <concepts>
#include <iostream>
#include <numeric>
#include <optional>
#include <ranges>
#include <set>
#include <stdexcept>
#include <vector>

namespace icus {

using compass::utils::Self_managed_object;

/** The building brick.

    \anchor ISet_detailed_description

    \snippet iset/base.cpp Creating an ISet
*/
struct ISet {
   public:
    using mapping_t = Mapping;
#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
    using mapper_type = Mapping;  // deprecated: use mapping_t instead
#endif
    using iterator = typename Counter<index_type>::iterator;

   private:
    struct data : Self_managed_object<data> {
        // FIXME: it is probably useless to have index_type and size_type
        //        as indices are supposed to be used up to ISet::size() - 1
        index_type size;
        handle parent;
        mapping_t mapping;
        data() = default;
        data(std::integral auto n) : size{(index_type)n} {
            assert(std::cmp_greater(n, 0));
            assert(std::cmp_less(n, std::numeric_limits<index_type>::max()));
        }
    };

    data::handle handle;

   public:
    struct Chain {
        struct iterator {
            using value_type = ISet;
            using pointer = ISet *;
            using reference = ISet &;
            // FIXME: must be defined to as signed type for ranges-v3
            //        even for forward iterators
            using difference_type = long;
            data *p = nullptr;
            auto operator*() const { return ISet{p}; }
            iterator &operator++() {
                assert(p);
                p = p->parent;
                return *this;
            }
            iterator operator++(int) {
                assert(p);
                iterator pre_increment{*this};
                p = p->parent;
                return pre_increment;
            }
            bool operator==(const iterator &it) const { return p == it.p; }
            bool operator!=(const iterator &it) const { return p != it.p; }
            bool operator<(const iterator &it) const { return p < it.p; }
            /** Will only compare handles */
            bool operator==(const ISet &I) const { return p == I.handle.raw(); }
            /** Will only compare handles */
            bool operator!=(const ISet &I) const { return p != I.handle.raw(); }
        };
        Chain() = default;
        Chain(const ISet &I) : root{I.handle} {}
        iterator begin() const { return iterator{root}; }
        constexpr iterator end() const { return {}; }
        bool empty() const { return !root; }
        std::size_t size() const {
            std::size_t n = 0;
            for (auto it = begin(); it != end(); ++it) {
                ++n;
            }
            return n;
        }
        /** will only compare handles */
        bool contains(const ISet &I) const {
            for (auto it = begin(); it != end(); ++it) {
                if (it == I) return true;
            }
            return false;
        }
        std::optional<ISet> back() const {
            data *p = root;
            while (p) {
                if (!p->parent) return {ISet{data::handle{p}}};
                p = p->parent;
            }
            return {};
        }

       private:
        // keeping a reference to the lowest element of the chain of ISet is
        // enough so we can use raw pointers as iterators
        data::handle root = {};
    };

    struct Mapping_view {
        data::handle p;  // to be sure the mapping reference is valid
        const mapping_t &map;
        Mapping_view() = delete;
        Mapping_view(const Mapping_view &) = delete;
        Mapping_view(Mapping_view &&) = delete;
        Mapping_view &operator=(const Mapping_view &) = delete;
        Mapping_view &operator=(Mapping_view &&) = delete;
        bool operator==(const Mapping_view &other) const {
            return (p == other.p) && (&map == &other.map);
        }
        index_type operator[](const index_type &k) const noexcept {
            return map[k];
        }

       private:
        friend ISet;
        Mapping_view(const data::handle &sp, const mapping_t &mapref)
            : p{sp}, map{mapref} {
            assert(sp->size == sp->mapping.size());
            assert(&(sp->mapping) == &map);
        }
    };

   private:
    ISet(const data::handle &other) : handle{other} {}

   public:
    /** Default construct to empty ISet. */
    ISet() = default;
    ISet(const ISet &) = default;
    ISet(ISet &&) = default;
    ISet &operator=(const ISet &) = default;
    ISet &operator=(ISet &&) = default;
    /** ISet constructor.
    @param n the ISet size, n==0 will produce an empty ISet.
    */
    ISet(std::integral auto n) {
        assert(!handle);
        assert(n >= 0);
        if (n > 0) handle = data::make(n);
    }
    /** Empty ISet evaluates as false, all others as true. */
    operator bool() const {
        assert(!handle || handle->size > 0);
        return (bool)handle;
    }
    // "==" if the two ISets have same elements in same order
    // possibly different instances
    bool operator==(const ISet &other) const {
        // easy bets first
        if (handle == other.handle) return true;
        if (size() != other.size()) return false;
        auto lca = lowest_common_ancestor(*this, other);
        if (!lca) return false;
        return std::ranges::equal(resolve_mapping(*lca),
                                  other.resolve_mapping(*lca));
    }
    bool operator!=(const ISet &other) const { return !((*this) == other); }
    // this is useful to use ISet as map keys (the order is arbitrary)
    /** Compares the underlying pointers. This is useful to use ISet as map
     * keys. The resulting order is arbitrary. */
    bool operator<(const ISet &other) const { return handle < other.handle; }
    std::size_t size() const {
        if (handle) {
            assert(handle->size > 0);
            return handle->size;
        }
        return 0;
    }

   private:
    index_type size_as_index_type() const {
        if (handle) {
            assert(handle->size > 0);
            return handle->size;
        }
        return 0;
    }

   public:
    bool empty() const { return !handle; }
    ISet extract(index_type start, index_type end) const {
        if (end <= start) return {};
        return extract(mapping_t::slice{start, end});
    }
    template <std::ranges::range R>
        requires(std::is_same_v<
                 typename std::decay_t<std::ranges::range_value_t<R>>, bool>)
    ISet extract(R &&mask) const {
        assert(mask.size() == size());
        // get indices of the mask where true
        std::size_t n = 0;
        for (auto &&keep : mask) {
            if (keep) ++n;
        }
        mapping_t::vector where;
        where.reserve(n);
        for (auto i = begin(); auto &&keep : mask) {
            if (keep) where.push_back(*i);
            ++i;
        }
        assert(where.capacity() == where.size());
        // if where is empty, returns empty iset (has no parent !)
        if (where.empty()) return ISet{};
        return extract(move(where));
    }
    template <std::ranges::range R>
        requires(
            std::assignable_from<std::add_lvalue_reference_t<mapping_t>, R>)
    ISet extract(R &&indices) const {
        // if r is empty, returns empty iset (has no parent !)
        if (std::ranges::empty(indices)) return ISet{};
        assert(std::cmp_less(indices.size(),
                             std::numeric_limits<index_type>::max()));
        ISet result{(index_type)indices.size()};
        result.set_base(handle, std::forward<R>(indices));
        return result;
    }
    template <std::integral I>
    ISet extract(const std::initializer_list<I> &indices) const {
        if (indices.size() == 0) return {};
        return extract(mapping_t::vector{indices.begin(), indices.end()});
    }
    template <std::ranges::range R>
        requires(
            !std::assignable_from<std::add_lvalue_reference_t<mapping_t>, R> &&
            !std::is_same_v<
                typename std::decay_t<std::ranges::range_value_t<R>>, bool>)
    ISet extract(const R &indices) const {
        if (indices.empty()) return {};
        return extract(mapping_t::vector{indices.begin(), indices.end()});
    }
    auto use_count() const { return handle->use_count(); }
    /** Works for any ISet (including empty ISets) */
    iterator begin() const { return {0}; }
    /** Works for any ISet (including empty ISets) */
    iterator end() const { return {size_as_index_type()}; }
    bool has_base() const { return handle && handle->parent; }
    bool has_no_base() const { return !has_base(); }

   private:
    void throw_if_no_base(const auto &callee) const {
        if (!has_base())
            throw std::runtime_error(std::string{"Invalid call to "} + callee +
                                     std::string{": ISet has no base"});
    }

   public:
    ISet parent() const {
        throw_if_no_base("parent()");
        return ISet{handle->parent};
    }
    const mapping_t &mapping() const {
        throw_if_no_base("mapping()");
        return handle->mapping;
    };
    auto mapping_view() const { return Mapping_view{handle, mapping()}; }
    [[deprecated("use ISet::mapping()::operator[] e.g. I.mapping()[k]")]]
    index_type mapping(index_type k) const {
        // TODO: we might show a warning here hase checking the base may lead to
        // performance penalties
        throw_if_no_base("mapping(index_type)");
        return handle->mapping[k];
    }

   private:
    bool is_consistent_mapping(
        const data *parent, const std::ranges::range auto &R) const noexcept {
        if (!parent) return false;
        if (std::ranges::size(R) != size()) return false;
        // check bounds and look for duplicates
        const auto np = parent->size;
        std::vector<bool> used(np, false);
        for (auto &&i : R) {
            if (std::cmp_less(i, 0) || std::cmp_greater_equal(i, np))
                return false;
            if (used[i]) return false;
            used[i] = true;
        }
        return true;
    }
    void throw_if_cant_set_base(const data *parent,
                                std::ranges::range auto &&indices) {
        if (!handle) {
            throw std::runtime_error("Empty ISet have no base, can't set base");
        } else {
            if (handle->parent)
                throw std::runtime_error(
                    "ISet already has a parent, can't set base");
        }
        if (!is_consistent_mapping(parent, indices)) {
            throw std::runtime_error("Inconsistent mapping, can't set base.");
        }
    }

   public:
    template <std::ranges::range R>
        requires(
            std::assignable_from<std::add_lvalue_reference_t<mapping_t>, R>)
    ISet &set_base(const ISet &parent, R &&indices) {
        throw_if_cant_set_base(parent.handle, indices);
        handle->parent = parent.handle;
        handle->mapping = std::forward<R>(indices);
        return *this;
    }
    template <std::integral I>
        requires(!std::is_same_v<std::decay_t<I>, bool>)
    ISet &set_base(const ISet &parent,
                   const std::initializer_list<I> &indices) {
        throw_if_cant_set_base(parent.handle, indices);
        handle->parent = parent.handle;
        handle->mapping = mapping_t::vector{indices.begin(), indices.end()};
        return *this;
    }

   private:
    bool operator==(const data::handle &other) const noexcept {
        return handle == other;
    }

   public:
    Chain chain() const { return Chain{*this}; }

    bool has_ancestor(const ISet &I) const { return ancestors().contains(I); }

    Chain ancestors() const {
        assert(!handle || handle->size > 0);
        return handle ? Chain{handle->parent} : Chain{};
    }

    /** WARNING all comparison operations on the ancestor chain must be made
     * using handles because ISet::operator== can trigger an infinite recursive
     * call
     */
    static std::optional<ISet> lowest_common_ancestor(const ISet &iset1,
                                                      const ISet &iset2) {
        // if one iset at least is empty, no common ancestor return false
        if (!(iset1 && iset2)) return {};
        if (iset1.handle == iset2.handle) return iset1;
        using std::views::reverse;
        using std::views::zip;
        // C++23
        // std::vector<ISet> anc1{std::from_range, iset1.chain()};
        // std::vector<ISet> anc2{std::from_range, iset2.chain()};
        std::vector<ISet> anc1;
        std::ranges::copy(iset1.chain(), back_inserter(anc1));
        std::vector<ISet> anc2;
        std::ranges::copy(iset2.chain(), back_inserter(anc2));
        // start with an empty set
        ISet lca;
        for (auto &&[a1, a2] : zip(anc1 | reverse, anc2 | reverse)) {
            // WARNING: pointer comparison only because ISet::operator== would
            // trigger an infinite recursive call
            if (a1.handle != a2.handle) break;
            lca = a1;
        }
        if (lca) return lca;
        // do not return empty set but false optional if lca is still empty
        return {};
    }

    bool is_included_in(const ISet &other) const {
        // empty set is included in any set (even empty one)
        if (!*this) return true;
        // non empty set cannot be included in empty set
        if (!other) return false;
        if (handle == other.handle) return true;
        auto lca = lowest_common_ancestor(*this, other);
        // if no common ancestor sets are disjoint
        if (!lca) return false;
        // FIXME: avoid copy
        auto my_mapping = resolve_mapping_onto_ancestor(*lca);
        my_mapping.sort();
        auto its_mapping = other.resolve_mapping_onto_ancestor(*lca);
        its_mapping.sort();
        auto p = its_mapping.begin();
        for (auto &&k : my_mapping) {
            while (p != its_mapping.end() && *p < k) {
                ++p;
            }
            if (p == its_mapping.end()) return false;
            assert(*p >= k);
            if (*p > k) return false;
        }
        return true;
    }

    mapping_t resolve_mapping_onto_ancestor(const ISet &A) const {
        assert(*this);
        assert(A);
        assert(chain().contains(A));
        if (handle == A.handle) {
            return self_mapping();
        }
        std::vector<index_type> indices{mapping().copy_as_vector()};
        for (const data *p = handle->parent; p != A.handle; p = p->parent) {
            const auto &map = p->mapping;
            for (auto &&k : indices) {
                k = map[k];
            }
        }
        mapping_t result;
        result = move(indices);
        return result;
    }

    mapping_t self_mapping() const {
        return {mapping_t::slice{0, size_as_index_type()}};
    }
    mapping_t resolve_mapping(const ISet &I) const {
        // resolve mapping over empty iset returns empty mapping_t
        if (!*this) return {};
        assert(I);
        // if target iset is the same as this
        if (handle == I.handle) {
            return self_mapping();
        }
        // if the target iset is in the ancestors list
        if (has_ancestor(I)) {
            // TODO: remove useless copy
            // FIXME: return resolve_mapping_onto_ancestor(A);
            mapping_t result;
            result = resolve_mapping_onto_ancestor(I);
            return result;
        }
        // target iset is not in the ancestors list, compute from lca
        assert(is_included_in(I));
        auto lca = lowest_common_ancestor(*this, I);
        assert(lca);
        auto map_source = resolve_mapping_onto_ancestor(*lca);
        auto map_target = I.resolve_mapping_onto_ancestor(*lca);
        // init tmp vector to avoid find function
        std::vector<index_type> map_target_location;
        map_target_location.resize(map_target.max() + 1);
        for (index_type i = 0; i < map_target.size(); ++i) {
            map_target_location[map_target[i]] = i;
        }
        // fill the new mapping
        std::vector<index_type> new_mapping;
        new_mapping.reserve(size());
        for (auto &&i : map_source) {
            new_mapping.push_back(map_target_location[i]);
        }
        mapping_t result;
        result = move(new_mapping);
        return result;
    }

    ISet rebase(const ISet &target_iset) const {
        assert(target_iset);
        if (!handle) return {};  // empty set
        auto new_mapping = resolve_mapping(target_iset);
        return target_iset.extract(new_mapping);
    }

    template <typename T>
    ISet _reorder(const T &order) const {
        // reorder of empty ISet gives empty iset
        if (!*this) return ISet{};
        throw_if_no_base("reorder");
        const auto n = size();
        assert(std::ranges::size(order) == n);
        assert(std::ranges::all_of(order, [n](auto i) {
            return std::cmp_less(i, n);
        }));  // order is a permutation of [0,n-1]
        std::vector<index_type> new_mapping;
        new_mapping.reserve(n);
        const auto &this_mapping = mapping();
        for (auto &&k : order) {
            new_mapping.push_back(this_mapping[k]);
        }
        return parent().extract(new_mapping);
    }

    template <std::ranges::range R>
    ISet reorder(const R &order) const {
        return _reorder(order);
    }

    template <std::integral I>
    ISet reorder(const std::initializer_list<I> &order) const {
        return _reorder(order);
    }

    ISet substract(const ISet &other) const {
        // Note: no assertion is made that ISet other has non-empty intersection
        // with *this. If intersection is empty, returns a copy of *this
        auto lca = lowest_common_ancestor(*this, other);
        assert(lca && "Cannot substract from ISet with no common ancestor");
        auto rebased = this->rebase(*lca);
        auto map_other = other.resolve_mapping(*lca);
        std::set<index_type> map_other_set(map_other.begin(), map_other.end());
        auto &rebased_mapping = rebased.mapping();
        std::vector<index_type> new_mapping;
        for (auto &&i : rebased) {
            if (map_other_set.find(rebased_mapping[i]) == map_other_set.end()) {
                new_mapping.push_back(i);
            }
        }
        return this->extract(new_mapping);
    }

    friend std::ostream &operator<<(std::ostream &os,
                                    const ISet &iset_to_output) {
        auto N = iset_to_output.size();

        os << "ISet size: " << N << ", ";
        if (iset_to_output.has_base()) {
            os << " parent size: " << iset_to_output.parent().size()
               << std::endl;
            os << "mapping: {";
            compass::utils::pretty_print(iset_to_output.mapping(), os, " ", "");
            os << "}";
        } else {
            os << " it has no base.";
        }
        return os;
    }

    auto enumerate_mapping() const { return std::views::zip(*this, mapping()); }

};  // struct ISet

// this definitions are useful for ADL in combination
// with *functional-like* programing
inline auto begin(const ISet &I) { return I.begin(); }
inline auto end(const ISet &I) { return I.end(); }
inline auto size(const ISet &I) { return I.size(); }

inline auto lowest_common_ancestor(const ISet &iset1, const ISet &iset2) {
    return ISet::lowest_common_ancestor(iset1, iset2);
}

inline std::optional<ISet> lowest_common_ancestor(
    std::ranges::range auto &&isets)
    requires(std::is_same_v<typename std::decay_t<decltype(isets)>::value_type,
                            ISet>)
{
    if (isets.size() == 0) return {};
    auto it = isets.begin();
    ISet lca = *it;
    if (isets.size() == 1) return lca;
    for (++it; it != isets.end(); ++it) {
        auto new_lca = lowest_common_ancestor(lca, *it);
        if (!new_lca) return {};
        lca = *new_lca;
    }
    return lca;
}

template <typename... ISets>
    requires(sizeof...(ISets) > 2)
inline auto lowest_common_ancestor(ISets &&...isets) {
    return lowest_common_ancestor(
        std::array<ISet, sizeof...(ISets)>{std::forward<ISets>(isets)...});
}

inline std::optional<ISet> resolve_mapping(std::ranges::range auto &&isets)
    requires(std::is_same_v<typename std::decay_t<decltype(isets)>::value_type,
                            ISet>)
{
    if (isets.size() == 0) return {};
    auto it = isets.begin();
    ISet lca = *it;
    if (isets.size() == 1) return lca;
    for (++it; it != isets.end(); ++it) {
        auto new_lca = lowest_common_ancestor(lca, *it);
        if (!new_lca) return {};
        lca = *new_lca;
    }
    return lca;
}

inline bool are_disjoint_sets(const ISet &iset1, const ISet &iset2) {
    auto lca = lowest_common_ancestor(iset1, iset2);
    if (!lca) {  // Case iset1 and iset2 have no common ancestor
        return true;
    } else {  // Case iset1 and iset2 have a common ancestor
        // FIXME: to be adpated if there optimizations depending on the mapping
        // type
        ISet small, big;
        if (iset1.size() < iset2.size()) {
            small = iset1;
            big = iset2;
        } else {
            small = iset2;
            big = iset1;
        }
        auto map1 = small.resolve_mapping(*lca);
        auto map2 = big.resolve_mapping(*lca);
        map1.sort();
        for (auto &&i : map2) {
            if (std::ranges::binary_search(map1, i)) return false;
        }
        return true;
    }
}

#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
[[deprecated("replaced by operator==")]]
inline bool are_same_indexed_set(const ISet &I, const ISet &J) {
    return I == J;
}
#endif

inline ISet join_no_base(std::ranges::range auto &&isets) {
    int n = 0;
    for (auto &&I : isets) {
        assert(!I.has_base());
        n += I.size();
    }
    if (n == 0) return ISet{};
    auto parent = ISet{n};
    assert(parent.size() < std::numeric_limits<index_type>::max());
    index_type i = 0;
    for (auto &&I : isets) {
        // set_base raises error with empty iset
        if (I.empty()) continue;
        // suboptimal we should rely on slices
        std::vector<index_type> indices;
        n = I.size();
        indices.resize(n);
        std::iota(begin(indices), end(indices), i);
        I.set_base(parent, std::move(indices));
        i += n;
    }
    return parent;
}

template <typename... ISets>
    requires(sizeof...(ISets) > 1)
inline ISet join_no_base(ISets &&...isets) {
    return join_no_base(
        std::array<ISet, sizeof...(ISets)>{std::forward<ISets>(isets)...});
}

inline ISet join_common_ancestor(const ISet &iset1, const ISet &iset2) {
    auto lca = lowest_common_ancestor(iset1, iset2);
    assert(lca);
    auto map1 = iset1.resolve_mapping(*lca);
    auto map2 = iset2.resolve_mapping(*lca);
    std::vector<index_type> map;
    map.reserve(iset1.size() + iset2.size());
    map.insert(map.end(), map1.begin(), map1.end());
    map.insert(map.end(), map2.begin(), map2.end());
    // Remove duplicates - NB this sorts mapping
    compass::utils::sort_and_unique(map);
    return lca->extract(map);
}

inline ISet join_no_common_ancestor(const ISet &iset1, const ISet &iset2) {
    if (!iset1) return iset2;
    if (!iset2) return iset1;
    auto ancestor1{iset1.chain().back()};
    auto ancestor2{iset2.chain().back()};
    // Next assertion fails if iset1 and iset2 have a common ancestor
    assert(ancestor1 && ancestor2 && ancestor1 != ancestor2);
    // Next assertion fails if ancestor1 or ancestor2 has a parent
    // This should not hapen - BY design
    assert(!ancestor1->has_base() && !ancestor2->has_base());
    join_no_base(*ancestor1, *ancestor2);
    return join_common_ancestor(iset1, iset2);
}

inline ISet join(const ISet &iset1, const ISet &iset2) {
    // if one iset is empty, return the other one (can be also an empty iset)
    if (iset1.empty()) {
        return iset2;
    } else if (iset2.empty()) {
        return iset1;
    }
    assert(iset1);
    assert(iset2);
    // Case iset1 and iset2 have no parents
    if (!(iset1.has_base()) && !(iset2.has_base())) {
        return join_no_base(iset1, iset2);
    }
    auto lca = lowest_common_ancestor(iset1, iset2);
    if (lca) {  // Case iset1 and iset2 have a common ancestor
        return join_common_ancestor(iset1, iset2);
    } else if (!lca) {  // Case iset1 and iset2 have no common ancestor
        return join_no_common_ancestor(iset1, iset2);
    } else {
        throw std::runtime_error("Error in ISet union");
    }
}

inline ISet::mapping_t::vector intersect_maps(const auto &map1,
                                              const auto &map2) {
    // Compute intersection between map1 and map2
    ISet::mapping_t::vector inter_map;
    std::set<index_type> second_indices(map2.begin(), map2.end());
    for (auto &&k : map1) {
        if (second_indices.find(k) != second_indices.end()) {
            inter_map.push_back(k);
        }
    }
    return inter_map;
}

inline ISet intersect(const ISet &iset1, const ISet &iset2,
                      const ISet &new_base) {
    // Compute intersection between iset1 and iset2
    // as a child of their lowest common ancestor
    // and rebase onto new_base
    if (iset1.empty() || iset2.empty()) return ISet();
    assert(iset1 && iset2);
    auto lca = lowest_common_ancestor(iset1, iset2);
    assert(lca && "Calling intersection with no common ancestor");
    auto first_map = iset1.resolve_mapping_onto_ancestor(*lca);
    auto second_map = iset2.resolve_mapping_onto_ancestor(*lca);
    auto inter_map = intersect_maps(first_map, second_map);
    // if inter_map is empty, return empty iset
    auto out = lca->extract(inter_map);
    return out.rebase(new_base);
}

inline ISet intersect_siblings(const ISet &iset1, const ISet &iset2) {
    // Compute intersection between iset1 and iset2
    // that have the same parent
    if (iset1.empty() || iset2.empty()) return ISet();
    assert(iset1.parent() == iset2.parent());
    auto inter_map = intersect_maps(iset1.mapping(), iset2.mapping());
    // if inter_map is empty, return empty iset
    auto out = iset1.parent().extract(inter_map);
    return out;
}

inline auto enumerate_mapping(const ISet &I) { return I.enumerate_mapping(); }

template <typename F>
inline void visit_mapping(const ISet::mapping_t &mapping, F &&f) {
    for (auto i : mapping) {
        f(i);
    }
}

/** We copy I to increase the underlying shared pointer reference count. */
template <typename F>
inline void visit_mapping(const ISet I, F &&f) {
    visit_mapping(I.mapping(), std::forward<F>(f));
}

}  // namespace icus
