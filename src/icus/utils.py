def have_same_support(a, b):
    result = False
    a_support = getattr(a, "support", a)
    b_support = getattr(b, "support", b)
    inputs = [(a, b), (a_support, b_support), (a, b_support), (a_support, b)]
    for _a, _b in inputs:
        try:
            result = _a == _b
        except TypeError:
            pass
        if result:
            return True
    return False
