import verstr

try:
    from . import _version

    __version__ = verstr.verstr(_version.version)
except ImportError:
    __version__ = None

import compass

with compass.nanobind():
    from .bindings import *

    # cf. static_assert(std::is_same_v<int, std::int32_t>) in src/bindings/field.cpp
    Labels = FieldInt32
    if long_is_int64:
        FieldLong = FieldInt64
    else:
        assert FieldLong is not FieldInt64
        pass

# FIXME: may interact badly with the exposure of have_same_support
#        in ../src/bindings/connectivity.cpp
from .utils import have_same_support
from . import fig
