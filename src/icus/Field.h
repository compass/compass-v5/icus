#pragma once

#include <compass_cxx_utils/pretty_print.h>
#include <icus/ISet.h>

#include <algorithm>
#include <iostream>
#include <memory>
#include <type_traits>
#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
#include <span>
#endif

namespace icus {

namespace detail {

template <typename T, typename MappingIterator>
struct Filtered_field_iterator {
    // iterator traits
    using value_type = T;
    using pointer = T *;
    using reference = T &;
    // FIXME: must be defined to as signed type for ranges-v3
    //        even for forward iterators
    using difference_type = long;
    using iterator_category = std::forward_iterator_tag;
    // implementation
    T *values;
    MappingIterator offset;
    Filtered_field_iterator() = default;
    Filtered_field_iterator(const Filtered_field_iterator &) = default;
    Filtered_field_iterator(Filtered_field_iterator &&) = default;
    Filtered_field_iterator &operator=(const Filtered_field_iterator &) =
        default;
    Filtered_field_iterator &operator=(Filtered_field_iterator &&) = default;
    template <typename It>
    Filtered_field_iterator(T *p, It &&pos)
        : values{p}, offset{std::forward<It>(pos)} {
        assert(values);
    }
    reference operator*() const { return *(values + *offset); }
    // we must define increment operations so that they return the correct
    // reference type
    Filtered_field_iterator &operator++() {
        ++offset;
        return *this;
    }
    Filtered_field_iterator operator++(int) {
        Filtered_field_iterator pre_increment{*this};
        ++offset;
        return pre_increment;
    }
    bool operator==(const Filtered_field_iterator &other) const {
        assert(values == other.values);
        return offset == other.offset;
    }
    bool operator!=(const Filtered_field_iterator &other) const {
        assert(values == other.values);
        return offset != other.offset;
    }
};

}  // namespace detail

/** Base clase of all fields. */
struct Field_base {};

template <typename T = double, bool is_view = false>
struct Field : Field_base {
    using value_type = T;

    ISet support;
    std::shared_ptr<value_type[]> values;

    Field() = default;
    Field(const ISet &sup) : support{sup} {
        // CXX20: not all compilers extend std::make_shared() to arrays yet
        values = std::shared_ptr<value_type[]>{new T[support.size()]};
        assert(values);
    }

   private:
    friend Field<value_type, !is_view>;
    Field(ISet &&I, const std::shared_ptr<value_type[]> &sp)
        : support{std::forward<ISet>(I)}, values{sp} {}

   public:
    auto view(const ISet &I) const
        requires(!is_view)
    {
        return Field<value_type, true>{I.rebase(support), values};
    }

    auto view(const ISet &I) const
        requires(is_view)
    {
        return Field<value_type, true>{I.rebase(support.parent()), values};
    }

#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
    auto span() const
        requires(!is_view)
    {
        return std::span{values.get(), size()};
    }
#endif

    T *data() const
        requires(!is_view)
    {
        return values.get();
    }

    T *begin() const
        requires(!is_view)
    {
        return values.get();
    }

    T *end() const
        requires(!is_view)
    {
        return values.get() + size();
    }

    auto begin() const
        requires(is_view)
    {
        // FIXME: do not test against empty support
        ISet::mapping_t::iterator it;
        if (support) it = support.mapping().begin();
        return detail::Filtered_field_iterator<value_type, decltype(it)>{
            values.get(), it};
    }

    auto end() const
        requires(is_view)
    {
        // could be simpler and sentinel like
        // FIXME: do not test against empty support
        ISet::mapping_t::iterator it;
        if (support) it = support.mapping().end();
        return detail::Filtered_field_iterator<value_type, decltype(it)>{
            values.get(), it};
    }

    Field &operator=(const Field &) = default;

    Field &fill(const value_type &value) {
        for (auto &&x : *this) x = value;
        return *this;
    }

#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
    [[deprecated("use Field::fill instead of Field::operator=")]]
    Field &operator=(const value_type &value) {
        for (auto &&x : *this) x = value;
        return *this;
    }
#endif

    template <typename R>
        requires(std::ranges::range<R>)
    Field &fill(const R &r) {
        assert(std::ranges::size(r) == size());
        std::copy(std::begin(r), std::end(r), values.get());
        return *this;
    }

#ifndef COMPASS_HAS_NO_DEPRECATED_CODE
    template <typename R>
        requires(std::ranges::range<R>)
    [[deprecated("use Field::fill instead of Field::operator=")]]
    Field &operator=(const R &r) {
        assert(std::ranges::size(r) == size());
        std::copy(std::begin(r), std::end(r), values.get());
        return *this;
    }
#endif

    bool operator==(const value_type &value) const {
        return std::ranges::all_of(begin(), end(),
                                   [value](auto &&x) { return x == value; });
    }

    bool operator==(std::ranges::range auto &&r) const {
        return std::ranges::equal(*this, r);
    }

    value_type &operator()(const index_type &i) const
        requires(!is_view)
    {
        return values[i];
    }

    value_type &operator()(const index_type &i) const
        requires(is_view)
    {
        // CHECKME: we could store a copy of the mapping in views for
        // performance reasons
        return values[support.mapping()[i]];
    }

    auto size() const { return support.size(); }

    Field<value_type> extract(const ISet &I) const { return view(I).copy(); }

    Field<value_type> copy() const { return copy(support); }

    /** Copy a Field changing its support - while preserving its size. */
    Field<value_type> copy(const ISet &I) const {
        if (I.size() != support.size()) {
            throw std::runtime_error("new support has an unconsistent size");
        }
        // the result must not be a view
        Field<value_type> result{I};
        std::copy(begin(), end(), result.values.get());
        return result;
    }

    /** Create a new field changing its support, replacing it with
     *  an ISet of the same size, sharing the underlying memory. */
    Field share(ISet I) const {
        if (I.size() != support.size()) {
            throw std::runtime_error("new support has an unconsistent size");
        }
        return {std::move(I), values};
    }

    auto enumerate() const { return std::views::zip(support, *this); }
    auto enumerate() { return std::views::zip(support, *this); }

};  // struct Field

template <typename T>
inline auto copy(const Field<T> &P) {
    return P.copy();
}

template <typename Field_>
inline auto enumerate(Field_ &&F) {
    return F.enumerate();
}

template <typename T, bool is_view>
inline auto view(const Field<T, is_view> &P, const auto &I) {
    return P.view(I);
}

template <typename T, bool is_view>
std::ostream &operator<<(std::ostream &os, const Field<T, is_view> &P) {
    if (P.support.size() == 0) os << "EMPTY ";
    os << "Field";
    if constexpr (is_view) os << " view";
    if (P.support.size() > 0) {
        os << " carried by an iset whose size is " << P.support.size()
           << std::endl;
        os << "Values : {";
        compass::utils::Pretty_printer print{os};
        print.nbmax = 12;
        print.nbmin = 5;
        print.endl = "";
        print(P);
        os << "}";
    }
    return os;
}

template <typename value_type = double>
auto make_field(const ISet &support) {
    return Field<value_type>{support};
}

/** Creates a field from support and values.
 *  \param support the support of the field.
 *  \param value a range of values to be used to fill the field
 *               (the support and the field size must be consistent).
 */
template <std::ranges::range R>
auto make_field(const ISet &support, R &&values)
    -> Field<std::ranges::range_value_t<R>> {
    Field<std::ranges::range_value_t<R>> result{support};
    result.fill(std::forward<R>(values));
    return result;
}

/** Creates a field from support and value.
 *  \param support the support of the field.
 *  \param value a value to be used to fill the field
 *               (the type of the field will be deduced from the value type).
 */
template <typename T>
    requires(!std::ranges::range<T>)
auto make_field(const ISet &support, const T &value) -> Field<T> {
    Field<T> result{support};
    result.fill(value);
    return result;
}

template <typename F0, typename F1, typename... Fs>
bool have_same_support(F0 &&f0, F1 &&f1, Fs &&...fs) {
    if (f0.support != f1.support) {
        return false;
    } else if constexpr (sizeof...(Fs) > 0) {
        return have_same_support(std::forward<F1>(f1), std::forward<Fs>(fs)...);
    }
    return true;
}

template <typename T>
struct Toggle_field_view;

template <typename T, bool is_view>
struct Toggle_field_view<Field<T, is_view>> {
    using type = Field<T, !is_view>;
};

template <typename T>
using toggle_field_view_t = typename Toggle_field_view<T>::type;

template <typename T>
struct Is_view;

template <typename T, bool is_view>
struct Is_view<Field<T, is_view>> {
    static inline constexpr bool value = is_view;
};

template <typename F>
inline constexpr bool is_view = Is_view<F>::value;

using Labels = Field<int>;

}  // namespace icus
