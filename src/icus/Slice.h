#pragma once

#include <cassert>

#include "Counter.h"

namespace icus {

template <std::integral I>
struct Slice {
    using iterator = typename Counter<I>::iterator;
    I _begin;
    I _end;
    I front() const noexcept {
        assert(!empty());
        return _begin;
    }
    I back() const noexcept {
        assert(!empty());
        assert(_end - 1 >= _begin);
        return _end - 1;
    }
    iterator begin() const { return {_begin}; }
    iterator end() const { return {_end}; }
    bool is_consistent() const noexcept {
        if (std::cmp_less(_begin, 0)) return false;
        if (_begin > _end) return false;
        return true;
    }
    bool empty() const noexcept {
        assert(is_consistent());
        return _begin == _end;
    }
    std::size_t size() const noexcept {
        assert(is_consistent());
        return _end - _begin;
    }
    I operator[](std::integral auto k) const noexcept {
        assert(std::cmp_less(k, size()));  // size will check consitency
        assert(std::cmp_equal(static_cast<I>(k), k));
        assert(std::cmp_less(_begin + static_cast<I>(k), _end));
        return _begin + static_cast<I>(k);
    }
};

}  // namespace icus
