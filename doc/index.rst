=================
ICUS |version|
=================

Icus objects are lightweight (they are just handles to underlying data so that their copies is cheap).

Most of the time they are not mutable: most of operations produce new objects but do not modify the current object.

..
   Cf. hack below to insert relative html link in TOC tree
   https://stackoverflow.com/a/31820846/4757961

.. toctree::
   :maxdepth: 1
   :caption: Contents:

   isets
   connectivities
   fields
   python_api
   C++ API <./cpp/index.html#https://>

This documentation was built against ICUS version |release|.

License and credits
===================

ICUS is a ComPASS module.

ComPASS is co-developped by `BRGM`_ and `Université Côte d'Azur`_ / `LJAD`_ / `Inria`_

.. _BRGM: https://www.brgm.fr/fr
.. _Université Côte d'Azur: https://univ-cotedazur.fr/
.. _LJAD: https://univ-cotedazur.fr/laboratoires/laboratoire-jean-alexandre-dieudonne-ljad
.. _Inria: https://www.inria.fr/fr/centre-inria-universite-cote-azur
