# 1. ISet variant implementation

Date: 2022-11-15

## Status

Accepted

## Context

ISets are divided in two categories: on one hand those which only represent an independent set of indexed items (here called raw ISets), on the other hand those (here called Base) which represent a subset of a parent ISet. Raw ISets can be entirely defined by their cardinality, i.e. a simple unsigned integer. Bases however hold a reference to their parent ISet and a Mapper object to the corresponding indices of the parent. A raw ISet can become a Base in certain contexts (such as the union between ISets), and consequently ISets need to change nature. The nature of the Mapper object can vary (array of arbitrary indices, range, view...)

## Decision

We decide to implement the ISet class with a single std::variant attribute (https://en.cppreference.com/w/cpp/utility/variant) that is handeled through the class' methods.

## Consequences

The implementation of the Mapper object can evolve without changing the implementation of ISets.
ISets can change nature without the ICUS user knowing.
It is not straightforward to access the underlying object.
