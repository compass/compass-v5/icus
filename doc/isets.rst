ISets
=====

Work in progress...


Let's start with an ISet of 42 elements.

.. tabs::

   .. tab:: Python

      .. code-block:: python

        from icus import ISet

        I = ISet(42)

   .. tab:: C++

      .. code-block:: python

        #include <icus/ISet.h>
        using namespace icus;

        I = ISet(42);


On its own an ISet essentially behaves as the python range utility.


.. tabs::

   .. tab:: Python

      .. code-block:: python

         for i in I:
            print(i)

   .. tab:: C++

      .. code-block:: python

         for(auto&& i: I) {
            std::cout << i << std::endl;
         }

Talk about the comparison operator.
