Icus objects are lightweight (they are just handles to underlying data so that their copies is cheap).

Most of the time they are not mutable: most of operations produce new objects but do not modify the current object.
