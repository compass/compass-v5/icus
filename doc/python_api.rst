Python  API
===========

.. autosummary::
   :recursive:
   :toctree: generated

   icus.ISet
   icus.Connectivity
   icus.field
   icus.fig
