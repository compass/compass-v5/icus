#
# this file is generated
# if you edit it do not forget to track it with git
#

# most of sphinx configuration information is factorized in compass.sphinx package
from compass.doc.sphinx.html import *
from compass.doc.sphinx.utils import get_version_info

project = "ICUS"
version, release = get_version_info(__file__)
